package UML.Observeur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.Contrat.Conteneur.IConteneurAttribut;
import Noyau.Arbre.Contrat.Conteneur.IConteneurClasse;
import Noyau.Arbre.Contrat.Conteneur.IConteneurEnum;
import Noyau.Arbre.Contrat.Conteneur.IConteneurFonction;
import Noyau.Arbre.Contrat.Conteneur.IConteneurInterface;
import Noyau.Arbre.ElementJava.Attribut;
import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Arbre.Observeur.Conception.IObserveurClasse;
import Noyau.Dependance.Dependance;
import Noyau.Dependance.IConteneurDependance;
import Noyau.Modifieur.IConteneurModifieur;
import Noyau.Modifieur.Modifieur;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;
import UML.SchemaUML;

public class ObserveurClasse implements IObserveurClasse, IObserveurBoite {
	
	private List<ObserveurClasse> listeObserveursClasse;
	private List<ObserveurInterface> listeObserveursInterface;
	private List<ObserveurEnum> listeObserveursEnum;
	
	private Boite boite;
	
	private Classe classe;
	private List<Attribut> attributs = classe.getAttributs();
	private List<Fonction> fonctions = classe.getFonctions();
	
	private SchemaUML schemaUML;
	
	public ObserveurClasse(SchemaUML schuml,Classe c) {
		this.listeObserveursClasse = new ArrayList<ObserveurClasse>();
		this.listeObserveursInterface = new ArrayList<ObserveurInterface>();
		this.listeObserveursEnum = new ArrayList<ObserveurEnum>();
		this.schemaUML=schuml;
		//int nbElements = c.getAttributs().size()+c.getClasses().size()+c.getEnums().size()+c.getFonctions().size()+c.getInterfaces().size();
		//int hauteurBoite = nbElements/10;
		//this.boite = schemaUML.getSchema().addBoite(2,hauteurBoite);
		//this.creerCategories();
		c.addObserveur(this);
		this.classe=c;
	}
	
	public Classe getClasse() {
		return this.classe;
	}
	
	public ObserveurClasse getObserveur(Classe c) {
		ObserveurClasse oc = null;
		for(ObserveurClasse obsc : this.listeObserveursClasse) {
			if(obsc.getClasse()==c) {
				oc=obsc;
			}
		}
		return oc;
	}
	
	public ObserveurInterface getObserveur(Interface i) {
		ObserveurInterface oi = null;
		for(ObserveurInterface obsi : this.listeObserveursInterface) {
			if(obsi.getInterface()==i) {
				oi=obsi;
			}
		}
		return oi;
	}
	
	public ObserveurEnum getObserveur(Enum e) {
		ObserveurEnum oe = null;
		for(ObserveurEnum obse : this.listeObserveursEnum) {
			if(obse.getEnum()==e) {
				oe=obse;
			}
		}
		return oe;
	}
	
	public List<ObserveurClasse> getObserveursClasse() {
		return this.listeObserveursClasse;
	}
	
	public List<ObserveurInterface> getObserveursInterface() {
		return this.listeObserveursInterface;
	}
	
	public List<ObserveurEnum> getObserveursEnum() {
		return this.listeObserveursEnum;
	}
	
	public void supprimer() {
		for(ObserveurClasse oc : this.getObserveursClasse()) {
			oc.supprimer();
		}
		for(ObserveurInterface oi : this.getObserveursInterface()) {
			oi.supprimer();
		}
		for(ObserveurEnum oe : this.getObserveursEnum()) {
			oe.supprimer();
		}
		// supprimer l'orbservateur classe		
	}
	
	private void creerCategories() {
		for(int i=0;i<3;i++) {
			this.boite.addCategorie();
		}
	}
	
	private void creerTexte() {
		for(int i=0;i<this.boite.countCategorie();i++) {
			boite.clearTexte(i);
		}
		boite.addTexte(classe.getNom(), 0);
		for(Attribut att : this.classe.getAttributs()) {
			boite.addTexte(Modifieur.getSigle(att.getModifieurs()),att.getNom(),1);
		}
		for(Fonction f : this.classe.getFonctions()) {
			boite.addTexte(Modifieur.getSigle(f.getModifieurs()),f.getNom(),2);
		}	
	}

	@Override
	public void changeNom(INoeudNommable n) {
		FenetreDialogue.renommerNoeud(n);
	}

	@Override
	public void update(INoeud n) {
		creerTexte();
	}

	@Override
	public void updateFils(INoeud n) {
		creerTexte();
	}

	@Override
	public void updatePetitFils(INoeud n) {
		creerTexte();
	}

	@Override
	public void modifieurAjoute(IConteneurModifieur i, Modifieur a) {
		
	}

	@Override
	public void modifieurRetire(IConteneurModifieur i, Modifieur a) {
		
	}

	@Override
	public void dependanceAjoute(IConteneurDependance c, Dependance a) {
		
	}

	@Override
	public void dependanceRetire(IConteneurDependance c, Dependance a) {
		
	}

	@Override
	public void classeAjoute(IConteneurClasse i, Classe a) {
		this.listeObserveursClasse.add(new ObserveurClasse(schemaUML,a));
	}

	@Override
	public void classeRetire(IConteneurClasse i, Classe a) {
		ObserveurClasse obsa = this.getObserveur(a);
		obsa.supprimer();
	}

	@Override
	public void interfaceAjoute(IConteneurInterface i, Interface a) {
		this.listeObserveursInterface.add(new ObserveurInterface(schemaUML,a));
	}

	@Override
	public void interfaceRetire(IConteneurInterface i, Interface a) {
		ObserveurInterface obsa = this.getObserveur(a);
		obsa.supprimer();
	}

	@Override
	public void enumAjoute(IConteneurEnum i, Enum a) {
		this.listeObserveursEnum.add(new ObserveurEnum(schemaUML,a));
	}

	@Override
	public void enumRetire(IConteneurEnum i, Enum a) {
		ObserveurEnum obsa = this.getObserveur(a);
		obsa.supprimer();
	}

	@Override
	public void attributAjoute(IConteneurAttribut i, Attribut f) {
		
	}

	@Override
	public void attributRetire(IConteneurAttribut i, Attribut a) {
		
	}

	@Override
	public void fonctionAjout(IConteneurFonction i, Fonction a) {
		
	}

	@Override
	public void fonctionRetire(IConteneurFonction i, Fonction a) {
		
	}

	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		
		// Classe
		if(categorie==0) {
			JPopupMenu menu = new JPopupMenu ();
	    	
	    	JMenuItem rename = new JMenuItem("renommer classe");
	    	JMenuItem suppr = new JMenuItem("supprimer classe");
	    	
	    	rename.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					FenetreDialogue.modifierClasse(classe);
				}
	    	});
	    	
		    suppr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					classe.supprimer();
				}
	    	});
	    	
	    	menu.add (rename);
	    	menu.add (suppr);

	    	menu.show(e.getComponent(), e.getX(), e.getY());
		}
		
		// Attribut
		else if(categorie==1) {
			final int rangfin = rang;
			JPopupMenu menu = new JPopupMenu ();
	    	
	    	JMenuItem modif = new JMenuItem("modifier attribut");
	    	JMenuItem suppr = new JMenuItem("supprimer attribut");
	    	
	    	modif.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					FenetreDialogue.modifierAttribut(attributs.get(rangfin));
				}
	    	});
	    	
		    suppr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					attributs.get(rangfin).supprimer();
				}
	    	});
	    	
	    	menu.add (modif);
	    	menu.add (suppr);

	    	menu.show(e.getComponent(), e.getX(), e.getY());
		}
		
		// Fonction
		else if(categorie==2) {
			final int rangfin = rang;
			JPopupMenu menu = new JPopupMenu ();
			    
			JMenuItem logi = new JMenuItem("ouvrir logigramme");
			JMenuItem modif = new JMenuItem("modifier fonction");
			JMenuItem suppr = new JMenuItem("supprimer fonction");
			
			logi.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					schemaUML.getApplication().changerPanneau("Logigramme", new Object[] {fonctions.get(rangfin)});
				}
			});
			    	
			modif.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					FenetreDialogue.modifierFonction(fonctions.get(rangfin));
				}
			});
			    	
			suppr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fonctions.get(rangfin).supprimer();
				}
		   	});
			    	
			menu.add (modif);
			menu.add (suppr);

			menu.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier classe");
    	JMenuItem suppr = new JMenuItem("supprimer classe");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierClasse(classe);
			}
    	});
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				classe.supprimer();
			}
    	});
    	
    	menu.add (modif);
    	menu.add (suppr);

    	menu.show(e.getComponent(), e.getX(), e.getY());
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
