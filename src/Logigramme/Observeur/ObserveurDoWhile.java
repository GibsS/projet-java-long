package Logigramme.Observeur;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Logigramme.SchemaLogigramme;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.Algorithmique.DoWhile;
import Noyau.Arbre.Contrat.Conteneur.IConteneurClasse;
import Noyau.Arbre.Contrat.Conteneur.IConteneurEnum;
import Noyau.Arbre.Contrat.Conteneur.IConteneurInterface;
import Noyau.Arbre.Contrat.Conteneur.IConteneurPaquet;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurDoWhile;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;

public class ObserveurDoWhile implements IObserveurDoWhile, IObserveurBoite {
	
	private ObserveurSequence observeurSequence;
	
	private Boite boiteDebut;
	
	private Boite boiteFin;
	
	private SchemaLogigramme schemaLogigramme;
	
	private DoWhile dw;

	public ObserveurDoWhile(SchemaLogigramme schlogi, DoWhile dw) {
		this.observeurSequence = new ObserveurSequence(schlogi, dw.getContenu());
		this.schemaLogigramme = schlogi;
		this.dw = dw;
		/* Constructeur à compléter */
	
	}

	@Override
	public void update(INoeud n) {
		boiteFin.addTexte(dw.getCondition().toString(),0);
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}
	


	@Override
	public void changeExpression(DoWhile d) {
		// TODO Auto-generated method stub
		
	}
	
	// input
	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang, MouseEvent e) {
		
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier condition");
    	JMenuItem suppr = new JMenuItem("supprimer");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierDoWhile(dw);
			}
    	});
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dw.supprimer();
			}
    	});
    	
    	menu.add (modif);
    	menu.add (suppr);

    	menu.show(e.getComponent(), e.getX(), e.getY());
		
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}

