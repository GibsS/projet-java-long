package Noyau.Arbre.ElementJava;

import java.util.*;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudFichier;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.INoeudProjet;
import Noyau.Arbre.INommableJava;
import Noyau.Arbre.ModeleNoeud;
import Noyau.Arbre.Algorithmique.Expression;
import Noyau.Arbre.Contrat.Conteneur.IConteneurEnum;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conception.IObserveurEnum;
import Noyau.Arbre.Projet.Projet;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.GestionFichier.Fichier;
import Noyau.Modifieur.*;

/** la classe Enum contient les donn�es d'une �num�ration Java : le nom de leur membre ainsi que leur valeur par d�faut
 * si elle existe.
 * @author gibson
 */
public class Enum extends ModeleNoeud implements INoeudProjet, INoeudFichier, IConteneurModifieur, INommableJava, INoeudNommable {

	String nom;
	
	public Map<String,Expression> membres;
	public Map<String,Boolean> isInit;
	
	ConteneurModifieur modifieurs;
	
	Fichier fichier;
	
	public Enum(INoeud pere, String f) {
		super(pere);
		this.nom = f;
		membres = new HashMap<String, Expression>();
		isInit = new HashMap<String, Boolean>();
		
		modifieurs = new ConteneurModifieur(this);
		
		if(getProjet() != null)
			this.fichier = getProjet().getProjetMachine().creerFichierEnum(this);
	}
			
	public Projet getProjet() {
		if(getPere() != null)
			return ((INoeudProjet)getPere()).getProjet();
		else 
			return null;
	}
	
	public void compiler() {
		fichier.creerCode();
	}
	
	public String creerCode() {
		String res = "";
		res = modifieurs.toString();
		res += "enum " + getNom() + " {\n";
		for(String m:getMembres())
			res += "\t" + m + (isInit.get(m)?" = " + membres.get(m).valeur :"") + ",\n";
		res += "}\n";
		return res;
	}
	
	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code contenuCode;
		
		c.extraireEspaceInutile();
		for(Modifieur modifieur:c.extraireModifieurs())
			modifieurs.addModifieur(modifieur);
		c.extraireEspaceInutile();
		c.extraireMot("enum");
		c.extraireEspaceInutile();
		try {
			setNom(c.lireMot());
		} catch(Exception e) {
			
		}
		c.extraireEspaceInutile();
		contenuCode = c.extraireCrochet();
		
		while(!contenuCode.isEmpty() && contenuCode.toString() != "" && !contenuCode.toString().isEmpty()) {
			Expression e  = null;
			
			String membre = contenuCode.lireMot();
			contenuCode.extraireEspaceInutile();
			try {
				contenuCode.extraireMot("=");
				try {
					e = new Expression(contenuCode.extraireVersMot(",").toString());
				} catch(ParseException p1) {
					
				}
			} catch(ParseException p) {
				contenuCode.extraireEspaceInutile();
				try {
					contenuCode.extraireMot(",");
				} catch(ParseException p2) {
					
				}
			}

			if(e == null)
				this.addMembre(membre);
			else
				this.addMembre(membre,e);
		}
		
		return c;
	}
	
	public void supprimer() {
		((IConteneurEnum)getPere()).removeEnum(this);

		fichier.removeEnum(this);
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nouveauNom) throws ExceptionMauvaisNom { 
		nom = ExceptionMauvaisNom.testNom(nouveauNom); 
		for(IObserveur o : getObserveurs())((IObserveurNommable)o).changeNom(this);
		notifierModification();
	}

	/** addMembre : ajoute un membre.
	 * @param nom : le nouveau membre.
	 */
	public void addMembre(String nom) { 
		membres.put(nom,new Expression("0")); isInit.put(nom, Boolean.FALSE); 
		for(IObserveur o : getObserveurs())((IObserveurEnum)o).nouveauMembre(this,nom);
		notifierModification();
	}
	/** addMembre : ajoute un membre avec �a valeur associ�e.
	 * @param nom : le nom du membre
	 * @param valeur : la valeur par d�faut
	 */
	public void addMembre(String nom, Expression valeur) { 
		membres.put(nom,valeur); isInit.put(nom, Boolean.TRUE);
		for(IObserveur o : getObserveurs())((IObserveurEnum)o).nouveauMembre(this,nom);
		notifierModification();
	}
	/** setValeurMembre : change la valeur associ� du membre de l'�num�ration;
	 * @param nom : le nom du membre a changer.
	 * @param valeur : la nouvelle valeur.
	 */
	public void setValeurMembre(String nom, Expression valeur) { 
		membres.put(nom,valeur); isInit.put(nom, Boolean.TRUE); 
		for(IObserveur o : getObserveurs())((IObserveurEnum)o).changementMembre(this,nom);
		notifierModification();
	}
	/** removeValeurMembre : retire la valeur par d�faut du membre de nom "nom"
	 * @param nom : le nom du membre dont on retire la valeur.
	 */
	public void removeValeurMembre(String nom) { 
		isInit.put(nom, Boolean.FALSE);
		for(IObserveur o : getObserveurs())((IObserveurEnum)o).suppressionMembre(this,nom);
		notifierModification();
	}

	/** removeMembre : retire le membre de nom "nom"
	 * @param nom : le nom du membre a retirer
	 */
	public void removeMembre(String nom) { membres.remove(nom); }
	
	/** getMembres : renvoie tous les membres.
	 * @return la liste des membres.
	 */
	public ArrayList<String> getMembres() { return new ArrayList<String>(membres.keySet()); }
	/** getValeurDefaut : renvoie la valeur par defaut du membre de nom "nom"
	 * @param nom : le nom du membre a renvoyer
	 * @return la valeur par defaut
	 */
	public Expression getValeurDefaut(String nom) { return membres.get(nom); }

	public List<Modifieur> getModifieurs() { return modifieurs.getModifieurs(); }
	public boolean hasModifieur(String modifieur) { return modifieurs.hasModifieur(modifieur); }
	public void addModifieur(Modifieur modifieur) { modifieurs.addModifieur(modifieur); }
	public void removeModifieur(Modifieur modifieur) { modifieurs.removeModifieur(modifieur); }
	
	public Fichier getFichier() { return fichier; }
	public void setFichier(Fichier f) { fichier = f; }
	
	public List<String> getStringPath() {
		INoeudNommable courant = (INoeudNommable)getPere();
		List<String> chemin = new ArrayList<String>();
	
		while(!(courant instanceof Paquet)) {
			chemin.add(courant.getNom());
			courant = (INoeudNommable)courant.getPere();
		}
	
		Collections.reverse(chemin);
		return chemin;
	}
}
