package Schema.Observeur;

import java.awt.event.MouseEvent;

public interface IObserveurBoite {

	void cliqueDroitTexte(boolean droite, int categorie, int rang, MouseEvent e);
	void cliqueGaucheTexte(boolean droite, int categorie, int rang, MouseEvent e);
	
	void cliqueDroit(int x, int y, MouseEvent e);
	void cliqueGauche(int x, int y, MouseEvent e);
}
