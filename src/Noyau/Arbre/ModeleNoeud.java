package Noyau.Arbre;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import Noyau.Arbre.Observeur.IObserveur;

public abstract class ModeleNoeud implements INoeud {

	List<IObserveur> observeurs = new ArrayList<IObserveur>();
	ModeleNoeud pere;
	
	public ModeleNoeud(INoeud pere) {
		this.pere = (ModeleNoeud) pere;
	}
	
	public void addObserveur(IObserveur obs) {
		observeurs.add(obs);
	}

	public void removeObserveur(IObserveur obs) {
		observeurs.remove(obs);
	}
	
	public List<IObserveur> getObserveurs() {
		return observeurs;
	}
	
	public INoeud getPere(){
		return pere;
	}
	
	public void setPere(INoeud n) {
		pere = (ModeleNoeud) n;
	}
	
	public List<INoeud> getObjectPath() {
		List<INoeud> elements = new ArrayList<INoeud>();
		INoeud p = getPere();
		while(p != null) {
			elements.add(p);
			p = p.getPere();
		}
		Collections.reverse(elements);
		return elements;
	}
	
	public void notifierModification() {
		Iterator<IObserveur> l = observeurs.iterator();
		IObserveur obs;
		while(l.hasNext()) {
			obs = l.next();
			obs.update(this);
		}
		if(getPere() != null)
			notifierModificationFils(this);
	}
	
	public void notifierModificationFils(INoeud n) {
		Iterator<IObserveur> l = observeurs.iterator();
		IObserveur obs;
		while(l.hasNext()) {
			obs = l.next();
			obs.updateFils(n);
		}
		if(getPere() != null)
			getPere().notifierModificationGrandFils(n);
	}

	public void notifierModificationGrandFils(INoeud n) {
		Iterator<IObserveur> l = observeurs.iterator();
		IObserveur obs;
		while(l.hasNext()) {
			obs = l.next();
			obs.updatePetitFils(n);
		}
		if(getPere() != null)
			getPere().notifierModificationGrandFils(n);
	}
}
