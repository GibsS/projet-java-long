package Noyau.Arbre.Algorithmique;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudFichier;
import Noyau.Arbre.ModeleNoeud;
import Noyau.GestionFichier.Fichier;

/** Repr�sente un noeud de l'arbre Java repr�sentant un bout d'algorithme.
 * Tout autre algorithme en h�rite.
 * @author gibson
 */
public abstract class Algorithme extends ModeleNoeud implements INoeudFichier {
	
	public Algorithme(INoeud pere) {
		super(pere);
	}

	public void compiler() { ((INoeudFichier)getPere()).compiler(); }

	public Fichier getFichier() { return ((INoeudFichier)getPere()).getFichier(); }
	public void setFichier(Fichier f) {	}

	public void supprimer() { ((Sequence)getPere()).contenu.remove(this); }
}
