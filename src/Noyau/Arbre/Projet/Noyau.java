package Noyau.Arbre.Projet;

import java.util.*;

import Noyau.Arbre.ModeleNoeud;
import Noyau.Arbre.Contrat.Conteneur.*;
import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Projet.IObserveurNoyau;

public class Noyau extends ModeleNoeud {

	List<Projet> projets;
	
	public Noyau() {
		super(null);
		projets = new ArrayList<Projet>();
	}
	
	public void miseAJour() {
		for(Projet p : getProjets())
			p.miseAJour();
	}
	
	public Projet addProjet(String path, String nom) {
		Projet p = new Projet(this, path, nom);
		projets.add(p);
		for(IObserveur o: this.getObserveurs())((IObserveurNoyau)o).projetAjout(this, p);
		notifierModification();
		return p;
	}
	
	public void removeProjet(Projet p) {
		projets.remove(p);
		for(IObserveur o: this.getObserveurs())((IObserveurNoyau)o).projetRetire(this, p);
		notifierModification();
	}
	
	public List<Projet> getProjets() {
		return projets;
	}
	
	public Projet getProjet(String nom) {
		for(Projet p : projets)
			if(p.getNom().equals(nom))
				return p;
		return null;
	}
	
	public void deplacerPaquet(Paquet p, IConteneurPaquet conteneur) {
		((IConteneurPaquet)p.getPere()).removePaquet(p);
		conteneur.addPaquet(p);
	}
	public void deplacerClasse(Classe c, IConteneurClasse conteneur) {
		((IConteneurClasse)c.getPere()).removeClasse(c);
		conteneur.addClasse(c);
	}
	public void deplacerInterface(Interface c, IConteneurInterface conteneur) {
		((IConteneurInterface)c.getPere()).removeInterface(c);
		conteneur.addInterface(c);
	}
	public void deplacerEnum(Enum e, IConteneurEnum conteneur) {
		((IConteneurEnum)e.getPere()).removeEnum(e);
		conteneur.addEnum(e);
	}
	public void deplaceFonction(Fonction f, IConteneurFonction conteneur) {
		((IConteneurFonction)f.getPere()).removeFonction(f);
		conteneur.addFonction(f);
	}
	
	public void compiler() {
		for(Projet p : projets)
			p.compile();
	}

	// aucun effet
	public void supprimer() {
		
	}
}
