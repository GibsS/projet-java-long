package Application.GestionProjet;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.Algorithmique.*;
import Noyau.Arbre.Contrat.Conteneur.*;
import Noyau.Arbre.ElementJava.*;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.Projet.Noyau;
import Noyau.Modifieur.ExceptionModifieurIncorrect;
import Noyau.Modifieur.Modifieur;

public class FenetreDialogue {

	Noyau noyau;
	
	public FenetreDialogue(Noyau n) {
		this.noyau = n;
	}
	
	public static void renommerNoeud(INoeudNommable c) {
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nouveau nom : "),
    			nouveauNom,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Renommer", JOptionPane.PLAIN_MESSAGE);
    	c.setNom(nouveauNom.getText());
	}
	
	public static void ajouterProjet(Noyau c) {
		JTextField nouveauNom = new JTextField();
		JTextField chemin     = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom du projet : "),
    			nouveauNom,
    			new JLabel("Rentrez le path du projet : "),
    			chemin
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouveau paquet", JOptionPane.PLAIN_MESSAGE);
    	c.addProjet(chemin.getText(), nouveauNom.getText()).getPere();
	}
	
	public static void ajouterPaquet(IConteneurPaquet c) {
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom du paquet : "),
    			nouveauNom,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouveau paquet", JOptionPane.PLAIN_MESSAGE);
    	c.addPaquet(nouveauNom.getText()).getPere();
	}
	
	public static void ajouterClasse(IConteneurClasse c) {
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de la classe : "),
    			nouveauNom,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouvelle classe", JOptionPane.PLAIN_MESSAGE);
    	c.addClasse(nouveauNom.getText());
	}
	
	public static void ajouterInterface(IConteneurInterface c) {
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de l'interface : "),
    			nouveauNom,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouvelle interface", JOptionPane.PLAIN_MESSAGE);
    	c.addInterface(nouveauNom.getText());
	}
	
	public static void ajouterEnum(IConteneurEnum c) {
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de l'�num�ration : "),
    			nouveauNom,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouvelle �num�ration", JOptionPane.PLAIN_MESSAGE);
    	c.addEnum(nouveauNom.getText());
	}
	
	public static void ajouterAttribut(IConteneurAttribut c) {
		JTextField type = new JTextField();
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le type de l'attribut : "),
    			type,
    			new JLabel("Rentrez le nom de l'attribut : "),
    			nouveauNom,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouvelle attribut", JOptionPane.PLAIN_MESSAGE);
    	c.addAttribut(nouveauNom.getText(),type.getText());
	}
	
	public static void ajouterFonction(IConteneurFonction c) {
		JTextField type = new JTextField();
		JTextField nouveauNom = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le type de la fonction : "),
    			type,
    			new JLabel("Rentrez le nom de la fonction : "),
    			nouveauNom
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Nouvelle fonction", JOptionPane.PLAIN_MESSAGE);
    	c.addFonction(nouveauNom.getText(), type.getText());
	}
	
	public static void modifierClasse(Classe c) {
		JTextField nom = new JTextField();
		JTextField mod = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de la classe : "),
    			nom,
    			new JLabel("Rentrez les modifieurs de la classe : "),
    			mod
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer nom", JOptionPane.PLAIN_MESSAGE);
    	c.setNom(nom.getText());
    	
    	String[] parts = mod.getText().split(" ");
    	JOptionPane.showMessageDialog(null, inputs, "Changer modifieurs", JOptionPane.PLAIN_MESSAGE);
    	for(int i=0; i<c.getModifieurs().size(); i++) {
    		c.removeModifieur(c.getModifieurs().get(i));
    	}
    	for(int i=0; i<parts.length; i++) {
    		try {
    			c.addModifieur(new Modifieur(parts[i]));
    		}
    		catch(ExceptionModifieurIncorrect e) {
    		}
    	}
	}
	
	public static void modifierInterface(Interface interf) {
		JTextField nom = new JTextField();
		JTextField mod = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de l'interface : "),
    			nom,
    			new JLabel("Rentrez les modifieurs de l'interface : "),
    			mod
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer nom", JOptionPane.PLAIN_MESSAGE);
    	interf.setNom(nom.getText());
    	
    	String[] parts = mod.getText().split(" ");
    	JOptionPane.showMessageDialog(null, inputs, "Changer modifieurs", JOptionPane.PLAIN_MESSAGE);
    	for(int i=0; i<interf.getModifieurs().size(); i++) {
    		interf.removeModifieur(interf.getModifieurs().get(i));
    	}
    	for(int i=0; i<parts.length; i++) {
    		try {
    			interf.addModifieur(new Modifieur(parts[i]));
    		}
    		catch(ExceptionModifieurIncorrect e) {
    		}
    	}
	}
	
	public static void modifierEnumeration(Enum e) {
		JTextField nom = new JTextField();
		JTextField mod = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de l'énumération : "),
    			nom,
    			new JLabel("Rentrez les modifieurs de l'énumération : "),
    			mod
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer nom", JOptionPane.PLAIN_MESSAGE);
    	e.setNom(nom.getText());
    	
    	String[] parts = mod.getText().split(" ");
    	JOptionPane.showMessageDialog(null, inputs, "Changer modifieurs", JOptionPane.PLAIN_MESSAGE);
    	for(int i=0; i<e.getModifieurs().size(); i++) {
    		e.removeModifieur(e.getModifieurs().get(i));
    	}
    	for(int i=0; i<parts.length; i++) {
    		try {
    			e.addModifieur(new Modifieur(parts[i]));
    		}
    		catch(ExceptionModifieurIncorrect ex) {
    		}
    	}
	}
	
	public static void modifierAttribut(Attribut a) { 
		JTextField type = new JTextField();
		JTextField nom = new JTextField();
		JTextField mod = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de l'attribut : "),
    			nom,
    			new JLabel("Rentrez le type de l'attribut : "),
    			type,
    			new JLabel("Rentrez les modifieurs de l'attribut : "),
    			mod
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer nom", JOptionPane.PLAIN_MESSAGE);
    	a.setNom(nom.getText());
    	
    	JOptionPane.showMessageDialog(null, inputs, "Changer type", JOptionPane.PLAIN_MESSAGE);
    	a.setType(type.getText());
    	
    	String[] parts = mod.getText().split(" ");
    	JOptionPane.showMessageDialog(null, inputs, "Changer modifieurs", JOptionPane.PLAIN_MESSAGE);
    	for(int i=0; i<a.getModifieurs().size(); i++) {
    		a.removeModifieur(a.getModifieurs().get(i));
    	}
    	for(int i=0; i<parts.length; i++) {
    		try {
    			a.addModifieur(new Modifieur(parts[i]));
    		}
    		catch(ExceptionModifieurIncorrect e) {
    		}
    	}
	}
	
	public static void modifierFonction(Fonction f) {
		JTextField typeret = new JTextField();
		JTextField args = new JTextField();
		JTextField nom = new JTextField();
		JTextField mod = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez le nom de la fonction : "),
    			nom,
    			new JLabel("Rentrez le type de retour de la fonction : "),
    			typeret,
    			new JLabel("Rentrez les modifieurs de la fonction : "),
    			mod,
    			new JLabel("Rentrez les arguments et leurs types (ex : type1 arg1 type2 arg2) : "),
    			args,
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer nom", JOptionPane.PLAIN_MESSAGE);
    	f.setNom(nom.getText());
    	
    	JOptionPane.showMessageDialog(null, inputs, "Changer type retour", JOptionPane.PLAIN_MESSAGE);
    	f.setRetour(typeret.getText());
    	
    	String[] parts = mod.getText().split(" ");
    	JOptionPane.showMessageDialog(null, inputs, "Changer modifieurs", JOptionPane.PLAIN_MESSAGE);
    	for(int i=0; i<f.getModifieurs().size(); i++) {
    		f.removeModifieur(f.getModifieurs().get(i));
    	}
    	for(int i=0; i<parts.length; i++) {
    		try {
    			f.addModifieur(new Modifieur(parts[i]));
    		}
    		catch(ExceptionModifieurIncorrect e) {
    		}
    	}
    	
    	String[] parts2 = args.getText().split(" ");
    	JOptionPane.showMessageDialog(null, inputs, "Changer arguments", JOptionPane.PLAIN_MESSAGE);
    	for(int i=0; i<f.getArguments().size(); i++) {
    		f.removeArgument(f.getArguments().get(i));
    	}
    	for(int i=0; i<parts2.length-1; i=i+2) {
    		f.addArgument(parts2[i], parts2[i+1]);
    	}
	}
	
	public static void modifierDoWhile(DoWhile d) {
		JTextField expression = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez la condition du do while : "),
    			expression
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer Condition", JOptionPane.PLAIN_MESSAGE);
    	d.setCondition(new Expression(expression.getText()));
	}
	
	public static void modifierFor(For f) {
		JTextField init = new JTextField();
		JTextField cond = new JTextField();
		JTextField fin = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez l'instruction initiale : "),
    			init,
    			new JLabel("Rentrez la condition : "),
    			cond,
    			new JLabel("Rentrez l'instruction finale : "),
    			fin
    	};	
    	JOptionPane.showMessageDialog(null, inputs, "Changer instruction initiale", JOptionPane.PLAIN_MESSAGE);
    	Instruction instruction = new Instruction(f);
    	instruction.setContenu(init.getText());
    	f.setInstructionDebut(instruction);
    	
    	JOptionPane.showMessageDialog(null, inputs, "Changer condition", JOptionPane.PLAIN_MESSAGE);
    	f.setCondition(new Expression(cond.getText()));
    	
    	JOptionPane.showMessageDialog(null, inputs, "Changer instruction finale", JOptionPane.PLAIN_MESSAGE);   	
    	instruction = new Instruction(f);
    	instruction.setContenu(fin.getText());
    	f.setInstructionFin(instruction);
	}

	public static void modifierIf(If i) {
		JTextField cond = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez la condition du if : "),
    			cond
    	};	
    	
    	JOptionPane.showMessageDialog(null, inputs, "Changer condition if", JOptionPane.PLAIN_MESSAGE);
    	i.setCondition(new Expression(cond.getText()));
	}	
	
	public static void modifierWhile(While wh) {
		JTextField expression = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez la condition du while : "),
    			expression
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Changer Condition", JOptionPane.PLAIN_MESSAGE);
    	wh.setCondition(new Expression(expression.getText()));
	}
	
	public static void modifierSwitch(Switch sw) {
		JTextField[] cond = new JTextField[sw.getConditions().size()];
		
    	final JComponent[] inputs = new JComponent[sw.getConditions().size()*2];	
    	List<Expression> conditions = new ArrayList<Expression>();
    	conditions = (ArrayList<Expression>) sw.getConditions();
    	
    	for(int i = 0; i < cond.length; i++) {
    		inputs[2*i] = new JLabel("Rentrez la condition du cas " + i + " : ");
    		cond[i] = new JTextField();
    		cond[i].setText(conditions.get(i).toString());
    		inputs[i*2+1] = cond[i];      	 		
    	}
    	
    	JOptionPane.showMessageDialog(null, inputs, "Changer Conditions" , JOptionPane.PLAIN_MESSAGE);
    	
    	for(int i=0; i < cond.length; i++) {
    		sw.setExpression(new Expression(cond[i].getText()));   
    	}
	}	
	
	public static void ajouterCas(Switch sw) {
		JTextField expression = new JTextField();
    	final JComponent[] inputs = new JComponent[] {
    			new JLabel("Rentrez la condition du cas : "),
    			expression
    	};
    	JOptionPane.showMessageDialog(null, inputs, "Initialiser Condition", JOptionPane.PLAIN_MESSAGE);
    	sw.addCase(new Expression (expression.getText()), new Sequence(sw));
    	
	}
	
	public static void supprimerNoeud(INoeud noeud) {
		Object[] options = {"Oui",
                "Non"};
		int n = JOptionPane.showOptionDialog(null,
						"Voulez vous vraiment supprimer?",
						"Suppression",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[1]);
		if(n == 0){
			noeud.supprimer();
		}
	}
}
