package Noyau.Modifieur;

import java.util.List;

public interface IConteneurModifieur {

	List<Modifieur> getModifieurs();
	boolean hasModifieur(String modifieur);
	
	void addModifieur(Modifieur modifieur);
	void removeModifieur(Modifieur modifieur);
}
