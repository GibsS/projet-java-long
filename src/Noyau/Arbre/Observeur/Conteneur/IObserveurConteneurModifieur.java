package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Modifieur.IConteneurModifieur;
import Noyau.Modifieur.Modifieur;

public interface IObserveurConteneurModifieur extends IObserveur {

	void modifieurAjoute(IConteneurModifieur i, Modifieur a);
	void modifieurRetire(IConteneurModifieur i, Modifieur a);
}
