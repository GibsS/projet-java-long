package Logigramme.Observeur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Logigramme.SchemaLogigramme;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.Algorithmique.Expression;
import Noyau.Arbre.Algorithmique.For;
import Noyau.Arbre.Algorithmique.Sequence;
import Noyau.Arbre.Algorithmique.Switch;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurSwitch;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;

public class ObserveurSwitch implements IObserveurSwitch, IObserveurBoite {

	private List<ObserveurSequence> listObserveurSequence;
	
	private Boite boiteDebut;
	
	private List<Boite> listeBoite;
	
	private Boite boiteFin;
	
	private SchemaLogigramme schemaLogigramme;
		
	private Switch sw;

	public ObserveurSwitch(SchemaLogigramme schlogi, Switch sw) {
		this.listObserveurSequence = new ArrayList<ObserveurSequence>();
		this.schemaLogigramme = schlogi;
		this.sw=sw;
		/* Constructeur à compléter */
	}	

	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCase(Switch s, Expression e, Sequence se) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCase(Switch s, Expression e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeExpression(Switch s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier conditions");
    	JMenuItem ajout = new JMenuItem("ajouter cas");
    	JMenuItem suppr = new JMenuItem("supprimer");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierSwitch(sw);
			}
    	});
    	
	    ajout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.ajouterCas(sw);
			}
    	});  	
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sw.supprimer();
			}
    	});
    	
    	menu.add (modif);
    	menu.add(ajout);
    	menu.add (suppr);

    	menu.show(e.getComponent(), e.getX(), e.getY());
		
				
		
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}
