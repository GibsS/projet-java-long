package Noyau.Arbre.Observeur;

import Noyau.Arbre.INoeudNommable;

public interface IObserveurNommable extends IObserveur {

	void changeNom(INoeudNommable n);
}
