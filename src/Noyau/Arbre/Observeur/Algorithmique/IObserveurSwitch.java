package Noyau.Arbre.Observeur.Algorithmique;

import Noyau.Arbre.Algorithmique.Sequence;
import Noyau.Arbre.Algorithmique.Switch;
import Noyau.Arbre.Algorithmique.Expression;
import Noyau.Arbre.Observeur.IObserveur;

/** l'interface IObserveurSwitch représente un type d' observeur de structure switch.
 * Toutes les fonctions sont appelés pour signaler le changement correspondant.
 * @author gibson
 */
public interface IObserveurSwitch extends IObserveur {

	void addCase(Switch s, Expression e, Sequence se);
	void removeCase(Switch s, Expression e);
	void changeExpression(Switch s);
}
