package Noyau.Arbre.Observeur;

import Noyau.Arbre.INoeud;

public interface IObserveur {
	
	void update(INoeud n);
	void updateFils(INoeud n);
	void updatePetitFils(INoeud n);
}
