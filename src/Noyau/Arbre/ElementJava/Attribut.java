package Noyau.Arbre.ElementJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudFichier;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.INoeudProjet;
import Noyau.Arbre.INommableJava;
import Noyau.Arbre.ModeleNoeud;
import Noyau.Arbre.Algorithmique.Expression;
import Noyau.Arbre.Contrat.Conteneur.IConteneurAttribut;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conception.IObserveurAttribut;
import Noyau.Arbre.Projet.Projet;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.Dependance.ConteneurDependance;
import Noyau.Dependance.Dependance;
import Noyau.Dependance.IConteneurDependance;
import Noyau.GestionFichier.Fichier;
import Noyau.Modifieur.ConteneurModifieur;
import Noyau.Modifieur.IConteneurModifieur;
import Noyau.Modifieur.Modifieur;

/** la classe attribut repr�sente une instruction d'assignation ou d'initialisation de variable.
 * Les �l�ments repr�sent�s sont : le nom, le type, leur modifieur, leur d�pendance ainsi que leur valeur par d�faut.
 * @author gibson
 */
public class Attribut extends ModeleNoeud implements INoeudProjet, INoeudFichier, IConteneurModifieur, IConteneurDependance, INommableJava {

	String type;
	
	String nom;
	
	Expression valeur;
	
	ConteneurModifieur modifieurs;
	ConteneurDependance dependances;

	public Attribut(INoeud pere, String nom, String type) {
		super(pere);
		this.type = type;
		
		setNom(nom);
		
		modifieurs = new ConteneurModifieur(this);
		dependances = new ConteneurDependance(this);
		valeur = new Expression();
	}
	
	public Projet getProjet() {
		return ((INoeudProjet)getPere()).getProjet();
	}
	
	public void supprimer() {
		((IConteneurAttribut)getPere()).removeAttribut(this);
	}
	
	public void setNom(String nouveauNom) throws ExceptionMauvaisNom { 
		nom = ExceptionMauvaisNom.testNom(nouveauNom); 
		for(IObserveur o : getObserveurs())((IObserveurNommable)o).changeNom(this);
		notifierModification();
	}
	public String getNom() { return nom; }
	
	public String creerCode() {
		String res;
		res = modifieurs.toString();
		res += type + " " + getNom() + "; ";
		return res;
	}
	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		c.extraireEspace();
		for(Modifieur modifieur:c.extraireModifieurs()) {
			modifieurs.addModifieur(modifieur);
			c.extraireEspaceInutile();
		}
		type = c.lireMot();
		c.extraireEspaceInutile();
		try {
			setNom(c.lireMot());
		} catch(ExceptionMauvaisNom p) {
			
		}
		c.extraireEspaceInutile();
		try {
			c.extraireMot("=");
			c.extraireEspaceInutile();
			Code valeurCode = c.extraireVersMot(";");
			valeur.interpreterCode(valeurCode);
			c = new Code(";" + c.toString());
		} catch(ParseException p) {
			
		}
		c.extraireMot(";");
		return c;
		//throw new ParseException("attribut n'est pas d�fini");
	}

	/** setType : change le type de l'attribut.
	 * @param nom : le nouveau type.
	 */
	public void setType(String nom) { 
		type = nom; 
		for(IObserveur o : getObserveurs())((IObserveurAttribut)o).changementType(this);
		notifierModification();
	}
	/** setTypeInt : change le type � int.*/
	public void setTypeInt() { setType("int"); }
	/** setTypeFloat : change le type � float. */
	public void setTypeFloat() { setType("float"); }
	/** setTypeString : change le type � string. */
	public void setTypeString() { setType("string"); }

	public List<Modifieur> getModifieurs() { return modifieurs.getModifieurs(); }
	public boolean hasModifieur(String modifieur) { return modifieurs.hasModifieur(modifieur); }
	public void addModifieur(Modifieur modifieur) { modifieurs.addModifieur(modifieur); }
	public void removeModifieur(Modifieur modifieur) { modifieurs.removeModifieur(modifieur); }
	
	public List<IConteneurDependance> getSousDependant() { return null; }
	public List<Dependance> getDependance() { return dependances.getDependance(); }
	public List<Dependance> getAllDependance() { return dependances.getDependance(); }
	public void addDependance(Dependance dependance) { dependances.addDependance(dependance);}
	public void removeDependance(Dependance dependance) { dependances.removeDependance(dependance); }
	
	public List<String> getStringPath() {
		INoeudNommable courant = (INoeudNommable)getPere();
		List<String> chemin = new ArrayList<String>();
	
		while(!(courant instanceof Paquet)) {
			chemin.add(courant.getNom());
			courant = (INoeudNommable)courant.getPere();
		}
	
		Collections.reverse(chemin);
		return chemin;
	}

	public void compiler() {((INoeudFichier)getPere()).compiler();}

	public Fichier getFichier() { return ((INoeudFichier)getPere()).getFichier(); }
	public void setFichier(Fichier f) { }
}
