package Noyau.Arbre.Contrat.Conteneur;

import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Interface;

/** l'interface IConteneurAttribut d�finit un conteneur d'attribut, les classes et les interfaces
 * en font partie.
 * @author gibson
 */
public interface IConteneurInterface extends INoeudNommable {
	
	Interface addInterface(String nom);
	void addInterface(Interface i);
	
	Interface getInterface(String nom);
	List<Interface> getInterfaces();
	
	void removeInterface(Interface a);
	void detruireInterface(Interface a);
}
