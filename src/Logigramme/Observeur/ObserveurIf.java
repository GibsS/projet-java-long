package Logigramme.Observeur;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Logigramme.SchemaLogigramme;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.Algorithmique.For;
import Noyau.Arbre.Algorithmique.If;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurIf;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;

public class ObserveurIf implements IObserveurIf, IObserveurBoite {

	private ObserveurSequence observeurSequence;
	
	private Boite boiteDebut;
	
	private Boite boiteElse;
	
	private Boite boiteFin;
	
	private SchemaLogigramme schemaLogigramme;
	
	private If i;

	public ObserveurIf(SchemaLogigramme schlogi, If i) {
		this.observeurSequence = new ObserveurSequence(schlogi, i.getSequence());
		this.schemaLogigramme = schlogi;
		this.i = i;
		/* Constructeur à compléter */
	}
	
	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeCondition(If i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addElse(If i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeElse(If i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addElseIf(If i, If other) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeElseIf(If i, If other) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier condition");
    	JMenuItem elsif = new JMenuItem("ajouter elsif");
    	JMenuItem suppr = new JMenuItem("supprimer");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierIf(i);
			}
    	});
    	
	    elsif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				i.addElseIf();
			}
    	});  	
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				i.supprimer();
			}
    	});
    	
    	menu.add (modif);
    	menu.add(elsif);
    	menu.add (suppr);

    	menu.show(e.getComponent(), e.getX(), e.getY());
		
		
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
