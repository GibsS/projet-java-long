package Schema.Observeur;

import java.awt.event.MouseEvent;

import Schema.Boite;
import Schema.Fleche;

public interface IObserveurSchema {

	void mouseClicked(MouseEvent e);
	
	void cliqueDroitCaseVide(int x, int y) ;
	void cliqueGaucheCaseVide(int x, int y);
	
	void flecheFini(Fleche f);
	void dragFini(Boite b);
}
