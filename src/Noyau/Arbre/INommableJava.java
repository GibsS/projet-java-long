package Noyau.Arbre;

public interface INommableJava extends INommable {

	void setNom(String nom) throws ExceptionMauvaisNom;
	
	@SuppressWarnings("serial")
	public class ExceptionMauvaisNom extends RuntimeException {

		public static String testNom(String nom) throws ExceptionMauvaisNom {
			char[] array = nom.toCharArray();
			int i = 0;
			int debut;
			int fin;
			while(i < nom.length() && array[i] == ' ')
				i++;
			
			if(i == nom.length())
				throw new ExceptionMauvaisNom();
			
			debut = i;
			if(array[i] == 0 || array[i] == 1 || array[i] == 2 || array[i] == 3 || array[i] == 4 || array[i] == 5 ||
			   array[i] == 6 || array[i] == 7 || array[i] == 8 || array[i] == 9)
				throw new ExceptionMauvaisNom();
			
			while(i < array.length && array[i] != ' ')
				i++;
			
			fin = i;
			
			while(i < nom.length() && array[i] == ' ')
				i++;
			
			if(i != nom.length())
				throw new ExceptionMauvaisNom();
			else 
				return nom.substring(debut, fin);
		}
	}
}
