package Noyau.Arbre.Observeur.Algorithmique;

import Noyau.Arbre.Algorithmique.DoWhile;
import Noyau.Arbre.Observeur.IObserveur;

/** l'interface IObserveurDoWhile repr�sente un type qui peut suivre la progression d'un dowhile.
 * @author gibson
 */
public interface IObserveurDoWhile extends IObserveur {

	/** changeExpression : est appel� quand le dowhile "d" a chang� d'expression.
	 * @param d : le dowhile qui a chang�.
	 */
	void changeExpression(DoWhile d);
}
