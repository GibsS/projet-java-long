package Noyau.GestionFichier;

import java.util.ArrayList;
import java.util.List;

import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.Dependance.Dependance;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Fichier {
	
	ProjetMachine projet;
	long date;
	File file;
	
	List<Classe> classes;
	List<Interface> interfaces;
	List<Enum> enums;
	
	public Fichier(ProjetMachine p, File file, boolean fichierExterne) {
		this.projet = p;
		this.file = file;
		
		classes = new ArrayList<Classe>();
		interfaces = new ArrayList<Interface>();
		enums = new ArrayList<Enum>();
		if(fichierExterne)
			interpreterCode();
		else
			date = file.lastModified();
	}
	
	public void update() {
		//if(!file.exists())
		//	supprimer();
		/*else*/ if(date != file.lastModified()){
			interpreterCode();
		}
	}
	
	public void interpreterCode() {
		System.out.println("[interpreterCode] mise � jour de : " + file.getName() + " path : " + file.getAbsolutePath());
		System.out.println("[A faire] impl�menter la d�compilation");
		
		Code code = new Code(getContenu());
		Code nomPaquetCode;
		List<String> nomPaquets = new ArrayList<String>();
		Paquet paquet;
		Paquet courant;
		
		code.extraireEspaceInutile();
		try {
			code.extraireMot("package");
			code.extraireEspace();
			nomPaquetCode = code.extraireVersMot(";");
			while(!nomPaquetCode.isEmpty() && nomPaquetCode.toString() != "") {
				try {
					nomPaquets.add(code.extraireVersMot(".").toString());
				} catch (ParseException p) {
					
				}
				nomPaquets.add(code.toString());
			}
		} catch (ParseException p) {
			
		}
		
		paquet = new Paquet(null,"racine");
		courant = paquet;
		for(String s: nomPaquets) {
			courant = courant.addPaquet(s);
		}
		
		while(true) {
			try {
				code.extraireMot("import");
				code.extraireEspace();
				code.extraireVersMot(";");
			} catch(ParseException p) {
				break;
			}
		}
		while(!code.isEmpty() && code.toString() != "") {
			try {
				Classe nouvelleClasse = new Classe(courant, "tmp");
				code = nouvelleClasse.interpreterCode(code);
				this.addClasse(nouvelleClasse);
				continue;
			} catch (ParseException p) {
			
			}
			try {
				Interface nouvelleInterface = new Interface(courant, "tmp");
				code = nouvelleInterface.interpreterCode(code);
				this.addInterface(nouvelleInterface);
				continue;
			} catch (ParseException p) {
			
			}
			try {
				Enum nouvelleEnum = new Enum(courant, "tmp");
				code = nouvelleEnum.interpreterCode(code);
				this.addEnum(nouvelleEnum);
				continue;
			} catch (ParseException p) {
			
			}
		}
		date = file.lastModified();
	}
	
	public String getNom() {
		String nom = file.getName();
		return nom.substring(0, nom.indexOf("."));
	}
	
	public File getFile() {
		return file;
	}
	
	public String getPath() {
		return file.getAbsolutePath();
	}
	
	public void creerCode() {
		String res = "";
		
		List<Dependance> dependance = new ArrayList<Dependance>();
		
		for(Classe c:classes) {
			dependance.addAll(c.getAllDependance());
			res += c.creerCode() + "\n";
		}
		for(Interface i:interfaces) {
			dependance.addAll(i.getAllDependance());
			res += i.creerCode() + "\n";
		}
		for(Enum e: enums) {
			res += e.creerCode()  + "\n";
		}
		
		if(!dependance.isEmpty()) {
		res = "\n" + res;
		
		for(Dependance c:dependance)
			res = "import " + c + ";\n" + res;
		
		}
		setContenu(res);
	}
	
	public String getContenu() {
		try {
			return new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void setContenu(String contenu) {
		try {
			file.getParentFile().mkdirs();
			Files.write(Paths.get(file.getAbsolutePath()), contenu.getBytes());
			date = file.lastModified();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<Classe> getClasses() { return classes; }
	public List<Interface> getInterfaces() { return interfaces; }
	public List<Enum> getEnums() { return enums; }
	
	public void addClasse(Classe c) { classes.add(c); }
	public void addInterface(Interface i) { interfaces.add(i); }
	public void addEnum(Enum e) { enums.add(e); }
	
	public void removeClasse(Classe c) {classes.remove(c); if(estVide())supprimer();}
	public void removeInterface(Interface c) {interfaces.remove(c); if(estVide())supprimer();}
	public void removeEnum(Enum c) {enums.remove(c); if(estVide())supprimer();}
	
	public boolean estVide() { return classes.isEmpty() && interfaces.isEmpty() && enums.isEmpty(); }
	
	public void supprimer() {
		for(Classe c : classes)
			c.supprimer();
		for(Interface i: interfaces)
			i.supprimer();
		for(Enum e: enums)
			e.supprimer();
		
		try {
			if(file.exists())
				Files.delete(Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		projet.retirerFichier(this);
	}
}