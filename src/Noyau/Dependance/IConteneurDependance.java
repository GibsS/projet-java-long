package Noyau.Dependance;

import java.util.List;

public interface IConteneurDependance {

	List<IConteneurDependance> getSousDependant();
	List<Dependance>           getDependance();
	List<Dependance>		   getAllDependance();
	
	void addDependance(Dependance dependance);
	void removeDependance(Dependance dependance);
}
