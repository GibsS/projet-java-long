package Logigramme.Observeur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Logigramme.SchemaLogigramme;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.Algorithmique.For;
import Noyau.Arbre.Algorithmique.While;
import Noyau.Arbre.Observeur.IObserveur;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;

public class ObserveurWhile implements IObserveur, IObserveurBoite  {

	private ObserveurSequence observeurSequence;
	
	private Boite boiteDebut;
	
	private Boite boiteFin;
	
	private SchemaLogigramme schemaLogigramme;

	private While wh;

	public ObserveurWhile(SchemaLogigramme schlogi, While wh) {
	this.observeurSequence = new ObserveurSequence(schlogi, wh.getContenu());
	this.schemaLogigramme = schlogi;
	this.wh = wh;
	/* Constructeur à compléter */
}	
	
	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier condition");
    	JMenuItem suppr = new JMenuItem("supprimer");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierWhile(wh);
			}
    	});
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wh.supprimer();
			}
    	});
    	
    	menu.add (modif);
    	menu.add (suppr);

    	menu.show(e.getComponent(), e.getX(), e.getY());		
		
	}
}
