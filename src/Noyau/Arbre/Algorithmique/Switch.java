package Noyau.Arbre.Algorithmique;

import java.util.ArrayList;
import java.util.List;

import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurSwitch;
import Noyau.Arbre.Algorithmique.Switch;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;

/** la classe Switch represente un switch java : son expression a �valuer, les expressions
 * auxquelles on compare l'expression et les contenus.
 * @author gibson
 */
public class Switch extends Algorithme {
	Expression expressionTeste;
	ArrayList<Expression> conditions;
	ArrayList<Sequence> contenus;
	
	public Switch(Sequence pere) {
		super(pere);
		conditions = new ArrayList<Expression>();
		contenus = new ArrayList<Sequence>();
	}

	public String creerCode() {
		String res = "switch (" + expressionTeste.creerCode() + ") {\n";
		
		for(int i = 0; i < conditions.size(); i++) {
			res += Code.tabuler("case " + conditions.get(i).creerCode() + " :\n" + Code.tabuler(contenus.get(i).creerCode()));
		}
		res += "}\n";
		return res;
	}

	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code expressionCode;
		Code crochetCode;
		Code expressionCaseCode;
		Code contenuCaseCode;

		contenus.clear();
		conditions.clear();
		
		c.extraireEspaceInutile();
		c.extraireMot("switch");
		c.extraireEspaceInutile();
		expressionCode = c.extraireParenthese();
		expressionTeste = new Expression();
		expressionTeste.interpreterCode(expressionCode);
		c.extraireEspaceInutile();
		crochetCode = c.extraireCrochet();
		crochetCode.extraireEspaceInutile();
		crochetCode.extraireMot("case");
		while(true) {
			crochetCode.extraireEspace();
			expressionCaseCode = crochetCode.extraireVersMot(":");
			Expression expression = new Expression();
			Sequence   sequence = new Sequence(this);
			expression.interpreterCode(expressionCaseCode);
			try {
				contenuCaseCode = crochetCode.extraireVersMot("case");
			} catch(ParseException p) {
				sequence.interpreterCode(crochetCode);
				conditions.add(expression);
				contenus.add(sequence);
				break;
			}
			sequence.interpreterCode(contenuCaseCode);
			conditions.add(expression);
			contenus.add(sequence);	
		}
		
		return c;
	}
	
	public void setExpression(Expression e) {
		expressionTeste = e;
		for(IObserveur o : getObserveurs())((IObserveurSwitch)o).changeExpression(this);
		notifierModification();
	}
	
	public void addCase(Expression e, Sequence s) {
		conditions.add(e);
		contenus.add(s);
		for(IObserveur o : getObserveurs())((IObserveurSwitch)o).addCase(this, e, s);
		notifierModification();
	}
	
	public void removeCase(Expression e) {
		int i = conditions.indexOf(e);
		conditions.remove(e);
		contenus.remove(i);
		for(IObserveur o : getObserveurs())((IObserveurSwitch)o).removeCase(this, e);
		notifierModification();
	}
	
	public List<Expression> getConditions() {
		return conditions;
	}
	
	public List<Sequence> getContenus() {
		return contenus;
	}
}
