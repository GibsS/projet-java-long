package Noyau.Arbre.Observeur.Conception;

import Noyau.Arbre.ElementJava.Attribut;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conteneur.*;

public interface IObserveurAttribut extends IObserveurNommable, IObserveurConteneurModifieur, 
										  IObserveurConteneurDependance {
	void changementType(Attribut a);
}

