package Noyau.Arbre;

public interface INommable {

	public String getNom();
	public void setNom(String nom);
}
