package Noyau.Arbre.Observeur.Algorithmique;

import Noyau.Arbre.Algorithmique.Algorithme;
import Noyau.Arbre.Algorithmique.Sequence;
import Noyau.Arbre.Observeur.IObserveur;

/** l'interface IObserveurSequence repr�sente un type d'observeur de s�quence.
 * Toutes les fonctions sont appel�s pour signaler le changement correspondant.
 * @author gibson
 */
public interface IObserveurSequence extends IObserveur {

	void addAlgorithme(Sequence s, Algorithme a);
	void removeAlgorithme(Sequence s, Algorithme a);
	
	void addAlgorithme(Sequence s, Algorithme previous, Algorithme a);
}
