package Schema;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import Schema.Schema.SettingSchema;

public class Contexte {

	private Schema schema;
	private SettingSchema settings;
	
	private int x;
	private int y;
	private int largeur;
	private int hauteur;
	
	List<Contexte> contextes;
	
	boolean dessiner;
	String texte;
	
	public Contexte(int x, int y, int largeur, int hauteur, Schema schema) {
		this.x = x;
		this.y = y;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.schema = schema;
		this.settings = schema.settings;
		
		contextes = new ArrayList<Contexte>();
	}
	
	public void setX(int x) { this.x = x; }
	public void setY(int y) { this.y = y; }
	
	public void setLargeur(int x) { this.largeur = x; }
	public void setHauteur(int x) { this.hauteur = x; }
	
	public int getX() { return x; } 
	public int getY() { return y; }
	
	public int getLargeur() { return x; }
	public int getHauteur() { return y; }
	
	public void addContexte(Contexte c) { contextes.add(c); }
	public void removeContexte(Contexte c) { contextes.remove(c); }
	
	public void enableDraw() { dessiner = true; }
	public void disableDraw() { dessiner = false; }
	
	public void afficherContenu(Graphics g) {
		if(dessiner) {
			Point start = schema.gridToScreen(new Point(x,y));
			g.drawRoundRect(start.x, start.y, largeur*settings.getLargeurPixel(), hauteur*settings.getHauteurPixel(), 10, 10);
		}
		for(Contexte c: contextes)
			c.afficherContenu(g);
	}
}
