package Noyau.Arbre.Algorithmique;

import java.util.List;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudFichier;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.GestionFichier.Fichier;

/** la classe Instruction contient le code d'une instruction (priv�e de son point virgule)
 * stocke l'information sous la forme d'un string.
 * @author gibson
 */
public class Instruction extends Algorithme {

	String contenu;
	
	Algorithme pere;
	
	public Instruction(INoeud pere) {
		super(pere);
	}
	
	public String creerCode() {
		return contenu;
	}
	
	@Override
	public Code interpreterCode(Code code) throws ParseException {
		contenu = code.toString();
		return new Code("");
	}
	
	public String getContenu() {
		return contenu;
	}
	
	public void setContenu(String contenu) {
		this.contenu = contenu;
		notifierModificationFils(this);
	}
	
	public INoeud getPere() { return pere; }
	public void setPere(INoeud n) { pere = (Algorithme) n; }

	public void compiler() { ((INoeudFichier)getPere()).compiler(); }

	public Fichier getFichier() { return ((INoeudFichier)getPere()).getFichier(); }
	public void setFichier(Fichier f) {	}

	public void supprimer() { ((Sequence)getPere()).contenu.remove(this); }

	public void notifierModificationFils(INoeud n) {
		//pere.notifierModificationGrandFils(n);
	}
	public void notifierModificationGrandFils(INoeud n) {
		//pere.notifierModificationGrandFils(n);	
	}

	public List<IObserveur> getObserveurs() {
		return null;
	}

	public void addObserveur(IObserveur o) {
		
	}

	public void removeObserveur(IObserveur o) {
		
	}

	@Override
	public void notifierModification() {
		// TODO Auto-generated method stub
		
	}

}
