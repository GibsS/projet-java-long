package UML;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import Application.Application;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.Observeur.Conception.IObserveurPaquet;
import Noyau.Arbre.Observeur.Projet.IObserveurNoyau;
import Noyau.Arbre.Projet.Noyau;
import Noyau.Arbre.Projet.Projet;
import Schema.*;
import Schema.Observeur.IObserveurSchema;
import UML.Fleche.AggregationFleche;
import UML.Fleche.CompositionFleche;
import UML.Fleche.HeritageFleche;
import UML.Fleche.ImplementationFleche;
import UML.Observeur.ObserveurPaquet;

@SuppressWarnings("serial")
public class SchemaUML extends JPanel implements IObserveurNoyau, IObserveurSchema {
	
	private Application application;
	private BarreChoix barreChoix; // la barre de choix
	private Schema schema; // le schema
	private List<ObserveurPaquet> observeurProjet; // la liste d'observateurs de paquets
	
	private Noyau noyau; // le noyau
	
	final public static int largeurBoiteElementaire = 100; // la largeur des boite elementaires
	final public static int hauteurBoiteElementaire = 100; // la hauteur des boite elementaires
	final public static int nombreBoiteLargeur = 20; // le nombre de boites sur la largeur
	final public static int nombreBoiteHauteur = 15; // le nombre de boites sur la hauteur
	
	private boolean isboite;
	private typeBoite boiteAPlacer;
	
	public enum typeBoite { Classe, Interface, Enum }
	
	public SchemaUML(Application a) {
		this.application = a;
		this.setLayout(new BorderLayout());
		
		creerBarre();
		creerSchema();
	}
	
	private void creerBarre() {
		this.barreChoix = new BarreChoix(2,100,1200);
		
		JButton boutonClasse = new JButton("Classe");
		boutonClasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = true;
				schema.disableFleche();
				boiteAPlacer = typeBoite.Classe;
			}
		});
		boutonClasse.setSize(100,80);
		JButton boutonInterface = new JButton("Interface");
		boutonInterface.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = true;
				schema.disableFleche();
				boiteAPlacer = typeBoite.Interface;
			}
		});
		boutonInterface.setSize(100,80);
		JButton boutonEnumeration = new JButton("Enumeration");
		boutonEnumeration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = true;
				schema.disableFleche();
				boiteAPlacer = typeBoite.Enum;
			}
		});
		boutonEnumeration.setSize(100,80);
		
		JButton boutonHeritage = new JButton("Heritage");
		boutonHeritage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = false;
				schema.enableFleche(new HeritageFleche(schema));
			}
		});
		boutonHeritage.setSize(100,80);
		JButton boutonAgregation = new JButton("Agregation");
		boutonAgregation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = false;
				schema.enableFleche(new AggregationFleche(schema));
			}
		});
		boutonAgregation.setSize(100,80);
		JButton boutonComposition = new JButton("Composition");
		boutonComposition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = false;
				schema.enableFleche(new CompositionFleche(schema));
			}
		});
		boutonComposition.setSize(100,80);
		JButton boutonImplementation = new JButton("Implementation");
		boutonImplementation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isboite = false;
				schema.enableFleche(new ImplementationFleche(schema));
			}
		});
		boutonImplementation.setSize(100,80);
		
		barreChoix.ajouterBouton(boutonClasse,0);
		barreChoix.ajouterBouton(boutonInterface,0);
		barreChoix.ajouterBouton(boutonEnumeration,0);
		barreChoix.ajouterBouton(boutonHeritage,1);
		barreChoix.ajouterBouton(boutonAgregation,1);
		barreChoix.ajouterBouton(boutonComposition,1);
		barreChoix.ajouterBouton(boutonImplementation,1);

		this.add(barreChoix,BorderLayout.NORTH);
	}
	private void creerSchema() {
		this.schema = new Schema(largeurBoiteElementaire,hauteurBoiteElementaire,nombreBoiteLargeur,nombreBoiteHauteur);
		this.add(schema,BorderLayout.CENTER);
		
		schema.enableDragging();
		schema.enableMovement();
		schema.disableFleche();
		schema.addObserveur(this);
	}
	
	public Application getApplication() {
		return application;
	}
	public Schema getSchema() {
		return this.schema;
	}
	public BarreChoix getBarreChoix() {
		return this.barreChoix;
	}
	
	public Noyau getNoyau() {
		return this.noyau;
	}
	
	@Override
	public void update(INoeud n) {
		
	}
	@Override
	public void updateFils(INoeud n) {
		
	}
	@Override
	public void updatePetitFils(INoeud n) {
		
	}

	@Override
	public void projetRetire(Noyau n, Projet p) {
		for(ObserveurPaquet o : observeurProjet)
			if(o.getPaquet().getNom().equals(p.getNom())) {
				o.supprimer();
				observeurProjet.remove(o);
				break;
			}				
	}
	@Override
	public void projetAjout(Noyau n, Projet p) {
		ObserveurPaquet o = new ObserveurPaquet(this, p);
		observeurProjet.add(o);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroitCaseVide(int x, int y) {
		//System.out.println("[SchemaUML] clique droit : " + x + " " + y + " " + isboite);
		
	}
	@Override
	public void cliqueGaucheCaseVide(int x, int y) {

	}

	@Override
	public void flecheFini(Fleche f) {
		
	}
	@Override
	public void dragFini(Boite b) {
		
	}
}
