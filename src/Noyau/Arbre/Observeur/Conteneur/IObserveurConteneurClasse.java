package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Contrat.Conteneur.IConteneurClasse;
import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.Observeur.IObserveur;

public interface IObserveurConteneurClasse extends IObserveur {

	void classeAjoute(IConteneurClasse i, Classe a);
	void classeRetire(IConteneurClasse i, Classe a);
}
