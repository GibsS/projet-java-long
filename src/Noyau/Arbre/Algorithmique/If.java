package Noyau.Arbre.Algorithmique;

/** La classe If contient toutes les informations relatives � un if : la condition d'ex�cution,
 * la s�quence qui s'ex�cute en cas de validation de l'expression, le contenu du else si il existe
 * et une r�f�rence au else if si il existe.
 */
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurIf;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;

public class If extends Algorithme {
	Expression condition;
	If suivant;
	Sequence contenu;
	Sequence elseContenu;
	
	public If(Algorithme pere) {
		super(pere);
		condition = new Expression();
		suivant = null;
		contenu = new Sequence(this);
		elseContenu = null;
	}

	public String creerCode() {
		return "if (" + condition.creerCode() + ") "
				+ "{\n" + Code.tabuler(contenu.creerCode()) + "} " 
				+ (suivant != null?"else " +suivant.creerCode(): (elseContenu != null? "else {\n" + Code.tabuler(elseContenu.creerCode()) + "}\n":"")); 
	}

	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
			Code elseCode;
			Code conditionCode;
			Code contenuCode;
			boolean suite = true;
		
			c.extraireEspaceInutile();
			c.extraireMot("if");
			c.extraireEspaceInutile();
			conditionCode = c.extraireParenthese();
			condition.interpreterCode(conditionCode);
			c.extraireEspaceInutile();
			contenuCode = c.extraireCrochet();
			contenu.interpreterCode(contenuCode);
			c.extraireEspaceInutile();
			try {
				c.extraireMot("else");
			} catch(ParseException e) {
				suite = false;
			}
			if(suite) {
				try {
					If i = new If((Sequence)getPere());
					c.extraireEspace();
					c = i.interpreterCode(c);
					suivant = i;
					if(getPere() != null)
						((Sequence)getPere()).addMilieu(this, i);
				} catch(ParseException e) {
					Sequence s = new Sequence(this);
					c.extraireEspace();
					elseCode = c.extraireCrochet();
					s.interpreterCode(elseCode);
					elseContenu = s;
				}
			}
		return c;
	}
	
	public void setCondition(Expression c) {
		condition = c;
		for(IObserveur o : getObserveurs()) ((IObserveurIf)o).changeCondition(this);
		notifierModification();
	}
	
	public void addElse() {
		elseContenu = new Sequence(this);
		for(IObserveur o : getObserveurs()) ((IObserveurIf)o).addElse(this);
		notifierModification();
	}
	
	public void removeElse() {
		elseContenu = null;
		for(IObserveur o : getObserveurs()) ((IObserveurIf)o).removeElse(this);
		notifierModification();
	}
	
	public void addElseIf() {
		suivant = new If((Algorithme)this.getPere());
		for(IObserveur o : getObserveurs()) ((IObserveurIf)o).addElseIf(this,suivant);
		notifierModification();
	}
	
	public void removeElseIf() {
		suivant.supprimer();
		suivant = null;
		for(IObserveur o : getObserveurs()) ((IObserveurIf)o).removeElseIf(this,suivant);
		notifierModification();
	}
	
	public Sequence getSequence() {
		return contenu;
	}
	public Sequence getElseSequence() {
		return elseContenu;
	}
	public If getElseIf() {
		return suivant;
	}
	public boolean hasElseSequence() {
		return elseContenu != null;
	}
	public boolean hasElseIf() {
		return suivant != null;
	}
}
