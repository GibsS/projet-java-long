package Logigramme.Observeur;

import Logigramme.SchemaLogigramme;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.Algorithmique.Algorithme;
import Noyau.Arbre.Algorithmique.Sequence;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurSequence;
import Schema.Boite;

public class ObserveurSequence implements IObserveurSequence {

	private Boite boiteDebut;
	
	private Boite boiteFin;
	
	private SchemaLogigramme schemaLogigramme;

	private Sequence seq;

	public ObserveurSequence(SchemaLogigramme schlogi, Sequence seq) {
	this.schemaLogigramme = schlogi;
	this.seq = seq;
	/* Constructeur à compléter */
}
	
	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAlgorithme(Sequence s, Algorithme a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeAlgorithme(Sequence s, Algorithme a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAlgorithme(Sequence s, Algorithme previous, Algorithme a) {
		// TODO Auto-generated method stub
		
	}
	
}
