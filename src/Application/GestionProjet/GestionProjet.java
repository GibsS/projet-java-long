package Application.GestionProjet;

import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.Contrat.Conteneur.*;
import Noyau.Arbre.ElementJava.*;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Projet.Noyau;
import Noyau.Arbre.Projet.Projet;

@SuppressWarnings("serial")
public class GestionProjet extends JScrollPane implements IObserveur {

	Noyau noyau; // le noyau : repr�sente les �l�ments java avec lesquels l'utilisateur peut int�ragir
	ArbreProjet arbre; // l'arbre d'affichage
	
	// permet de cr�er un arbre d'affichage si pourvu d'un noyau non null  [fonction fini]
	public GestionProjet(Noyau n) {
		noyau = n;
		
		arbre = new ArbreProjet(creerArbreNoyau(n));
		this.setViewportView(arbre);
		
		n.addObserveur(this);
	}
	
	public DefaultMutableTreeNode creerArbreNoyau(Noyau n) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		
		node.setUserObject(null);
		for(Projet p:n.getProjets()) {
			node.add(creerArbreProjet(p));
		}
		
		return node;
	}
	
	// g�n�re un arbre repr�sentant un projet  [fonction fini]
	public DefaultMutableTreeNode creerArbreProjet(Projet c) {
		//System.out.println("[projet] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = creerArbrePaquet(c);
		node.setUserObject(c);
		return node;
	}
	
	// g�n�re un arbre repr�sentant un paquet  [fonction fini]
	public DefaultMutableTreeNode creerArbrePaquet(Paquet c) {
		//System.out.println("[paquet] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		node.setUserObject(c);
		
		for(Interface i:c.getInterfaces())
			node.add(creerArbreInterface(i));
		for(Classe c1:c.getClasses())
			node.add(creerArbreClasse(c1));
		for(Enum i:c.getEnums())
			node.add(creerArbreEnum(i));
		for(Paquet c1:c.getPaquets())
			node.add(creerArbrePaquet(c1));
		
		return node;
	}
	
	// g�n�re un arbre repr�sentant une classe  [fonction fini]
	public DefaultMutableTreeNode creerArbreClasse(Classe c) {
		//System.out.println("[classe] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		
		node.setUserObject(c);
		for(Attribut a:c.getAttributs())
			node.add(creerArbreAttribut(a));
		for(Fonction f:c.getFonctions())
			node.add(creerArbreFonction(f));
		for(Interface i:c.getInterfaces())
			node.add(creerArbreInterface(i));
		for(Classe c1:c.getClasses())
			node.add(creerArbreClasse(c1));
		
		return node;
	}
	
	// g�n�re un arbre repr�sentant une interface  [fonction fini]
	public DefaultMutableTreeNode creerArbreInterface(Interface c) {
		//System.out.println("[interface] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		
		node.setUserObject(c);
		for(Attribut a:c.getAttributs())
			node.add(creerArbreAttribut(a));
		for(Fonction f:c.getFonctions())
			node.add(creerArbreFonction(f));
		
		return node;
	}
	
	// g�n�re un arbre repr�sentant un attribut  [fonction fini]
	public DefaultMutableTreeNode creerArbreAttribut(Attribut c) {
		//System.out.println("[attribut] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		node.setUserObject(c);
		return node;
	}
	
	// g�n�re un arbre repr�sentant une fonction  [fonction fini]
	public DefaultMutableTreeNode creerArbreFonction(Fonction c) {
		//System.out.println("[fonction] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		node.setUserObject(c);
		return node;
	}
	
	// g�n�re un arbre repr�sentant une �num�ration  [fonction fini]
	public DefaultMutableTreeNode creerArbreEnum(Enum c) {
		//System.out.println("[enum] "+((INoeudNommable)c).getNom());
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		node.setUserObject(c);
		return node;
	}
	
	// r�cup�re le noeud de l'arbre associ� � l'objet java "e"  [fonction fini]
	public DefaultMutableTreeNode getNode(INoeud e) {
		List<INoeud> elements = new ArrayList<INoeud>();// = e.getObjectPath();
		INoeud courant = e;
		
		while(e != null) {
			elements.add(e);
			courant = courant.getPere();
		}
		
		Collections.reverse(elements);
		
		DefaultMutableTreeNode node = arbre.top;
		@SuppressWarnings("rawtypes")
		Enumeration noeuds = null;
		
		for(INoeud j:elements) {
			noeuds = node.preorderEnumeration();
			while(noeuds.hasMoreElements()) { 
				node = (DefaultMutableTreeNode) noeuds.nextElement();
				if(j == node.getUserObject())
					break;
			}
		}
		return node;
	}
	
	// supprime le noeud de l'arbre associ� � c  [fonction fini]
	public void suppressionNoeud(INoeud c) {
		DefaultMutableTreeNode node = getNode(c);
		node.removeFromParent();
	}
	
	// ajoute un �l�ment java � l'arbre  [fonction fini]
	public void ajoutNoeud(INoeud j) {
		DefaultMutableTreeNode node;
		if(j instanceof Classe)
			node = creerArbreClasse((Classe)j);
		else if(j instanceof Interface) 
			node = creerArbreInterface((Interface)j);
		else if(j instanceof Paquet)
			node = creerArbrePaquet((Paquet)j);
		else if(j instanceof Attribut) 
			node = creerArbreAttribut((Attribut)j);
		else if(j instanceof Fonction)
			node = creerArbreFonction((Fonction)j);
		else if(j instanceof Enum)
			node = creerArbreEnum((Enum)j);
		else
			throw new RuntimeException("mauvais type de noeud");
		
		DefaultMutableTreeNode pere = getNode(j.getPere());
		pere.add(node);
	}
	
	// d�place un �l�ment java dans l'arbre d'affichage : l'�l�ment java a �t� chang� dans le noyau donc le p�re 
	// actuel est celui qu'il aura dor�navant  [fonction fini]
	public void deplacerNoeud(INoeud perePrecedent, INoeud j) {
		DefaultMutableTreeNode node = getNode(perePrecedent);
		@SuppressWarnings("rawtypes")
		Enumeration noeuds = node.preorderEnumeration();
		while(noeuds.hasMoreElements() && (node = (DefaultMutableTreeNode) noeuds.nextElement()).getUserObject() == j) { ; }
		node.removeFromParent();
		ajoutNoeud(j);
	}
	
	// l'arbre d'affichage � proprement parl�, on en cr�� notre propre version car on veut pouvoir afficher de notre propre
	// mani�re les donn�es de chaque noeud est peut �tre aussi l'icone de chaque noeud de l'arbre.
	class ArbreProjet extends JTree {
		
		DefaultMutableTreeNode top;
		
		public ArbreProjet(DefaultMutableTreeNode d) {
			super(d);
			top = d;
			this.addMouseListener (new Selector(this));
		}
		
		public String convertValueToText(Object noeud, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			INoeudNommable n = (INoeudNommable)((DefaultMutableTreeNode)noeud).getUserObject();
			if(n != null)
				return n.getNom();
			else 
				return "projets";
		}
	}
	
	// Cette classe permet de g�rer les diff�rentes formes d'input offerte � l'utilisateur : 
	// - changement de nom
	// - suppression
	// - ajout d'�l�ment
	// (- d�placement, mais un peu trop compliqu� � faire)
	class Selector extends MouseAdapter {
		
		ArbreProjet arbre;
		
		public Selector(ArbreProjet a) {
			arbre = a;
		}
		
	    public void mousePressed ( MouseEvent e ) {
	        INoeud noeud;  
	    	
	    	if ( SwingUtilities.isRightMouseButton ( e ) ) {
	    		TreePath path = arbre.getPathForLocation ( e.getX (), e.getY () );
	        	Rectangle pathBounds = arbre.getUI ().getPathBounds ( arbre, path );
	        	if ( pathBounds != null && pathBounds.contains ( e.getX (), e.getY () ) ) {
	            	noeud = (INoeud)((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();
	        		
	            	if(noeud instanceof Projet) {
	            		cliqueDroitProjet((Projet)noeud, pathBounds);
	            	} else if(noeud instanceof Classe) {
	            		cliqueDroitClasse((Classe)noeud, pathBounds);
	            	} else if(noeud instanceof Paquet) {
	            		cliqueDroitPaquet((Paquet)noeud, pathBounds);
	            	} else if(noeud instanceof Interface) {
	            		cliqueDroitInterface((Interface)noeud, pathBounds);
	            	} else if(noeud instanceof Fonction) {
	            		cliqueDroitEnum((Enum)noeud, pathBounds);
	            	} else if(noeud instanceof Attribut) {
	            		cliqueDroitAttribut((Attribut)noeud, pathBounds);
	            	}
	            }
	        } 
	    }
	    
	    void cliqueDroitProjet(final Projet projet, Rectangle pathBounds) {
	    	JPopupMenu menu = new JPopupMenu ();
	    	
	    	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(projet);
        		}
        	});
        	menu.add(supprimer);
	    	
        	JMenuItem rename = new JMenuItem("renommer");
        	JMenuItem p = new JMenuItem("ajouter un paquet");
        	JMenuItem c = new JMenuItem("ajouter une classe");
        	JMenuItem i = new JMenuItem("ajouter une interface");
        	JMenuItem e = new JMenuItem("ajouter une enum�ration");
        	
        	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(projet);
        		}
        	});
        	p.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterPaquet((IConteneurPaquet) projet);
        		}
        	});
        	c.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterClasse((IConteneurClasse) projet);
        		}
        	});
        	i.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterInterface((IConteneurInterface) projet);
        		}
        	});
        	e.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterEnum((IConteneurEnum) projet);
        		}
        	});
	    	menu.add (rename);
	    	menu.add (p);
	    	menu.add (c);
	    	menu.add (i);
	    	menu.add (e);
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	    
	    void cliqueDroitPaquet(final Paquet paquet, Rectangle pathBounds) {
        	JPopupMenu menu = new JPopupMenu ();
        	
        	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(paquet);
        		}
        	});
        	menu.add(supprimer);
        	
        	JMenuItem rename = new JMenuItem("renommer");
        	JMenuItem p = new JMenuItem("ajouter un paquet");
        	JMenuItem c = new JMenuItem("ajouter une classe");
        	JMenuItem i = new JMenuItem("ajouter une interface");
        	JMenuItem e = new JMenuItem("ajouter une enum�ration");
        	
        	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(paquet);
        		}
        	});
        	p.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterPaquet((IConteneurPaquet) paquet);
        		}
        	});
        	c.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterClasse((IConteneurClasse) paquet);
        		}
        	});
        	i.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterInterface((IConteneurInterface) paquet);
        		}
        	});
        	e.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterEnum((IConteneurEnum) paquet);
        		}
        	});
        	
	    	menu.add (rename);
	    	menu.add (p);
	    	menu.add (c);
	    	menu.add (i);
	    	menu.add (e);
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	    
	    void cliqueDroitClasse(final Classe classe, Rectangle pathBounds) {
	    	JPopupMenu menu = new JPopupMenu ();
        	
	    	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(classe);
        		}
        	});
        	menu.add(supprimer);
        	
	    	JMenuItem rename = new JMenuItem("renommer");
        	JMenuItem c = new JMenuItem("ajouter une classe");
        	JMenuItem i = new JMenuItem("ajouter une interface");
        	JMenuItem e = new JMenuItem("ajouter une enum�ration");
        	JMenuItem f = new JMenuItem("ajouter une fonction");
        	JMenuItem a = new JMenuItem("ajouter un attribut");
        	
        	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(classe);
        		}
        	});
        	c.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterClasse((IConteneurClasse) classe);
        		}
        	});
        	i.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterInterface((IConteneurInterface) classe);
        		}
        	});
        	e.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterEnum((IConteneurEnum) classe);
        		}
        	});
        	f.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterFonction((IConteneurFonction) classe);
        		}
        	});
        	a.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterAttribut((IConteneurAttribut) classe);
        		}
        	});
        	
	    	menu.add (rename);
	    	menu.add (c);
	    	menu.add (i);
	    	menu.add (e);
	    	menu.add (f);
	    	menu.add (a);
	    	
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	    
	    void cliqueDroitInterface(final Interface i, Rectangle pathBounds) {
	    	JPopupMenu menu = new JPopupMenu ();
        	
	    	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(i);
        		}
        	});
        	menu.add(supprimer);
        	
	    	JMenuItem rename = new JMenuItem("renommer");
        	JMenuItem e = new JMenuItem("ajouter une enum�ration");
        	JMenuItem f = new JMenuItem("ajouter une fonction");
        	JMenuItem a = new JMenuItem("ajouter un attribut");
        	
        	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(i);
        		}
        	});
        	e.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterEnum((IConteneurEnum) i);
        		}
        	});

        	f.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterFonction((IConteneurFonction) i);
        		}
        	});
        	a.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.ajouterAttribut((IConteneurAttribut) i);
        		}
        	});
        	
	    	menu.add (rename);
	    	menu.add (e);
	    	menu.add (f);
	    	menu.add (a);
	    	
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	    
	    void cliqueDroitEnum(final Enum enumeration, Rectangle pathBounds) {
	    	JPopupMenu menu = new JPopupMenu ();
        	
	    	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(enumeration);
        		}
        	});
        	menu.add(supprimer);
        	
	    	JMenuItem rename = new JMenuItem("renommer");
	    	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(enumeration);
        		}
        	});
	    	
	    	menu.add (rename);
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	    
	    void cliqueDroitFonction(final Fonction f, Rectangle pathBounds) {
	    	JPopupMenu menu = new JPopupMenu ();
     
	    	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(f);
        		}
        	});
        	menu.add(supprimer);
        	
	    	JMenuItem rename = new JMenuItem("renommer");
	    	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(f);
        		}
        	});
	    	
	    	menu.add (rename);
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	    
	    void cliqueDroitAttribut(final Attribut a, Rectangle pathBounds) {
	    	JPopupMenu menu = new JPopupMenu ();
        	
	    	JMenuItem supprimer = new JMenuItem("supprimer");
        	supprimer.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.supprimerNoeud(a);
        		}
        	});
        	menu.add(supprimer);
        	
	    	JMenuItem rename = new JMenuItem("renommer");
	    	rename.addMouseListener(new MouseAdapter() {
        		public void mousePressed(MouseEvent e) {
        			FenetreDialogue.renommerNoeud(a);
        		}
        	});
	    	
	    	menu.add (rename);
        	menu.show ( arbre, pathBounds.x, pathBounds.y + pathBounds.height );
	    }
	}

	public void updateFils(INoeud n) {
		update(n);
	}

	public void update(INoeud n) {
		
		//if(INoeudNommable.class.isAssignableFrom(n.getClass()))
			
		System.out.println("[GestionProjet] update : " + ((INoeudNommable)n).getNom());
		arbre = new ArbreProjet(creerArbreNoyau(noyau));
		this.setViewportView(arbre);
	}

	public void updatePetitFils(INoeud n) {
		update(n);
	}
}
