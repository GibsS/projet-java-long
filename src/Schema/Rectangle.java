package Schema;

public class Rectangle {

	int x;
	int y;
	int width;
	int height;
	
	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public int getX() { return x; }
	public int getY() { return y; }
	public int getWidth() { return width; } 
	public int getHeight() { return height;}
	
	public String toString() { return "(x:" + x + ", y:" + y + ", width:" + width +", height:" + height +")"; }
}
