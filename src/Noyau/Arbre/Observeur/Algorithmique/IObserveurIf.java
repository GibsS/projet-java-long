package Noyau.Arbre.Observeur.Algorithmique;

import Noyau.Arbre.Algorithmique.If;
import Noyau.Arbre.Observeur.IObserveur;

/** l'interface IObserveurIf représente un type d' observeur de structure if.
 * Toutes les fonctions sont appelés pour signaler le changement correspondant.
 * @author gibson
 */
public interface IObserveurIf extends IObserveur {
	
	void changeCondition(If i);
	void addElse(If i);
	void removeElse(If i);
	
	void addElseIf(If i, If other);
	void removeElseIf(If i, If other);
}
