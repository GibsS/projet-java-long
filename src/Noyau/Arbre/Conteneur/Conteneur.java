package Noyau.Arbre.Conteneur;

import java.util.ArrayList;
import java.util.List;

import Noyau.Arbre.INoeudNommable;

/** Cette classe factorise le comportement de collection d'�l�ment nomm�.
 * @author gibson
 * @param <X> : le type de noeud nommable contenu
 */
public class Conteneur<X extends INoeudNommable> {

	/** les �l�ments list�s*/
	List<X> elements; 
	
	public Conteneur() {
		elements = new ArrayList<X>();
	}
	
	public void addElement(X c) {
		elements.add(c);
	}
	
	public X getElement(String nom) {
		for(X e: elements)
			if(e.getNom().equals(nom))
				return e;
		return null;
	}
	public List<X> getElements() {
		return elements;
	}
	
	public void removeElement(X a) {
		elements.remove(a);
	}
	public void supprimerElement(X a) {
		a.supprimer();
	}
}
