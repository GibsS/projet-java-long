package Schema;

import java.util.ArrayList;
import java.util.List;

public class Point {

	int x;
	int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equal(Point p) {
		return p.x == x && p.y == y;
	}
	
	public boolean equals(Object o) {
		return ((Point)o).x == x && ((Point)o).y == y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public List<Point> getAdjacent() {
		List<Point> voisins = new ArrayList<Point>();
		voisins.add(new Point(x+1, y));
		voisins.add(new Point(x-1, y));
		voisins.add(new Point(x, y+1));
		voisins.add(new Point(x, y-1));
		return voisins;
	}
	
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
	public static int produitScalaire(Point p1, Point p2) {
		return p1.x*p2.x + p1.y*p2.y;
	}
	
	public static Point soustraction(Point p1, Point p2) {
		return new Point(p2.x - p1.x, p2.y - p2.y);
	}
	
	public static Point addiction(Point p1, Point p2) {
		return new Point(p1.x + p2.x, p1.y + p2.y);
	}
	
	public static Point produitParScalaire(Point p, float l) {
		return new Point((int)(p.x*l), (int)(p.y*l));
	}
	
	public static int distance(Point p1, Point p2) {
		return (int) Math.sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
	}
	
	public int getNorme() {
		return (int) Math.sqrt(x*x + y*y);
	}
	
	public static Point normalize(Point p) {
		return produitParScalaire(p,1/p.getNorme());
	}
}
