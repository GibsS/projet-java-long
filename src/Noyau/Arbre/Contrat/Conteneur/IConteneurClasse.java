package Noyau.Arbre.Contrat.Conteneur;

import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Classe;

/** l'interface IConteneurClasse d�finit un conteneur de classe, les classes et les paquets
 * en font notamment partie.
 * @author gibson
 */
public interface IConteneurClasse extends INoeudNommable {
	
	Classe addClasse(String nom);
	void addClasse(Classe c);
	
	Classe getClasse(String nom);
	List<Classe> getClasses();
	
	void removeClasse(Classe a);
	void detruireClasse(Classe a);
}