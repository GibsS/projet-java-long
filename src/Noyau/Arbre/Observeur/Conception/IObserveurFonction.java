package Noyau.Arbre.Observeur.Conception;

import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.Observeur.IObserveurNommable;

public interface IObserveurFonction extends IObserveurNommable {

	void changeType(Fonction f);
	void addArgument(Fonction f, String argument);
	void removeArgument(Fonction f, String argument);
	
	void addCorp(Fonction f);
	void removeCorp(Fonction f);
}
