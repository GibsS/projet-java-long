package Noyau.Arbre.Contrat.Conteneur;

import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Paquet;

/** l'interface IConteneurPaquet d�finit un conteneur de paquet, les paquets
 * en font partie.
 * @author gibson
 */
public interface IConteneurPaquet extends INoeudNommable {

	Paquet addPaquet(String nom);
	void addPaquet(Paquet e);
	
	Paquet getPaquet(String nom);
	List<Paquet> getPaquets();
	
	void removePaquet(Paquet a);
	void detruirePaquet(Paquet a);
}
