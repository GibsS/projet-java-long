package Noyau.Arbre.ElementJava;

import java.util.*;

import Noyau.Arbre.*;
import Noyau.Arbre.Algorithmique.Sequence;
import Noyau.Arbre.Contrat.Conteneur.IConteneurFonction;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conception.IObserveurFonction;
import Noyau.Arbre.Projet.Projet;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.Dependance.*;
import Noyau.GestionFichier.Fichier;
import Noyau.Modifieur.*;

/** la classe Fonction contient les informations d'une fonction.: son type de retour, son nom, son algorithme et ces variables locales
 * @author gibson
 */
public class Fonction extends ModeleNoeud implements INoeudProjet, INoeudFichier, IConteneurModifieur, IConteneurDependance, INommableJava  {
	
	String typeRetour;
	List<String> typeArg;
	List<String> nomArg;
	
	public List<Attribut> variableLocal;
	public Sequence algorithmique;
	
	String nom;
	
	ConteneurModifieur modifieurs;
	ConteneurDependance dependances;
	
	public Fonction(IConteneurFonction pere, String nom, boolean aCorp, String typeRetour) {
		super(pere);
		this.typeRetour = typeRetour;
		
		if(aCorp) {
			algorithmique = new Sequence(this);
			variableLocal = new ArrayList<Attribut>();
		} 
		
		modifieurs = new ConteneurModifieur(this);
		dependances = new ConteneurDependance(this);
		
		setNom(nom);
		
		this.typeArg = new ArrayList<String>();
		this.nomArg = new ArrayList<String>();
	}
	
	public Projet getProjet() {
		return ((INoeudProjet)getPere()).getProjet();
	}
	
	public void compiler() {
		((INoeudFichier)getPere()).getFichier().creerCode();
	}
	
	public String creerCode() {
		String res;
		res =modifieurs.toString();
		res += this.typeRetour + " " + getNom() + " (";
		for(int i = 0; i < typeArg.size(); i++) {
			res += typeArg.get(i) + " " + nomArg.get(i);
			if(i != typeArg.size() -1)
				res +=", ";
		}
		res += ")";
		if(aCorp()) {
			res += " {\n";
			//res += " \n";
			for(Attribut a:variableLocal)
				res += Code.tabuler(a.creerCode() + ";\n");
			
			res += Code.tabuler(algorithmique.creerCode());
			res += "}\n";
		} else {
			res += "; ";
		}
		return res;
	}
	
	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code contenuCode;
		Code argumentCode;
		
		c.extraireEspaceInutile();
		for(Modifieur modifieur:c.extraireModifieurs())
			modifieurs.addModifieur(modifieur);
		c.extraireEspaceInutile();
		this.setRetour(c.lireMot());
		c.extraireEspace();
		try {
			setNom(c.lireMot());
		} catch(Exception e) {
			//System.out.println("[Fonction] mauvais nom chop�!");
			//throw new ParseException("mauvais nom");
		}
		argumentCode = c.extraireParenthese();
		argumentCode.extraireEspaceInutile();
		while(!argumentCode.isEmpty() && argumentCode.toString() != "" && !argumentCode.toString().isEmpty()) {
			argumentCode.extraireEspaceInutile();
			String type = argumentCode.lireMot();
			argumentCode.extraireEspace();
			String nom = argumentCode.lireMot();
			argumentCode.extraireEspaceInutile();
			this.addArgument(type, nom);
			try {
				argumentCode.extraireMot(",");
			} catch (ParseException p) {
				break;
			}
		}
		
		c.extraireEspaceInutile();
		try {
			contenuCode = c.extraireCrochet();
			Sequence s = new Sequence(this);
			s.interpreterCode(contenuCode);
			this.addCorp(s);
		} catch(ParseException p) {
			c.extraireMot(";");
		}
		
		return c;
	}
	
	public void supprimer() {
		for(Attribut a:variableLocal)
			a.supprimer();
		
		((IConteneurFonction)getPere()).removeFonction(this);
	}
	
	public String getNom() { return nom; }
	public void setNom(String nouveauNom) throws ExceptionMauvaisNom { 
		nom = ExceptionMauvaisNom.testNom(nouveauNom); 
		for(IObserveur o : getObserveurs())((IObserveurNommable)o).changeNom(this);
		notifierModification();
	}
	
	/** addArgument : rajoute un argument � la liste d'argument
	 * @param type : le type de l'argument
	 * @param nom : le nom de l'argument
	 */
	public void addArgument(String type, String nom) {
		typeArg.add(type);
		nomArg.add(nom);
		
		for(IObserveur o : getObserveurs())((IObserveurFonction)o).addArgument(this, nom);
		notifierModification();
	}
	
	/** removeArgument : retire un argument � la liste d'argument
	 * @param nom : le nom de l'argument � retirer.
	 */
	public void removeArgument(String nom) {
		int i = nomArg.indexOf(nom);
		typeArg.remove(i);
		nomArg.remove(i);
		
		for(IObserveur o : getObserveurs())((IObserveurFonction)o).removeArgument(this, nom);
		notifierModification();
	}
	
	/** setRetour : change le type de retour
	 * @param type : le nom du type
	 */
	public void setRetour(String type) {
		typeRetour = type;
		
		for(IObserveur o : getObserveurs())((IObserveurFonction)o).changeType(this);
		notifierModification();
	}
	
	/** getTypeArgument : renvoie le type de l'argument de nom "nom"
	 * @param nom : le nom de l'argument
	 * @return le type de l'argument
	 */
	public String getTypeArgument(String nom) {
		int i = nomArg.indexOf(nom);
		return typeArg.get(i);
	}
	
	/** getArguments : retourne le nom de tous les arguments.
	 * @return retourne la liste des noms des arguments.
	 */
	public List<String> getArguments() {
		return nomArg;
	}
	
	/** addCorp : rajoute un corp � la fonction
	 * @param s : la s�quence repr�sentant l'algorithmique
	 */
	public void addCorp(Sequence s) {
		if(!aCorp()) {
			s.setPere(this);
			algorithmique = s;
			variableLocal = new ArrayList<Attribut>();
			for(IObserveur o : getObserveurs())((IObserveurFonction)o).addCorp(this);
			notifierModification();
		}
	}
	
	/** removeCorp : retire le corp de la fonction
	 */
	public void removeCorp() {
		if(aCorp()) {
			algorithmique = null;
			for(IObserveur o : getObserveurs())((IObserveurFonction)o).removeCorp(this);
			notifierModification();
		}
	}
	
	/** aCorp : renvoie true si il y a un corp
	 * @return true si la fonction a un corp
	 */
	public boolean aCorp() {
		return algorithmique != null;
	}

	public List<Modifieur> getModifieurs() { return modifieurs.getModifieurs(); }
	public boolean hasModifieur(String modifieur) { return modifieurs.hasModifieur(modifieur); }
	public void addModifieur(Modifieur modifieur) { modifieurs.addModifieur(modifieur); }
	public void removeModifieur(Modifieur modifieur) { modifieurs.removeModifieur(modifieur); }

	public List<IConteneurDependance> getSousDependant() {
		List<IConteneurDependance> tous = new ArrayList<IConteneurDependance>(variableLocal);
		return tous;
	}
	public List<Dependance> getDependance() { return dependances.getDependance(); }	
	public List<Dependance> getAllDependance() {
		List<Dependance> tous = new ArrayList<Dependance>(dependances.getDependance());
		for(IConteneurDependance i : variableLocal)
			tous.addAll(i.getAllDependance());
		return tous;
	}
	public void addDependance(Dependance dependance) { dependances.addDependance(dependance); }
	public void removeDependance(Dependance dependance) { dependances.removeDependance(dependance); }

	public Fichier getFichier() { return ((INoeudFichier)getPere()).getFichier(); }
	public void setFichier(Fichier f) { }
	
	public List<String> getStringPath() {
		INoeudNommable courant = (INoeudNommable)getPere();
		List<String> chemin = new ArrayList<String>();
	
		while(!(courant instanceof Paquet)) {
			chemin.add(courant.getNom());
			courant = (INoeudNommable)courant.getPere();
		}
	
		Collections.reverse(chemin);
		return chemin;
	}
}
