package Logigramme.Observeur;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Logigramme.SchemaLogigramme;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.Algorithmique.For;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurFor;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;

public class ObserveurFor implements IObserveurFor, IObserveurBoite {
	
	private ObserveurSequence observeurSequence;
	
	private Boite boiteDebut;
	
	private Boite boiteFin;
	
	private SchemaLogigramme schemaLogigramme;
	
	private For f;

	public ObserveurFor(SchemaLogigramme schlogi, For f) {
		this.observeurSequence = new ObserveurSequence(schlogi, null/*f.getContenu()*/);
		this.schemaLogigramme = schlogi;
		/* Constructeur à compléter */
	
	}	
	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeInstructionDebut(For f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeInstructionFin(For f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeCondition(For f) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier");
    	JMenuItem suppr = new JMenuItem("supprimer");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierFor(f);
			}
    	});
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				f.supprimer();
			}
    	});
    	
    	
    	menu.add (modif);
    	menu.add (suppr);
    	
    	menu.show(e.getComponent(), e.getX(), e.getY());
	}
	
	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
