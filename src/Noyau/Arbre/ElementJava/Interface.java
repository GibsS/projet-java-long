package Noyau.Arbre.ElementJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Noyau.Arbre.*;
import Noyau.Arbre.Contrat.Conteneur.*;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Conception.IObserveurInterface;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Projet.Projet;
import Noyau.GestionFichier.Fichier;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.Dependance.*;
import Noyau.Modifieur.*;

/** la classe Interface représente une interface Java : ces fonctions, ces attributs, ces énumérations.
 * @author gibson
 */
public class Interface extends ModeleNoeud implements INoeudProjet, INoeudFichier, IConteneurDependance, IConteneurModifieur, IConteneurEnum, IConteneurFonction, 
													  IConteneurAttribut, INommableJava {

	List<Attribut> attributs;
	List<Fonction> fonctions;
	List<Enum>		enums;
	
	ConteneurModifieur modifieurs;
	ConteneurDependance dependances;
	
	String nom;
	
	Fichier fichier;
	
	public Interface(INoeud pere, String nom) { 
		super(pere);
		attributs  = new ArrayList<Attribut>();
		fonctions  = new ArrayList<Fonction>();
		enums      = new ArrayList<Enum>();
		
		modifieurs  = new ConteneurModifieur(this);
		dependances = new ConteneurDependance(this);
		
		setNom(nom);
		
		if(getProjet() != null)
			this.fichier = getProjet().getProjetMachine().creerFichierInterface(this);
	}
			
	public Projet getProjet() {
		if(getPere() != null)
			return ((INoeudProjet)getPere()).getProjet();
		else 
			return null;
	}
	
	public void compiler() {
		fichier.creerCode();
	}
	
	public String creerCode() {
		String res;
		res = modifieurs.toString();
		res += "interface " + getNom() + " {\n\n";
		for(Attribut a:getAttributs())
			res += Code.tabuler(a.creerCode()) + "\n";
		for(Fonction f:getFonctions())
			res += Code.tabuler(f.creerCode()) + "\n";
		for(Enum e:getEnums())
			res += Code.tabuler(e.creerCode()) + "\n";
		res = res + "}\n";
		return res;
	}
	
	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code contenuCode;
		
		c.extraireEspaceInutile();
		for(Modifieur modifieur:c.extraireModifieurs())
			modifieurs.addModifieur(modifieur);
		c.extraireEspaceInutile();
		c.extraireMot("interface");
		c.extraireEspaceInutile();
		try {
			setNom(c.lireMot());
		} catch(Exception e) {
			
		}
		c.extraireEspaceInutile();
		contenuCode = c.extraireCrochet();
		
		while(!contenuCode.isEmpty() && contenuCode.toString() != "" && !contenuCode.toString().isEmpty()) {
			try {
				Fonction nouvelleFonction = new Fonction(this, "tmp", false, "tmp");
				contenuCode = nouvelleFonction.interpreterCode(contenuCode);
				this.addFonction(nouvelleFonction);
				continue;
			} catch (ParseException p) {
				
			}
			try {
				Enum nouvelleEnum = new Enum(this, "tmp");
				contenuCode = nouvelleEnum.interpreterCode(contenuCode);
				this.addEnum(nouvelleEnum);
				continue;
			} catch (ParseException p) {
				
			}
			try {
				Attribut nouvelAttribut = new Attribut(this,"tmp", "tmp");
				contenuCode = nouvelAttribut.interpreterCode(contenuCode);
				this.addAttribut(nouvelAttribut);
				continue;
			} catch (ParseException p) {
				
			}
		}
		return c;
	}
	
	public void supprimer() {
		for(int i = 0; i < getAttributs().size();i++)
			getAttributs().get(i).supprimer();
		for(int i = 0; i < getEnums().size();i++)
			getEnums().get(i).supprimer();
		for(int i = 0; i < getFonctions().size();i++)
			getFonctions().get(i).supprimer();
		
		((IConteneurInterface)getPere()).removeInterface(this);
		
		fichier.removeInterface(this);
	}
	
	public List<Modifieur> getModifieurs() {
		return modifieurs.getModifieurs();
	}

	public boolean hasModifieur(String modifieur) {
		return modifieurs.hasModifieur(modifieur);
	}

	public void addModifieur(Modifieur modifieur) {
		modifieurs.addModifieur(modifieur);
	}

	public void removeModifieur(Modifieur modifieur) {
		modifieurs.removeModifieur(modifieur);
	}
	
	public List<IConteneurDependance> getSousDependant() {
		List<IConteneurDependance> tous = new ArrayList<IConteneurDependance>(attributs);
		tous.addAll(fonctions);
		return tous;
	}

	public List<Dependance> getDependance() {
		return dependances.getDependance();
	}

	public List<Dependance> getAllDependance() {
		List<Dependance> tous = new ArrayList<Dependance>(dependances.getDependance());
		for(Fonction f : fonctions)
			tous.addAll(f.getAllDependance());
		for(Attribut a : attributs)
			tous.addAll(a.getAllDependance());
		return tous;
	}
	
	public void addDependance(Dependance dependance) {
		dependances.addDependance(dependance);
	}
	
	public void removeDependance(Dependance dependance) {
		dependances.removeDependance(dependance);
	}

	public Attribut addAttribut(String nom, String type) { 
		Attribut a = new Attribut(this, nom, type);
		addAttribut(a); 
		return a;
	}
	public Fonction addFonction(String nom, String type) { 
		Fonction f = new Fonction(this, nom, false, type);
		addFonction(f); 
		return f;
	}
	public Enum addEnum(String nom) { 
		Enum e = new Enum(this, nom);
		addEnum(e); 
		return e;
	}
	
	public void addEnum(Enum e) { 
		enums.add(e); 
		for(IObserveur o : getObserveurs())((IObserveurInterface)o).enumAjoute(this,e);
		notifierModification();
	}
	public void addAttribut(Attribut a) { 
		attributs.add(a); 
		for(IObserveur o : getObserveurs())((IObserveurInterface)o).attributAjoute(this,a);
		notifierModification();
	}
	public void addFonction(Fonction f) { 
		fonctions.add(f); 
		for(IObserveur o : getObserveurs())((IObserveurInterface)o).fonctionAjout(this,f);
		notifierModification();
	}
	
	public void removeAttribut(Attribut a) { 
		attributs.remove(a); 
		for(IObserveur o : getObserveurs())((IObserveurInterface)o).attributRetire(this,a);
		notifierModification();
	}
	public void removeFonction(Fonction f) { 
		fonctions.remove(f); 
		for(IObserveur o : getObserveurs())((IObserveurInterface)o).fonctionRetire(this,f);
		notifierModification();
	}
	public void removeEnum(Enum e) { 
		enums.remove(e); 
		for(IObserveur o : getObserveurs())((IObserveurInterface)o).enumRetire(this,e);
		notifierModification();
	}
	
	public String getNom() { return nom; }
	public void setNom(String nouveauNom) throws ExceptionMauvaisNom { 
		nom = ExceptionMauvaisNom.testNom(nouveauNom); 
		for(IObserveur o : getObserveurs())((IObserveurNommable)o).changeNom(this);
		notifierModification();
	}
	
	public List<Attribut> getAttributs() { return attributs; }
	public List<Fonction> getFonctions() { return fonctions; }
	public List<Enum> getEnums() { return enums; }
	
	public Attribut getAttribut(String nom) { for(INommableJava n:attributs)if(n.getNom().equals(nom))return (Attribut) n; return null; }
	public Fonction getFonction(String nom) { for(INommableJava n:fonctions)if(n.getNom().equals(nom))return (Fonction) n; return null; }
	public Enum getEnum(String nom) { for(INommableJava n:enums)if(n.getNom().equals(nom))return (Enum) n; return null; }

	public void detruireAttribut(Attribut a) { a.supprimer(); }
	public void detruireFonction(Fonction a) { a.supprimer(); }
	public void detruireEnum(Enum a) { a.supprimer(); }

	public Fichier getFichier() { return fichier; }
	public void setFichier(Fichier f) { fichier = f; }
	
	public List<String> getStringPath() {
		INoeudNommable courant = (INoeudNommable)getPere();
		List<String> chemin = new ArrayList<String>();
	
		while(!(courant instanceof Paquet)) {
			chemin.add(courant.getNom());
			courant = (INoeudNommable)courant.getPere();
		}
	
		Collections.reverse(chemin);
		return chemin;
	}
}
