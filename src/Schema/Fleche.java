package Schema;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import Schema.Schema.SettingSchema;

/** Represente une fleche
 * @author G33
 */
public class Fleche {
	
	private Boite boiteDepart; 
	private Boite boiteArrivee; 
	
	private Point[] chemin;
	
	Schema schema;
	SettingSchema settings;
	
	public Fleche(Schema s) {
		schema = s;
		settings = s.getSettingSchema();
	}
	
	public void setBoite(Boite depart, Boite arrivee) {
		this.boiteArrivee = arrivee;
		this.boiteDepart = depart;
	}
	public Boite getBoiteDepart() { return this.boiteDepart; }
	public Boite getBoiteArrivee() { return this.boiteArrivee; }
	
	public void afficherContenu(Graphics g) {
		Point[] points = getTrajet();
		Point[] screenPoints = new Point[points.length];
		
		for(int i = 0; i < points.length; i++)
			screenPoints[i] = schema.offsetToScreen(points[i]);
		
		Graphics2D g2d = (Graphics2D) g;
		float[] style2 = {5,0}; // pas de pointilles
		g2d.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,style2,0.0f));
		for(int i = 0; i < points.length-1; i++) {
			Line2D r = new Line2D.Double(screenPoints[i].x,screenPoints[i].y,screenPoints[i+1].x,screenPoints[i+1].y);
			g2d.draw(r);
		}
	}
	
	public void setChemin(Point[] chemin) {
		this.chemin = chemin;
	}
	public void clearChemin() {
		this.chemin = null;
	}
	public Point[] getTrajet() {
		if(chemin != null && chemin.length != 0) {
			Point[] vraiChemin = new Point[chemin.length+2];
			Point debut;
			Point fin;
		
			for(int i = 0; i < chemin.length; i++)
				vraiChemin[i+1] = new Point((int)((chemin[i].x + 0.5)*settings.getLargeurPixel()), (int)((chemin[i].y + 0.5)*settings.getHauteurPixel()));
			debut = cheminPointVersBoite(vraiChemin[1], boiteDepart);
			fin = cheminPointVersBoite(vraiChemin[chemin.length], boiteArrivee);
			vraiChemin[0] = debut;
			vraiChemin[chemin.length+1] = fin;
			return vraiChemin;
		} else {
			return cheminBoiteVersBoite(boiteDepart, boiteArrivee);
		}
	}
	
	private Point cheminPointVersBoite(Point p, Boite b) {
		int x1 = b.getX()*settings.getLargeurPixel() + b.getLargeur()*settings.getLargeurPixel()/2;
		int y1 = b.getY()*settings.getHauteurPixel() + b.getHauteur()*settings.getHauteurPixel()/2;
		
		int l1 = (b.getLargeur()*settings.getLargeurPixel() - settings.getPadding())/2;
		int h1 = (b.getHauteur()*settings.getLargeurPixel() - settings.getPadding())/2;
		
		int xrel = p.x - x1;
		int yrel = p.y - y1;
		
		int distance = (int) Math.sqrt(xrel*xrel + yrel*yrel);
		
		float cos = (float)xrel/(float)distance;
		float sin = (float)yrel/(float)distance;
		
		Point res;
		
		float c = (float) (Math.sqrt(2)/2);
		
		if(cos > c && Math.abs(sin) < c) {
			res = new Point(x1 + l1,y1);
		} else if(Math.abs(cos) < c && sin > c) {
			res = new Point(x1,y1 + h1);
		} else if(cos < - c && Math.abs(sin) < c) {
			res = new Point(x1 - l1, y1);
		} else {
			res = new Point(x1, y1 - h1);
		}
		
		return res;
	}
	
	private Point[] cheminBoiteVersBoite(Boite b1, Boite b2) {
		int x1 = boiteDepart.getX()*settings.getLargeurPixel() + boiteDepart.getLargeur()*settings.getLargeurPixel()/2;
		int y1 = boiteDepart.getY()*settings.getHauteurPixel() + boiteDepart.getHauteur()*settings.getHauteurPixel()/2;
		int x2 = boiteArrivee.getX()*settings.getLargeurPixel() + boiteArrivee.getLargeur()*settings.getLargeurPixel()/2;
		int y2 = boiteArrivee.getY()*settings.getHauteurPixel() + boiteArrivee.getHauteur()*settings.getHauteurPixel()/2;
		
		int l1 = (boiteDepart.getLargeur()*settings.getLargeurPixel() - settings.getPadding())/2;
		int l2 = (boiteArrivee.getLargeur()*settings.getLargeurPixel() - settings.getPadding())/2;
		int h1 = (boiteDepart.getHauteur()*settings.getLargeurPixel() - settings.getPadding())/2;
		int h2 = (boiteArrivee.getHauteur()*settings.getLargeurPixel() - settings.getPadding())/2;
		
		int xrel = x2 - x1;
		int yrel = y2 - y1;
		
		int distance = (int) Math.sqrt(xrel*xrel + yrel*yrel);
		
		float cos = (float)xrel/(float)distance;
		float sin = (float)yrel/(float)distance;
		
		Point debut;
		Point fin;
		
		float c = (float) (Math.sqrt(2)/2);
		
		if(cos > c && Math.abs(sin) < c) {
			debut = new Point(x1 + l1,y1);
			fin   = new Point(x2 - l2,y2);
		} else if(Math.abs(cos) < c && sin > c) {
			debut = new Point(x1,y1 + h1);
			fin   = new Point(x2, y2 -h2);
		} else if(cos < - c && Math.abs(sin) < c) {
			debut = new Point(x1 - l1, y1);
			fin   = new Point(x2 + l2,y2);
		} else {
			debut = new Point(x1, y1 - h1);
			fin   = new Point(x2, y2 + h2);
		}
		
		return new Point[] {debut,fin};
	}
	
	public int distanceToPoint(Point p) {
		Point[] points = getTrajet();
		int res = Integer.MAX_VALUE;
		
		for(int i = 0; i < points.length-1; i++) {
			int distance = distancePointASegment(p,points[i],points[i+1]);
			if(distance < res)
				res = distance;
		}
		
		return res;
	}
	
	public int distancePointASegment(Point p, Point p1, Point p2) {
		float f = Point.produitScalaire(Point.soustraction(p1, p2), Point.soustraction(p1, p));
		if (f <= 0)
			return Point.distance (p1, p);
		else if (f >= Point.distance (p2, p1))
			return Point.distance (p2, p);
		else
			return Point.distance (Point.addiction(p1, Point.produitParScalaire(Point.normalize(Point.soustraction(p1, p2)),f)), p);
	}
}
