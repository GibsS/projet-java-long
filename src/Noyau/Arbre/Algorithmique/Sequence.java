package Noyau.Arbre.Algorithmique;

import java.util.ArrayList;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurSequence;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;

/** la classe Sequence repr�sente une s�quence d'algorithme java.
 * @author gibson
 */
public class Sequence extends Algorithme  {
	public ArrayList<Algorithme> contenu;
	
	public Sequence(INoeud n) {
		super(n);
		contenu = new ArrayList<Algorithme>();
	}

	public String creerCode() {
		String res = "";
		for(Algorithme c:contenu) {
			res += c.creerCode();
			if(c instanceof Instruction) {
				res += ";";
			}
			res += "\n";
		}
		return res;
	}

	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		
		contenu.clear();
		
		while(!code.isEmpty()) {
			c.extraireEspaceInutile();
			try {
				While w = new While(this);
				c = w.interpreterCode(c);
				addFin(w);
			} catch(ParseException e) {
				try {
					DoWhile w = new DoWhile(this);
					c = w.interpreterCode(c);
					addFin(w);
				} catch(ParseException f) {
					try {
						For fo = new For(this);
						c = fo.interpreterCode(c);
						addFin(fo);
					} catch(ParseException g) {
						try {
							If i = new If(this);
							c = i.interpreterCode(c);
							addFin(i);
						} catch(ParseException h) {
							try {
								Switch s = new Switch(this);
								c = s.interpreterCode(c);
								addFin(s);
							} catch(ParseException x) {
								try {
									Instruction instruction = new Instruction(this);
									Code instructionCode;
									instructionCode = c.extraireVersMot(";");
									instruction.interpreterCode(instructionCode);
									addFin(instruction);
								} catch(ParseException y) {
									break;
								}
							}
						}
					}
				}
			}
		}
		return c;
	}
	
	public void supprimer() {
		//System.out.println("[Sequence] Probl�me de suppression de sequence");
		//if(getPere() instanceof If)	
	}
	
	public void addFin(Algorithme i) {
		contenu.add(i);
		i.setPere(this);
		for(IObserveur o : getObserveurs())((IObserveurSequence)o).addAlgorithme(this, i);
		notifierModification();
	}
	
	public void addMilieu(Algorithme precedent, Algorithme n) {
		int i = contenu.indexOf(precedent);
		contenu.add(i+1, n);
		for(IObserveur o : getObserveurs())((IObserveurSequence)o).addAlgorithme(this, precedent, n);
		notifierModification();
	}
	
	public void removeAlgorithme(Algorithme i) {
		contenu.remove(i);
		for(IObserveur o : getObserveurs())((IObserveurSequence)o).removeAlgorithme(this, i);
		notifierModification();
	}
}
