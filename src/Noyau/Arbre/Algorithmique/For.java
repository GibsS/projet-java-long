package Noyau.Arbre.Algorithmique;

/** La classe For contient toutes les informations pertinentes � la structure de controle
 * du For : �a s�quence interne, sa condition de boucle et ces instructions (d'initialisation et de fin de boucle).
 * @author gibson
 */
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Algorithmique.*;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;

public class For extends Algorithme {
	Expression condition;
	Instruction debut;
	Instruction fin;
	Sequence contenu;
	
	public For(Sequence pere) {
		super(pere);
		contenu = new Sequence(this);
		debut = new Instruction(this);
		fin = new Instruction(this);
		condition = new Expression();
	}

	public String creerCode() {
		return "for(" + debut.creerCode() + "; " + condition.creerCode() + "; " + fin.creerCode() + ") {\n" 
				+ Code.tabuler(contenu.creerCode()) + "}\n";
	}

	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code instructionDebut;
		Code instructionFin;
		Code instructionCondition;
		Code contenuCode;
		
		c.extraireEspaceInutile();
		c.extraireMot("for");
		c.extraireEspaceInutile();
		instructionFin = c.extraireParenthese();
		
		// dans les parenth�ses :
		instructionDebut = instructionFin.extraireVersMot(";");
		debut.interpreterCode(instructionDebut);
		instructionCondition = instructionFin.extraireVersMot(";");
		condition.interpreterCode(instructionCondition);
		fin.interpreterCode(instructionFin);
		
		// dans les crochet
		c.extraireEspaceInutile();
		contenuCode = c.extraireCrochet();
		contenu.interpreterCode(contenuCode);
		return c;
	}
	
	public void setCondition(Expression e) {
		condition = e;
		for(IObserveur o : getObserveurs()) ((IObserveurFor)o).changeCondition(this);
		notifierModification();
	}
	
	public void setInstructionDebut(Instruction d) {
		debut = d;
		for(IObserveur o : getObserveurs()) ((IObserveurFor)o).changeInstructionDebut(this);
		notifierModification();
	}
	
	public void setInstructionFin(Instruction d) {
		fin = d;
		for(IObserveur o : getObserveurs()) ((IObserveurFor)o).changeInstructionFin(this);
		notifierModification();
	}
	
	public Expression getCondition() {
		return condition;
	}
	
	public Instruction getDebut() {
		return debut;
	}
	
	public Instruction getFin() {
		return fin;
	}
}
