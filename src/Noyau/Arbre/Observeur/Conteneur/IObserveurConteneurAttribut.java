package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Contrat.Conteneur.IConteneurAttribut;
import Noyau.Arbre.ElementJava.Attribut;
import Noyau.Arbre.Observeur.IObserveur;

public interface IObserveurConteneurAttribut extends IObserveur {
	
	void attributAjoute(IConteneurAttribut i, Attribut f);
	void attributRetire(IConteneurAttribut i, Attribut a);
}
