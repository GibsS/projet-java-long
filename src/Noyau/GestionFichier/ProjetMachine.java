package Noyau.GestionFichier;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.Projet.Projet;

public class ProjetMachine {

	public List<Fichier> fichiers;
	Projet associe;
	String path;
	 
	public ProjetMachine(Projet associe, String path) {
		this.associe = associe;
		this.path = path;
		
		fichiers = new ArrayList<Fichier>();
	}
	
	public void miseAJour() {
		trouverNouveauFichiers(new File(this.path).listFiles());
		
		for(Fichier f:fichiers)
			f.update();
	}
	
	void trouverNouveauFichiers(File[] files) {
		boolean present;
		
		for(File file : files) {
			if(file.isDirectory()) {
				trouverNouveauFichiers(file.listFiles());
			} else {

				if(getFileExtension(file).equals(".java")){
					present = false;
					
					for(Fichier f:fichiers) {
						if(f.getFile().getName().equals(file.getName())){
							present = true; 
							break;
						}
					}
					if(!present) {
						creerFichier(file.getAbsolutePath());
					}
				}
			}
		}
	}

	private String getFileExtension(File file) {
    	String name = file.getName();
    	try {
        	return name.substring(name.lastIndexOf("."));
    	} catch (Exception e) {
    		return "";
    	}
	}
	
	public void retirerFichier(Fichier f) {
		fichiers.remove(f);
	}
	
	public Fichier creerFichierClasse(Classe c) {
		Fichier  f = creerFichier(c.getStringPath(), c.getNom());
		f.addClasse(c);
		return f;
	}
	
	public Fichier creerFichierInterface(Interface i) {
		Fichier  f = creerFichier(i.getStringPath(), i.getNom());
		f.addInterface(i);
		return f;
	}
	
	public Fichier creerFichierEnum(Enum e) {
		Fichier  f = creerFichier(e.getStringPath(), e.getNom());
		f.addEnum(e);
		return f;
	}
	
	private Fichier creerFichier(List<String> chemin, String nom) {
		String path = "";
		Fichier f = null;
		File file = null;
		
		for(String k:chemin) {
			path += k + "\\";
		}
		
		file = new File(this.path + File.separator  + path + File.separator + nom + ".java");
		f = new Fichier(this, file, false);
		fichiers.add(f);
		return f;
	}
	
	private Fichier creerFichier(String path) {
		Fichier f = new Fichier(this, new File( path), true);
		fichiers.add(f);
		return f;
	}
}
