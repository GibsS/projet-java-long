package Noyau.Arbre.Contrat.Conteneur;

import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Attribut;

/** l'interface IConteneurAttribut d�finit un conteneur d'attribut, les classes et les interfaces
 * en font partie.
 * @author gibson
 */
public interface IConteneurAttribut extends INoeudNommable {

	Attribut addAttribut(String nom, String type);
	void addAttribut(Attribut c);
	
	Attribut getAttribut(String nom);
	List<Attribut> getAttributs();
	
	void removeAttribut(Attribut a);
	void detruireAttribut(Attribut a);
}
