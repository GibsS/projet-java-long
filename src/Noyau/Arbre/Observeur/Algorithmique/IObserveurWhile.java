package Noyau.Arbre.Observeur.Algorithmique;

import Noyau.Arbre.Algorithmique.While;
import Noyau.Arbre.Observeur.IObserveur;

/** l'interface IObserveurWhile représente un type d' observeur de structure while.
 * Toutes les fonctions sont appelés pour signaler le changement correspondant.
 * @author gibson
 */
public interface IObserveurWhile extends IObserveur {

	void changeCondition(While w);
}
