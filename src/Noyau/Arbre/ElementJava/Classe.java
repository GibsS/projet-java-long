package Noyau.Arbre.ElementJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.Dependance.*;
import Noyau.Modifieur.*;
import Noyau.Arbre.INoeudFichier;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.INoeudProjet;
import Noyau.Arbre.INommableJava;
import Noyau.Arbre.ModeleNoeud;
import Noyau.Arbre.Conteneur.Conteneur;
import Noyau.Arbre.Contrat.Conteneur.*;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conception.IObserveurClasse;
import Noyau.Arbre.Projet.Projet;
import Noyau.GestionFichier.Fichier;

/** la classe Classe représente les informations d'une classe java.
 * @author gibson
 */
public class Classe extends ModeleNoeud implements INoeudFichier, INoeudProjet, IConteneurModifieur, IConteneurDependance, IConteneurClasse, IConteneurInterface, IConteneurEnum, 
												   IConteneurAttribut, IConteneurFonction, INommableJava {

	Conteneur<Attribut> attributs;
	Conteneur<Fonction> fonctions;
	Conteneur<Classe> classes;
	Conteneur<Interface> interfaces;
	Conteneur<Enum> enums;
	
	ConteneurModifieur modifieurs;
	ConteneurDependance dependances;
	
	String nom;
	
	Fichier fichier;
	
	public Classe(IConteneurClasse pere, String s) { 
		super(pere);
		
		attributs	= new Conteneur<Attribut>();
		fonctions 	= new Conteneur<Fonction>();
		classes 	= new Conteneur<Classe>();
		interfaces 	= new Conteneur<Interface>();
		enums 		= new Conteneur<Enum>();
		
		modifieurs = new ConteneurModifieur(this);
		dependances = new ConteneurDependance(this);
		
		setNom(s);
		
		if(getProjet() != null)
			this.fichier = getProjet().getProjetMachine().creerFichierClasse(this);
	}
			
	public Projet getProjet() {
		if(getPere() != null)
			return ((INoeudProjet)getPere()).getProjet();
		else 
			return null;
	}
	
	public void compiler() {
		fichier.creerCode();
	}
	
	public Code interpreterCode(Code code) throws Code.ParseException {
		Code c = new Code(code);
		Code contenuCode;
		
		c.extraireEspaceInutile();
		for(Modifieur modifieur:c.extraireModifieurs())
			modifieurs.addModifieur(modifieur);
		c.extraireEspaceInutile();
		c.extraireMot("class");
		c.extraireEspaceInutile();
		try {
			setNom(c.lireMot());
		} catch(ExceptionMauvaisNom e) {

		}
		c.extraireEspaceInutile();
		contenuCode = c.extraireCrochet();
		
		while(!contenuCode.isEmpty() && contenuCode.toString() != "" && !contenuCode.toString().isEmpty()) {
			contenuCode.extraireEspaceInutile();
			try {
				Classe nouvelleClasse = new Classe(this, "tmp");
				contenuCode = nouvelleClasse.interpreterCode(contenuCode);
				this.addClasse(nouvelleClasse);
				continue;
			} catch (ParseException p) {
				
			}
			try {
				Interface nouvelleInterface = new Interface(this, "tmp");
				contenuCode = nouvelleInterface.interpreterCode(contenuCode);
				this.addInterface(nouvelleInterface);
				continue;
			} catch (ParseException p) {
				
			}

			try {
				Fonction nouvelleFonction = new Fonction(this,"tmp",false, "tmp");
				contenuCode = nouvelleFonction.interpreterCode(contenuCode);
				this.addFonction(nouvelleFonction);
				continue;
			} catch (ParseException p) {
				
			}
			try {
				Enum nouvelleEnum = new Enum(this, "tmp");
				contenuCode = nouvelleEnum.interpreterCode(contenuCode);
				this.addEnum(nouvelleEnum);
				continue;
			} catch (ParseException p) {
				
			}
			try {
				Attribut nouvelAttribut = new Attribut(this,"tmp", "tmp");
				contenuCode = nouvelAttribut.interpreterCode(contenuCode);
				this.addAttribut(nouvelAttribut);
				continue;
			} catch (ParseException p) {
				
			}
		}
		return c;
	}
	
	public String creerCode() {
		String res = "";
		res = modifieurs.toString();
		res += "class " + getNom() + " {\n\n";
		for(Attribut a:getAttributs())
			res += Code.tabuler(a.creerCode()) + ";\n";
		res += Code.tabuler("\n");
		for(Fonction f:getFonctions())
			res += Code.tabuler(f.creerCode()) + "\n";
		for(Interface i:getInterfaces())
			res += Code.tabuler(i.creerCode()) + "\n";
		for(Classe c:getClasses())
			res += Code.tabuler(c.creerCode()) + "\n";
		for(Enum e:getEnums())
			res += Code.tabuler(e.creerCode()) + "\n";
		
		res = res + "}\n";
		return res;
	}
	
	public void supprimer() {
		for(int i = 0; i < getAttributs().size();i++)
			getAttributs().get(i).supprimer();
		for(int i = 0; i < getInterfaces().size();i++)
			getInterfaces().get(i).supprimer();
		for(int i = 0; i < getClasses().size();i++)
			getClasses().get(i).supprimer();
		for(int i = 0; i < getFonctions().size();i++)
			getFonctions().get(i).supprimer();
		for(int i = 0; i < getEnums().size();i++)
			getEnums().get(i).supprimer();
		
		((IConteneurClasse)getPere()).removeClasse(this);
		
		fichier.removeClasse(this);
	}
	
	public List<Modifieur> getModifieurs() { return modifieurs.getModifieurs(); }
	public boolean hasModifieur(String modifieur) { return modifieurs.hasModifieur(modifieur); }
	public void addModifieur(Modifieur modifieur) { modifieurs.addModifieur(modifieur); }
	public void removeModifieur(Modifieur modifieur) { modifieurs.removeModifieur(modifieur); }
	
	public List<IConteneurDependance> getSousDependant() {
		List<IConteneurDependance> tous = new ArrayList<IConteneurDependance>(getClasses());
		tous.addAll(getInterfaces());
		tous.addAll(getAttributs());
		return tous;
	}
	public List<Dependance> getDependance() { return dependances.getDependance(); }	
	public List<Dependance> getAllDependance() {
		List<Dependance> tous = new ArrayList<Dependance>(dependances.getDependance());
		for(Interface i : getInterfaces())
			tous.addAll(i.getAllDependance());
		for(Classe c : getClasses())
			tous.addAll(c.getAllDependance());
		for(Fonction f : getFonctions())
			tous.addAll(f.getAllDependance());
		for(Attribut a : getAttributs())
			tous.addAll(a.getAllDependance());
		return tous;
	}
	public void addDependance(Dependance dependance) { dependances.addDependance(dependance); }
	public void removeDependance(Dependance dependance) { dependances.removeDependance(dependance); }
	
	public Attribut addAttribut(String nom, String type) { 
		Attribut a = new Attribut(this, nom, type);
		addAttribut(a);
		return a;
	}
	public Fonction addFonction(String nom, String type) { 
		Fonction f = new Fonction(this,nom, true, type);
		addFonction(f);
		return f;
	}
	public Classe addClasse(String nom) { 
		Classe c = new Classe(this, nom);
		addClasse(c);
		return c;
	}
	public Interface addInterface(String nom) { 
		Interface i = new Interface(this, nom);
		addInterface(i); 
		return i;
	}
	public Enum addEnum(String nom) {
		Enum e = new Enum(this, nom);
		addEnum(e); 
		return e;
	}
	
	public void addEnum(Enum e) { 
		enums.addElement(e); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).enumAjoute(this,e);
		notifierModification();
	}
	public void addInterface(Interface i) { 
		interfaces.addElement(i);
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).interfaceAjoute(this,i);
		notifierModification();
	}
	public void addClasse(Classe c) { 
		classes.addElement(c); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).classeAjoute(this,c);
		notifierModification();
	}
	public void addAttribut(Attribut a) { 
		attributs.addElement(a); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).attributAjoute(this,a);
		notifierModification();
	}
	public void addFonction(Fonction f) { 
		fonctions.addElement(f); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).fonctionAjout(this,f);
		notifierModification();
	}
	
	public void removeAttribut(Attribut a) { 
		attributs.removeElement(a);
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).attributRetire(this,a);
		notifierModification();
	}
	public void removeFonction(Fonction f) { 
		fonctions.removeElement(f);
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).fonctionRetire(this,f);
		notifierModification();
	}
	public void removeClasse(Classe c) { 
		classes.removeElement(c); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).classeRetire(this,c);
		notifierModification();
	}
	public void removeInterface(Interface i) { 
		interfaces.removeElement(i); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).interfaceRetire(this,i);
		notifierModification();
	}
	public void removeEnum(Enum e) { 
		enums.removeElement(e); 
		for(IObserveur o : getObserveurs())((IObserveurClasse)o).enumRetire(this,e);
		notifierModification();
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nouveauNom) throws ExceptionMauvaisNom { 
		nom = ExceptionMauvaisNom.testNom(nouveauNom); 
		for(IObserveur o : getObserveurs())((IObserveurNommable)o).changeNom(this);
		notifierModification();
	}
	
	public List<Attribut> getAttributs() { return attributs.getElements(); }
	public List<Fonction> getFonctions() { return fonctions.getElements(); }
	public List<Classe> getClasses() { return classes.getElements(); }
	public List<Interface> getInterfaces() { return interfaces.getElements(); }
	public List<Enum> getEnums() { return enums.getElements(); }
	
	public Classe getClasse(String nom) { for(INommableJava n:getClasses())if(n.getNom().equals(nom))return (Classe) n; return null; }
	public Interface getInterface(String nom) { for(INommableJava n:getInterfaces())if(n.getNom().equals(nom))return (Interface) n; return null; }
	public Enum getEnum(String nom) { for(INommableJava n:getEnums())if(n.getNom().equals(nom))return (Enum) n; return null; }
	public Attribut getAttribut(String nom) { for(INommableJava n:getAttributs())if(n.getNom().equals(nom))return (Attribut) n; return null; }
	public Fonction getFonction(String nom) { for(INommableJava n:getFonctions())if(n.getNom().equals(nom))return (Fonction) n; return null; }
	
	public void detruireAttribut(Attribut a) { a.supprimer(); }
	public void detruireFonction(Fonction a) { a.supprimer(); }
	public void detruireEnum(Enum a) { a.supprimer(); }
	public void detruireClasse(Classe a) { a.supprimer(); }
	public void detruireInterface(Interface a) { a.supprimer(); }

	public Fichier getFichier() { return fichier; }
	public void setFichier(Fichier f) { fichier = f; }
	
	public List<String> getStringPath() {
		INoeudNommable courant = (INoeudNommable)getPere();
		List<String> chemin = new ArrayList<String>();
	
		while(!(courant instanceof Projet)) {
			chemin.add(courant.getNom());
			courant = (INoeudNommable)courant.getPere();
		}
	
		Collections.reverse(chemin);
		return chemin;
	}
}
