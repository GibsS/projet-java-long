package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Contrat.Conteneur.IConteneurFonction;
import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.Observeur.IObserveur;

public interface IObserveurConteneurFonction extends IObserveur {

	void fonctionAjout(IConteneurFonction i, Fonction a);
	void fonctionRetire(IConteneurFonction i, Fonction a);
}
