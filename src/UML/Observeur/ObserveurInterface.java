package UML.Observeur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Application.GestionProjet.FenetreDialogue;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.Contrat.Conteneur.IConteneurAttribut;
import Noyau.Arbre.Contrat.Conteneur.IConteneurEnum;
import Noyau.Arbre.Contrat.Conteneur.IConteneurFonction;
import Noyau.Arbre.ElementJava.Attribut;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Arbre.Observeur.Conception.IObserveurInterface;
import Noyau.Dependance.Dependance;
import Noyau.Dependance.IConteneurDependance;
import Noyau.Modifieur.IConteneurModifieur;
import Noyau.Modifieur.Modifieur;
import Schema.Boite;
import Schema.Observeur.IObserveurBoite;
import UML.SchemaUML;

public class ObserveurInterface implements IObserveurInterface, IObserveurBoite {

	private List<ObserveurEnum>	 listeObserveursEnum;
	
	private Boite boite;
	
	private Interface interf;
	private List<Attribut> attributs = interf.getAttributs();
	private List<Fonction> fonctions = interf.getFonctions();
	
	private SchemaUML schemaUML;
	
	public ObserveurInterface(SchemaUML schuml, Interface i) {
		this.listeObserveursEnum = new ArrayList<ObserveurEnum>();
		this.schemaUML=schuml;
		int nbElements = i.getAttributs().size()+i.getEnums().size()+i.getFonctions().size();
		int hauteurBoite = nbElements/10;
		//this.boite = schemaUML.getSchema().addBoite(2,hauteurBoite);
		i.addObserveur(this);
		this.interf=i;
	}
	
	public Interface getInterface() {
		return this.interf;
	}
	
	public List<ObserveurEnum> getObserveursEnum() {
		return this.listeObserveursEnum;
	}
	
	public void supprimer() {
		for(ObserveurEnum oe : this.getObserveursEnum()) {
			oe.supprimer();
		}
		// supprimer l'observateur interface
	}

	@Override
	public void changeNom(INoeudNommable n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifieurAjoute(IConteneurModifieur i, Modifieur a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifieurRetire(IConteneurModifieur i, Modifieur a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dependanceAjoute(IConteneurDependance c, Dependance a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dependanceRetire(IConteneurDependance c, Dependance a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enumAjoute(IConteneurEnum i, Enum a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enumRetire(IConteneurEnum i, Enum a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attributAjoute(IConteneurAttribut i, Attribut f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attributRetire(IConteneurAttribut i, Attribut a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fonctionAjout(IConteneurFonction i, Fonction a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fonctionRetire(IConteneurFonction i, Fonction a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		
		// Interface
				if(categorie==0) {
					JPopupMenu menu = new JPopupMenu ();
			    	
			    	JMenuItem rename = new JMenuItem("renommer interface");
			    	JMenuItem suppr = new JMenuItem("supprimer interface");
			    	
			    	rename.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							FenetreDialogue.modifierInterface(interf);
						}
			    	});
			    	
				    suppr.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							interf.supprimer();
						}
			    	});
			    	
			    	menu.add (rename);
			    	menu.add (suppr);

			    	menu.show(e.getComponent(), e.getX(), e.getY());
				}
				
				// Attribut
				else if(categorie==1) {
					final int rangfin = rang;
					JPopupMenu menu = new JPopupMenu ();
			    	
			    	JMenuItem modif = new JMenuItem("modifier attribut");
			    	JMenuItem suppr = new JMenuItem("supprimer attribut");
			    	
			    	modif.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							FenetreDialogue.modifierAttribut(attributs.get(rangfin));
						}
			    	});
			    	
				    suppr.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							attributs.get(rangfin).supprimer();
						}
			    	});
			    	
			    	menu.add (modif);
			    	menu.add (suppr);

			    	menu.show(e.getComponent(), e.getX(), e.getY());
				}
				
				// Fonction
				else if(categorie==2) {
					final int rangfin = rang;
					JPopupMenu menu = new JPopupMenu ();
					    
					JMenuItem modif = new JMenuItem("modifier fonction");
					JMenuItem suppr = new JMenuItem("supprimer fonction");
					
			    	modif.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							FenetreDialogue.modifierFonction(fonctions.get(rangfin));
						}
					});
					    	
					suppr.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							fonctions.get(rangfin).supprimer();
						}
				   	});
					    	
					menu.add (modif);
					menu.add (suppr);

					menu.show(e.getComponent(), e.getX(), e.getY());
				}
		
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		JPopupMenu menu = new JPopupMenu ();
    	
    	JMenuItem modif = new JMenuItem("modifier interface");
    	JMenuItem suppr = new JMenuItem("supprimer interface");
    	
    	modif.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.modifierInterface(interf);
			}
    	});
    	
	    suppr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				interf.supprimer();
			}
    	});
    	
    	menu.add (modif);
    	menu.add (suppr);

    	menu.show(e.getComponent(), e.getX(), e.getY());
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
