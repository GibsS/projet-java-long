package Noyau.Arbre.Observeur.Conception;

import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.ElementJava.Enum;

public interface IObserveurEnum extends IObserveurNommable {

	void nouveauMembre(Enum e, String membre);
	void suppressionMembre(Enum e, String membre);
	
	void changementMembre(Enum e, String membre);
}
