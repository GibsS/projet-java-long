package Noyau.Arbre.Contrat.Conteneur;

import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Enum;

/** l'interface IConteneurEnum d�finit un conteneur d'enum, les classes et les paquets
 * en font partie.
 * @author gibson
 */
public interface IConteneurEnum extends INoeudNommable {
	
	Enum addEnum(String nom);
	void addEnum(Enum e);
	
	Enum getEnum(String nom);
	List<Enum> getEnums();
	
	void removeEnum(Enum a);
	void detruireEnum(Enum a);
}
