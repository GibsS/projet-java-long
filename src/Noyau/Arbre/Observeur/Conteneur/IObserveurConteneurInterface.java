package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Contrat.Conteneur.IConteneurInterface;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.Observeur.IObserveur;

public interface IObserveurConteneurInterface extends IObserveur {

	void interfaceAjoute(IConteneurInterface i, Interface a);
	void interfaceRetire(IConteneurInterface i, Interface a);
}
