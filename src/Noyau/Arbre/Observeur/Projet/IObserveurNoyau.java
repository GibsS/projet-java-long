package Noyau.Arbre.Observeur.Projet;

import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Projet.Noyau;
import Noyau.Arbre.Projet.Projet;

public interface IObserveurNoyau extends IObserveur {

	void projetRetire(Noyau n, Projet p);
	void projetAjout(Noyau n, Projet p);
}
