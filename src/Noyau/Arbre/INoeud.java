package Noyau.Arbre;

import java.util.List;

import Noyau.Arbre.Observeur.IObserveur;

public interface INoeud {

	INoeud getPere();
	void setPere(INoeud n);
	
	void supprimer();
	
	void notifierModification();
	void notifierModificationFils(INoeud n);
	void notifierModificationGrandFils(INoeud n);
	
	List<IObserveur> getObserveurs();
	void addObserveur(IObserveur o);
	void removeObserveur(IObserveur o);
}
