package Noyau.Arbre;

import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;
import Noyau.GestionFichier.Fichier;

public interface INoeudFichier extends INoeud {

	void compiler();
	
	Fichier getFichier();
	void setFichier(Fichier f);
	
	String creerCode();
	Code interpreterCode(Code code) throws ParseException;
}
