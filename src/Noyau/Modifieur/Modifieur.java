package Noyau.Modifieur;

import java.util.List;

public class Modifieur {
	
	public static String[] modifieursJava = new String[] {"public", "private", "protected", "package", "static", "abstract", "final"};
	
	String nom;
	
	public Modifieur(String nom) throws ExceptionModifieurIncorrect {
		boolean correct = false;
		for(String m : modifieursJava)
			if(m.equals(nom)) {
				correct = true;
				break;
			}
		if(correct) {
			this.nom = nom;
		} else {
			throw new ExceptionModifieurIncorrect();
		}
	}
	
	public String toString() {
		return nom;
	}
	
	public static String[] getModifieursJava() {
		return modifieursJava;
	}
	
	public static String getSigle(List<Modifieur> lm) {
		String res = "";
		for(Modifieur m : lm) {
			if (m.toString().equals("public")) {
				res+="+";
			} else if (m.toString().equals("private")) {
				res+="-";
			} else if (m.toString().equals("protected")) {
				res+="#";
			} else if (m.toString().equals("package")) {
				res+="/";
			} else if (m.toString().equals("static")) {
				res+="s";
			} else if (m.toString().equals("abstract")) {
				res+="a";
			} else {
				res+="f";
			}
		}
		return res;
	}
}
