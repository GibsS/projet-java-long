package Noyau.Arbre.Observeur.Conception;

import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conteneur.*;

public interface IObserveurInterface extends IObserveurNommable, IObserveurConteneurModifieur, 
											 IObserveurConteneurDependance, IObserveurConteneurEnum, 
											 IObserveurConteneurAttribut, IObserveurConteneurFonction {

}
