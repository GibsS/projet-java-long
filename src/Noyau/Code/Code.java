package Noyau.Code;

import java.util.ArrayList;
import java.util.List;

import Noyau.Modifieur.ExceptionModifieurIncorrect;
import Noyau.Modifieur.Modifieur;

public class Code {

	String code;
	
	public Code(String code) {
		this.code = code;
	}
	
	public Code(Code c) {
		this.code = c.toString();
	}
	
	public String toString() {
		return code;
	}
	
	public boolean isEmpty() {
		return code == null || code == "";
	}
	
	public void extraireMot(String mot) throws ParseException {
		char[] motChar = mot.toCharArray();
		char[] texteChar = code.toCharArray();
		
		if(code.length() >= mot.length()) {
			for(int k = 0; k < mot.length(); k++) {
				if(motChar[k] != texteChar[k])
					throw new ParseException("Mot non d�tect�");		
			}
		
			code = code.substring(mot.length(), code.length());
		} else {
			throw new ParseException("plus de place");
		}
	}
	
	public Code extraireVersMot(String mot) throws ParseException {
		char[] motChar = mot.toCharArray();
		char[] texteChar = code.toCharArray();
		int i = 0, j;
		
		while(i < texteChar.length) {
			j = 0;
			while(j < motChar.length && i+j < texteChar.length && motChar[j] == texteChar[i+j]){
				j++;
				
			}
			
			if(j == motChar.length) {
				Code retour = new Code(code.substring(0, i));
				code = code.substring(i+motChar.length, code.length());
				return retour;
			} 
			i++;
		}
		throw new ParseException("le mot n'est pas pr�sent");
	}
	
	public Code extraireCrochet() throws ParseException {
		int i = 0;
		StringBuilder extrait = new StringBuilder();
		int nombreParenthese = 0;
		
		if(code.toCharArray()[0] != '{')
			throw new ParseException("pas une parenthese");
		
		for(char c:code.toCharArray()) {
			switch(c) {
			case '{' : nombreParenthese ++;extrait.append(c); i++;break;
			case '}' : nombreParenthese --; extrait.append(c); i++;break;
			default : extrait.append(c); i++; break;
			}

			if(nombreParenthese == 0) {
				extrait.deleteCharAt(extrait.length()-1);
				extrait.deleteCharAt(0);
				code = code.substring(i, code.length());
				return new Code(extrait.toString()); 	
			}
		}
		
			throw new ParseException("La parenth�se n'est pas referm�");
	}
	
	public Code extraireParenthese() throws ParseException {
		int i = 0;
		StringBuilder extrait = new StringBuilder();
		int nombreParenthese = 0;
		
		if(code.toCharArray()[0] != '(')
			throw new ParseException("pas une parenthese");
		
		for(char c:code.toCharArray()) {
			switch(c) {
			case '(' : nombreParenthese ++;extrait.append(c); i++;break;
			case ')' : nombreParenthese --; extrait.append(c); i++;break;
			default : extrait.append(c); i++; break;
			}

			if(nombreParenthese == 0) {
				extrait.deleteCharAt(extrait.length()-1);
				extrait.deleteCharAt(0);
				code = code.substring(i, code.length());
				return new Code(extrait.toString()); 	
			}
		}
		
		throw new ParseException("La parenth�se n'est pas referm�");
	}
	
	public void extraireEspaceInutile()  {
		int i = 0;
		
		for(char c:code.toCharArray()) {
			switch(c) {
				case '\t' :
				case '\n' :
				case ' ' : i++; break;
				default : 
					//System.out.println(i);
					code = code.substring(i, code.length());
					return;
			}
		}
	}

	public void extraireEspace() throws ParseException {
		int i = 0;
		
		//System.out.println(code + "--" + code.substring(1, code.length()));
		for(char c:code.toCharArray()) {
			switch(c) {
				case '\t' :
				case '\n' :
				case ' ' : i++; 
				break;
				default : code = code.substring(i, code.length());return;
			}
		}
		//System.out.println(code);
		
		if(i == 0)
			throw new ParseException("Aucun espace libre");
	}
	
	public static String tabuler(String s) {
		if(s != null && s != "") {
			int index = 0;
			while((index = s.indexOf("\n", index)) != -1) {
				s = s.substring(0,index+1) + "\t" + s.substring(index+1, s.length());
				index ++;
			}
			return "\t" + s.substring(0, s.length()-1);
		} else {
			return "";
		}
	}
	
	public List<Modifieur> extraireModifieurs() throws ParseException {
		String backup = code;
		List<Modifieur> modifieursTrouve = null;
			
		modifieursTrouve = new ArrayList<Modifieur>();
		boolean motTrouve;
		
		do {
			motTrouve = false;
			for(String m : Modifieur.modifieursJava) {
				try {
					extraireEspaceInutile();
					extraireMot(m);
					try {
						modifieursTrouve.add(new Modifieur(m));
					} catch (ExceptionModifieurIncorrect e) {
						
					}
					motTrouve = true;

					break;
				} catch(ParseException p ) {
						
				}
			}
		} while(motTrouve);
		
		//System.out.println(code);
		return modifieursTrouve;
	}
	
	public String lireMot() {
		char[] texte = code.toCharArray();
		int i = 0;
		
		while(i < texte.length && texte[i] != ' ' && texte[i] != '\t' && texte[i] != '\n' 
				&& texte[i] != '{' && texte[i] != '}' && texte[i] != '(' && texte[i] != ')' && texte[i] != ';' 
				&& texte[i] != '=' && texte[i] != ',')
			i++;
		
		String reste = code.substring(0, i);
		code = code.substring(i, code.length());
		return reste;
	}
	
	@SuppressWarnings("serial")
	public class ParseException extends Exception {
		public ParseException(String message) {
			super(message);
		}
	}
}
