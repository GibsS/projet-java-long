package Noyau.Modifieur;

import java.util.ArrayList;
import java.util.List;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.Observeur.Conteneur.*;
import Noyau.Arbre.Observeur.IObserveur;

public class ConteneurModifieur implements IConteneurModifieur {

	List<Modifieur> modifieurs;
	INoeud pere;
	
	public ConteneurModifieur(INoeud pere) {
		this.pere = pere;
		modifieurs = new ArrayList<Modifieur>();
	}
	
	public List<Modifieur> getModifieurs() {
		return modifieurs;
	}

	public boolean hasModifieur(String modifieur) {
		for(Modifieur m: modifieurs)
			if(m.toString().equals(modifieur))
				return true;
		return false;
	}

	public void addModifieur(Modifieur modifieur) {
		modifieurs.add(modifieur);
		for(IObserveur o : pere.getObserveurs())((IObserveurConteneurModifieur)o).modifieurAjoute((IConteneurModifieur) pere,modifieur);
		if(pere.getPere() != null)pere.getPere().notifierModificationFils(pere);
	}

	public void removeModifieur(Modifieur modifieur) {
		modifieurs.remove(modifieur);
		for(IObserveur o : pere.getObserveurs())((IObserveurConteneurModifieur)o).modifieurRetire((IConteneurModifieur) pere,modifieur);
		pere.getPere().notifierModificationFils(pere);
	}
	
	public String toString() {
		String res = "";
		for(Modifieur m: modifieurs) {
			res += m.toString() + " ";
		}
		return res;
	}

}
