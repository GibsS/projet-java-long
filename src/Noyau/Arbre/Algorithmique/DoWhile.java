package Noyau.Arbre.Algorithmique;

import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurDoWhile;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;

/** La classe DoWhile contient toutes les informations pertinentes � la structure de controle
 * du do while : �a s�quence interne et sa condition de sortie de boucle.
 * @author gibson
 */
public class DoWhile extends Algorithme {
	Expression condition;
	Sequence contenu;
	
	public DoWhile(Sequence n) {
		super(n);
		condition = new Expression();
		contenu = new Sequence(this);
	}

	public String creerCode() {
		return "do {\n" + Code.tabuler(contenu.creerCode()) + "} while(" + condition.creerCode() + ")\n";
	}

	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code crochet;
		Code condition;
		
		c.extraireEspaceInutile();
		c.extraireMot("do");
		c.extraireEspaceInutile();
		crochet = c.extraireCrochet();
		contenu.interpreterCode(crochet);
		c.extraireEspaceInutile();
		c.extraireMot("while");
		c.extraireEspaceInutile();
		condition = c.extraireParenthese();
		this.condition.interpreterCode(condition);
		c.extraireEspaceInutile();
		c.extraireMot(";");
		return c;
	}

	public void setCondition(Expression e) {
		condition = e;
		for(IObserveur o : getObserveurs())((IObserveurDoWhile)o).changeExpression(this);
		notifierModification();
	}

	public Sequence getContenu() {
		return contenu;
	}
	
	public Expression getCondition() {
		return condition;
	}
}
