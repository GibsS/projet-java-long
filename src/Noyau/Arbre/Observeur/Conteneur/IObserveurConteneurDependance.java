package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Dependance.Dependance;
import Noyau.Dependance.IConteneurDependance;

public interface IObserveurConteneurDependance extends IObserveur {

	void dependanceAjoute(IConteneurDependance c, Dependance a);
	void dependanceRetire(IConteneurDependance c, Dependance a);
}
