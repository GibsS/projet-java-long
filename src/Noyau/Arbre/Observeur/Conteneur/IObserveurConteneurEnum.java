package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Contrat.Conteneur.IConteneurEnum;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.Observeur.IObserveur;

public interface IObserveurConteneurEnum extends IObserveur {

	void enumAjoute(IConteneurEnum i, Enum a);
	void enumRetire(IConteneurEnum i, Enum a);
}
