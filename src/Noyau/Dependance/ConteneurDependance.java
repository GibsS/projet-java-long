package Noyau.Dependance;

import java.util.ArrayList;
import java.util.List;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.Observeur.Conteneur.*;
import Noyau.Arbre.Observeur.IObserveur;

public class ConteneurDependance implements IConteneurDependance {

	List<Dependance> dependances;
	INoeud pere;
	
	public ConteneurDependance(INoeud pere) {
		this.pere = pere;
		dependances = new ArrayList<Dependance>();
	}
	
	public List<IConteneurDependance> getSousDependant() {
		return null;
	}

	public List<Dependance> getDependance() {
		return dependances;
	}
	
	public List<Dependance> getAllDependance() {
		return dependances;
	}

	public void addDependance(Dependance dependance) {
		dependances.add(dependance);
		for(IObserveur o : pere.getObserveurs())((IObserveurConteneurDependance)o).dependanceAjoute((IConteneurDependance) pere,dependance);
		pere.notifierModification();
	}

	public void removeDependance(Dependance dependance) {
		dependances.remove(dependance);
		for(IObserveur o : pere.getObserveurs())((IObserveurConteneurDependance)o).dependanceRetire((IConteneurDependance) pere,dependance);
		pere.notifierModification();
	}
}
