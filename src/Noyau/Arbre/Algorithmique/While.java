package Noyau.Arbre.Algorithmique;

import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.Algorithmique.IObserveurWhile;
import Noyau.Code.Code;
import Noyau.Code.Code.ParseException;

/** La classe While contient toutes les informations pertinentes � la structure de controle
 * du While : �a s�quence interne et sa condition d'ex�cution de boucle.
 * @author gibson
 */
public class While extends Algorithme {
	Expression condition;
	Sequence contenu;
	
	public While(Sequence sequence) {
		super(sequence);
		condition = new Expression();
		contenu = new Sequence(this);
	}

	public String creerCode() {
		return "while (" + condition.creerCode() + ") {\n" + Code.tabuler(contenu.creerCode()) + "}\n";
	}

	public Code interpreterCode(Code code) throws ParseException {
		Code c = new Code(code);
		Code conditionCode;
		Code contenuCode;
		
		c.extraireEspaceInutile();
		c.extraireMot("while");
		c.extraireEspaceInutile();
		conditionCode = c.extraireParenthese();
		condition.interpreterCode(conditionCode);
		c.extraireEspaceInutile();
		contenuCode = c.extraireCrochet();
		contenu.interpreterCode(contenuCode);
		return c;
	}
	
	public void setCondition(Expression c) {
		condition = c;
		for(IObserveur o : getObserveurs())((IObserveurWhile)o).changeCondition(this);
		notifierModification();
	}
	
	public Sequence getContenu() {
		return contenu;
	}
}
