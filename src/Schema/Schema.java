package Schema;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.*;
import java.util.*;
import java.util.Timer;
import java.awt.event.*;

import javax.swing.*;

import Schema.Observeur.IObserveurSchema;

/** Represente la partie dessin des boites
 * @author G33
 */
@SuppressWarnings("serial")
public class Schema extends JPanel {
	
	// boites et fleches
	//private List<Boite> boites; // la liste des boites presentes dans le schema
	private List<Fleche> fleches; // la liste des fleches presentes dans le schema
	private List<Boite> boites;
	private Boite grille[][];
	private Contexte contexte;
	
	// fleche
	private boolean enableFleche;
	private Fleche fleche;
	
	// movement
	private boolean enableMovement = true;

	// mouse input
	private Boite boiteSelected;
    private Point mousePositionStart;
    
    // drag
	private boolean enableDrag;
    private Point positionRelativeBoite;// l'ordonnee de la souris lors du debut d'un clic
    
    private Point offset;

	// donn�e du sch�ma
	public SettingSchema settings;
	
	// observeur
	private List<IObserveurSchema> observeur;
	
	/** Construit un schema a partir du nombre de boite elementaires et leur taille
	 * @param l		la largeur des boites elementaires
	 * @param h		la hauteur des boites elementaires
	 * @param nb_l	le nombre de boites sur la largeur
	 * @param nb_h	le nombre de boites sur la hauteur
	 */
	public Schema(int l, int h, int nb_l, int nb_h) {
		settings = new SettingSchema(20,5,l,h);
		
		boites = new ArrayList<Boite>();
		contexte = new Contexte(0,0,nb_l,nb_h,this);
		this.fleches = new ArrayList<Fleche>();
		grille = new Boite[nb_l][nb_h];
		
		this.addMouseListener(new ClicSouris());
		
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				repaint();
			}
		}, 50,50);
		
		offset = new Point(0,0);
		
		observeur = new ArrayList<IObserveurSchema>();
	}
	
	public Contexte getContexte() {
		return contexte;
	}
	
	public void setOffset(Point p) {
		if(p.x < 0)
			offset.x = 0;
		else if ( p.x+this.getSize().getWidth() > getLargeur()*settings.getLargeurPixel())
			offset.x = (int) (getLargeur()*settings.getLargeurPixel() - this.getSize().getWidth());
		else
			offset.x = p.x;
		
		if(p.y < 0)
			offset.y = 0;
		else if ( p.y+this.getSize().getHeight() > getHauteur()*settings.getHauteurPixel())
			offset.y = (int) (getHauteur()*settings.getHauteurPixel() - this.getSize().getHeight());
		else
			offset.y = p.y;
	}
	public void updateOffset() {
		setOffset(offset);
	}
	public int getXOffset() { return offset.x; }
	public int getYOffset() { return offset.y; }
	
	public Point screenToOffset(Point p) { return new Point(p.x + getXOffset(), p.y + getYOffset()); }
	public Point screenToGrid(Point p) { return new Point((p.x + getXOffset())/settings.getLargeurPixel(),(p.y + getYOffset())/settings.getHauteurPixel());	}
	
	public Point offsetToScreen(Point p) { return new Point(p.x - getXOffset(), p.y - getYOffset()); }
	public Point offsetToGrid(Point p) { return new Point(p.x /settings.getLargeurPixel(), p.y/settings.getHauteurPixel()); }
	
	public Point gridToScreen(Point p) { return new Point(p.x*settings.getLargeurPixel() - getXOffset(), p.y*settings.getHauteurPixel() - getYOffset()); }
	public Point gridToOffset(Point p) { return new Point(p.x*settings.getLargeurPixel(), p.y*settings.getHauteurPixel()); }
	
	// observeur
	public void addObserveur(IObserveurSchema o) { observeur.add(o); }
	public void removeObserveur(IObserveurSchema o) { observeur.remove(o); }
	
	// dimension
	public int getLargeur() { return grille.length; }
	public int getHauteur() { return grille[0].length; }
	
	// boite
	public Boite getBoite(int i, int j) { return grille[i][j]; }
	public boolean boitePresent(int i, int j) { return grille[i][j] != null; }
	
	public Boite addBoite(int x, int y, int larg, int haut) {
		if(canPlace(x,y,larg,haut,null)) {
			Boite b = new Boite(x, y, larg, haut,this);
			this.boites.add(b);

			for(int i=x;i<(larg+x);i++) {
				for(int j=y;j<(haut+y);j++) {
					this.grille[i][j] = (b);
				}
			}
			return b;
		} else {
			return null;
		}
	}
	
	public boolean canPlace(int x, int y, int largeur, int hauteur, Boite ignored) {
		boolean placeable = x >= 0 && 
							 y >= 0 &&
							 x + largeur <= getLargeur() && 
							 y + hauteur <= getHauteur();
		
		if(placeable) {
			for(int i = x; placeable && i < x + largeur;i++ )
				for(int j = y; placeable && j < y + hauteur; j++)
					if(boitePresent(i,j) && (getBoite(i,j) != ignored)) {
						placeable = false;
					}
		}
		return placeable;
	}
	
	public Boite addBoite(Boite b) {
		this.boites.add(b);

		for(int i=b.getX();i<(b.getLargeur()+b.getX());i++) {
			for(int j=b.getY();j<(b.getHauteur()+b.getY());j++) {
				this.grille[i][j] = b;
			}
		}
		return b;
	}
	
	public void removeBoite(Boite b) {
		for(int i=b.getX();i<(b.getLargeur()+b.getX());i++) {
			for(int j=b.getY();j<(b.getHauteur()+b.getY());j++) {
				this.grille[i][j] = null;
			}
		}
		this.boites.remove(b);
	}
	
	public void deplacerBoite(Boite b,int dx, int dy) {
		removeBoite(b);
		b.setX(b.getX()+dx);
		b.setY(b.getY()+dy);
		addBoite(b);
	}
	
	// fleche
	public Fleche addFleche(Fleche f, Boite b1, Boite b2) {
		f.setBoite(b1, b2);
		this.fleches.add(f);
		return f;
	}
	
	public void retirerFleche(Fleche f) {
		this.fleches.remove(f);
		(f.getBoiteDepart()).supprimerFleche(f);
		(f.getBoiteArrivee()).supprimerFleche(f);
	}
	
	public Fleche getFlecheProche(int x, int y, int seuil) {
		Fleche res = null;
		int distanceMin = Integer.MAX_VALUE;
		for(Fleche f : this.fleches) {
			int d = f.distanceToPoint(new Point(x,y));
			if (d<=seuil && d < distanceMin) {
				res=f;
				distanceMin = d;
			}
		}
		return res;
	}
	
	// dragging et fleche
	public void enableMovement() { this.enableMovement = true; }
	public void disableMovement() { this.enableMovement = false; }
	
	public void enableDragging() { this.enableDrag = true; this.enableFleche = false; }
	public void disableDragging() { this.enableDrag = false; cancelDragging(); }
	public void cancelDragging() { if(dragging()) this.boiteSelected = null;}
	
	public void enableFleche(Fleche f) { this.enableDrag = false; this.enableFleche = true; fleche = f; }
	public void disableFleche() { this.enableFleche = false; cancelFleche(); }
	public void cancelFleche() { if(drawingFleche()) this.boiteSelected = null; }
	
	public boolean dragging() { return enableDrag && boiteSelected != null; }
	public boolean drawingFleche() { return enableFleche && boiteSelected != null; }
	
	public class ClicSouris implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			Point p = screenToGrid(new Point(e.getX(), e.getY()));

			System.out.println(p);
			if(!boitePresent(p.x,p.y)) {
				if(SwingUtilities.isRightMouseButton(e))
					for(IObserveurSchema o : observeur)
						o.cliqueDroitCaseVide(p.x, p.y);
				if(SwingUtilities.isLeftMouseButton(e))
					for(IObserveurSchema o : observeur)
						o.cliqueGaucheCaseVide(p.x, p.y);
			} else {
				getBoite(p.x,p.y).mouseClicked(e);
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			mousePositionStart = screenToGrid(new Point(e.getX(),e.getY()));
			boiteSelected = getBoite(mousePositionStart.x,mousePositionStart.y);
				
			if((dragging() || drawingFleche()) && boiteSelected != null)
				positionRelativeBoite = new Point(mousePositionStart.x - boiteSelected.getX(), mousePositionStart.y - boiteSelected.getY());
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			Point positionEnd = screenToGrid(new Point(e.getX(),e.getY()));
			
			if(dragging()) {
				if(canPlace(positionEnd.x-positionRelativeBoite.x,positionEnd.y-positionRelativeBoite.y,boiteSelected.getLargeur(), boiteSelected.getHauteur(),boiteSelected)) {
					deplacerBoite(boiteSelected, positionEnd.x - mousePositionStart.x, positionEnd.y - mousePositionStart.y);
				} 

				cancelDragging();
			} else if(drawingFleche()) {
				Boite boiteEnd = getBoite(positionEnd.x, positionEnd.y);
				
				if(boiteEnd != null) {
					addFleche(fleche, boiteSelected, boiteEnd);
				}
				
				cancelFleche();
			} 
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
		
	}
	
	// affichage
	public void paint(Graphics g) {
		super.paintComponent(g);
		//setOffset(new Point(offset.x + 5,offset.y+5));
		updateOffset();
		
		Graphics2D g2d = (Graphics2D) g;
		Point positionMouse;
		Point positionDecalage;
		
		afficherPointille(g2d);
		
		contexte.afficherContenu(g);

		for(Boite b:boites) 
			b.afficherContenu(g);

		for(Fleche f:fleches) 
			f.afficherContenu(g);
		
		if(dragging()) {
			if(this.getMousePosition() != null) {
				System.out.println(getMousePosition());
				positionMouse = screenToGrid(new Point((int)this.getMousePosition().getX(), (int)this.getMousePosition().getY()));
				System.out.println(positionMouse);
				positionDecalage = gridToScreen(new Point(positionMouse.x -positionRelativeBoite.x, positionMouse.y - positionRelativeBoite.y));
				g.setColor(Color.RED);
				g.drawRect(positionDecalage.x, 
					   positionDecalage.y, 
					   boiteSelected.getLargeur()*settings.getLargeurPixel(),
					   boiteSelected.getHauteur()*settings.getHauteurPixel());
			}
		}
		
		if(getMousePosition() != null &&  enableMovement) {
			if(getMousePosition().getX() > getSize().getWidth() - 100)
				setOffset(new Point(offset.x + 5, offset.y));
			if(getMousePosition().getY() > getSize().getHeight() - 100)
				setOffset(new Point(offset.x, offset.y + 5));
			if(getMousePosition().getX() < 100)
				setOffset(new Point(offset.x - 5, offset.y));
			if(getMousePosition().getY() < 100)
				setOffset(new Point(offset.x, offset.y - 5));
		}
	}
	
	private void afficherPointille(Graphics2D g) {
		float[] style = {5,5}; // les pointillés seront aussi long que les blancs
		g.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,style,0.0f));
		int i;
		for(i=1;i<=getLargeur();i++) {
			Line2D l = new Line2D.Double(i*settings.largeurBoiteElementaire - getXOffset(), - getYOffset(), 
										i*settings.largeurBoiteElementaire - getXOffset(), this.getHauteur()*settings.largeurBoiteElementaire- getYOffset());
			g.draw(l);
		}
		for(i=1;i<=this.getHauteur();i++) {
			Line2D l = new Line2D.Double(-getXOffset(), i*settings.hauteurBoiteElementaire - getYOffset(), 
					                     getLargeur()*settings.largeurBoiteElementaire - getXOffset(), i*settings.hauteurBoiteElementaire - getYOffset());
			g.draw(l);
		}
		float[] style2 = {5,0}; // ligne pleine
		g.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,style2,0.0f));
		Line2D l1 = new Line2D.Double(0,0,2000,0);
		g.draw(l1);
	}
	
	public class SettingSchema {
		private int padding;
		private int paddingText;
		private int largeurBoiteElementaire;
		private int hauteurBoiteElementaire;
		
		private Font font;
		private FontRenderContext frc;
		private int textHeight;
		
		public SettingSchema(int padding, int paddingText, int largeurBoiteElementaire, int hauteurBoiteElementaire) {
			this.padding = padding;
			this.paddingText = paddingText;
			this.largeurBoiteElementaire = largeurBoiteElementaire;
			this.hauteurBoiteElementaire = hauteurBoiteElementaire;
			
			//System.out.println(largeurBoiteElementaire + " " + hauteurBoiteElementaire);
			
			AffineTransform affinetransform = new AffineTransform();
			frc = new FontRenderContext(affinetransform,true,true); 
			font = new Font("Arial", Font.PLAIN, 12);
			textHeight =(int)(font.getStringBounds("Test", frc).getHeight());
		}
		
		public int getPadding() { return padding; }	
		public int getPaddingText() { return paddingText; }		
		public int getLargeurPixel() { return largeurBoiteElementaire; }		
		public int getHauteurPixel() { return hauteurBoiteElementaire; }		
		public Font getFont() { return font; }
		public FontRenderContext getFontRender() { return frc; }
		public int getHeightText() { return textHeight; }
	}
	
	public SettingSchema getSettingSchema() {
		return this.settings;
	}
}
