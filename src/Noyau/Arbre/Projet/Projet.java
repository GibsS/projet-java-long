package Noyau.Arbre.Projet;

import Noyau.Arbre.INoeud;
import Noyau.Arbre.INommable;
import Noyau.Arbre.Contrat.Conteneur.IConteneurPaquet;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.GestionFichier.ProjetMachine;

public class Projet extends Paquet implements INommable, IConteneurPaquet {

	Noyau noyau;
	ProjetMachine associe;
	String nom;
	
	public Projet(Noyau noyau, String path, String nom) {
		super(null,nom);
		
		this.noyau = noyau;
		
		this.associe = new ProjetMachine(this, path);
	}
	
	public void setNom(String n) {
		this.nom = n;
	}
	public String getNom() {
		return nom;
	}
	
	public Projet getProjet() {
		return this;
	}
	
	public ProjetMachine getProjetMachine() { return associe; }
	public void miseAJour() { associe.miseAJour(); }
	
	public Noyau getNoyau() { return (Noyau) getPere(); }

	public void supprimer() {
		super.supprimer();
		noyau.removeProjet(this);
	}
	
	public void notifierModification() {
		super.notifierModification();
		if(noyau != null)
			noyau.notifierModificationFils(this);
	}
	
	public void notifierModificationFils(INoeud n) {
		super.notifierModificationFils(n);
		if(noyau != null)
			noyau.notifierModificationFils(n);
	}
	
	public void notifierModificationGrandFils(INoeud n) {
		super.notifierModificationGrandFils(n);
		if(noyau != null)
			noyau.notifierModificationGrandFils(n);
	}
}
