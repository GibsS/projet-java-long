package Noyau.Arbre.Observeur.Algorithmique;

import Noyau.Arbre.Algorithmique.For;
import Noyau.Arbre.Observeur.IObserveur;

/** l'interface IObserveurDoWhile repr�sente un type qui peut suivre la progression d'un dowhile.
 * @author gibson
 */
public interface IObserveurFor extends IObserveur {

	/** changeInstructionDebut :  est appel� quand le for "f" a chang� d'instruction d'initialisation. 
	 * param f : le for qui a chang�
	 */
	void changeInstructionDebut(For f);
	/** changeInstructionFin :  est appel� quand le for "f" a chang� d'instruction de fin de boucle. 
	 * param f : le for qui a chang�
	 */
	void changeInstructionFin(For f);
	/** changeCondition :  est appel� quand le for "f" a chang� la condition d'ex�cution de boucle. 
	 * param f : le for qui a chang�
	 */
	void changeCondition(For f);
}
