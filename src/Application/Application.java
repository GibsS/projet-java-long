package Application;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Application.GestionProjet.GestionProjet;
import Logigramme.SchemaLogigramme;
import Noyau.Arbre.Projet.Noyau;
import UML.SchemaUML;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Application extends JFrame {
	
	private Noyau noyau;
	
	private GestionProjet gproj;
	
	private JPanel panneau;
	private SchemaUML schemaUML;
	private SchemaLogigramme schemaLogigramme;
	
	private String panneauActuel;
	
	private CardLayout cl;

	public Application() {
		super("Assistant JAVA");
		
		this.noyau=new Noyau();
		this.gproj=new GestionProjet(noyau);
		
		this.panneau = new JPanel();
		this.schemaUML = new SchemaUML(this);
		this.schemaLogigramme = new SchemaLogigramme(this);
		this.cl = new CardLayout();
		
		this.creerInterface();
	}
	
	public void creerInterface() {
		Container contenu = this.getContentPane();
		contenu.setLayout(new BorderLayout());
		//contenu.removeAll();
			
		// creation du gestionnaire de projet 
		contenu.add(gproj,BorderLayout.WEST);
		gproj.setPreferredSize(new Dimension(200,100));
		
		contenu.add(panneau,BorderLayout.CENTER);
		panneau.setLayout(cl);
		panneau.add(this.schemaUML,"UML");
		panneau.add(this.schemaLogigramme,"Logigramme");
		cl.show(panneau,"UML");
		this.panneauActuel="UML";
		//contenu.add(this.schemaUML,BorderLayout.CENTER);
		//contenu.add(this.schemaLogigramme,BorderLayout.CENTER);
		/*Schema s = new Schema(100,100,12,8);
		s.addBoite(0,0,1,1);
		s.addBoite(2,3,2,2);
		s.addBoite(5,2,3,2);
		contenu.add(s,BorderLayout.CENTER);*/
		//panel = new JPanel();
		//contenu.add(panel);
		//panel.setBackground(Color.RED);
		//panel.add(schemaLogigramme);
		
		// creation du menu
		JMenuBar menu = new JMenuBar();
		JMenu fichier = new JMenu("Fichier");
		JMenu changerPanneau = new JMenu("Changer de panneau");
		this.setJMenuBar(menu);
		menu.add(fichier);
		JMenuItem nouveau_projet = new JMenuItem("Nouveau projet");
		fichier.add(nouveau_projet);
		JMenuItem ouvrir = new JMenuItem("Ouvrir");
		fichier.add(ouvrir);
		JMenuItem enregistrer = new JMenuItem("Enregistrer");
		fichier.add(enregistrer);
		JMenuItem enregistrer_sous = new JMenuItem("Enregistrer sous");
		fichier.add(enregistrer_sous);
		JMenuItem enregistrer_tout = new JMenuItem("Enregistrer tout");
		fichier.add(enregistrer_tout);
		JMenuItem quitter = new JMenuItem("Quitter");
		fichier.add(quitter);
		menu.add(changerPanneau);
		JMenuItem panneauLogigramme = new JMenuItem("Logigramme");
		changerPanneau.add(panneauLogigramme);
		JMenuItem panneauUML = new JMenuItem("UML");
		changerPanneau.add(panneauUML);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		nouveau_projet.addActionListener(new ActionNouveauProjet());
		ouvrir.addActionListener(new ActionOuvrir());
		enregistrer.addActionListener(new ActionEnregistrer());
		enregistrer_sous.addActionListener(new ActionEnregistrerSous());
		enregistrer_tout.addActionListener(new ActionEnregistrerTout());
		quitter.addActionListener(new ActionQuitter());
		panneauLogigramme.addActionListener(new ActionPanneauLogigramme());
		panneauUML.addActionListener(new ActionPanneauUML());
		
		//this.setPreferredSize(new Dimension(1000,500));
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.pack();
		this.setVisible(true);
	}
	
	public void changerPanneau(String contexte, Object[] argument) {
		this.panneau.removeAll();
		
		if(contexte.equals("UML")) {
			panneau.add(schemaUML,BorderLayout.CENTER);
			this.panneauActuel="UML";
			/*schemaUML.setPreferredSize(schemaUML.getMaximumSize());
			panel.set(BorderLayout.CENTER); */
		} else {
			panneau.add(schemaLogigramme);
			this.panneauActuel="Logigramme";
		}
		this.pack();
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public void changerPanneau(String contexte) {
		if(contexte.equals("UML")) {
			cl.show(panneau,"UML");
			this.panneauActuel="UML";
		} else {
			cl.show(panneau,"Logigramme");
			this.panneauActuel="Logigramme";
		}
	}
	
	class ActionPanneauLogigramme implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			changerPanneau("Logigramme");
		}
	}
	
	class ActionPanneauUML implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			changerPanneau("UML");
		}
	}
	
	class ActionQuitter implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			System.exit(0);
		}
	}
	
	class ActionNouveauProjet implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			
		}
	}
	
	class ActionOuvrir implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			
		}
	}
	
	class ActionEnregistrer implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			
		}
	}
	
	class ActionEnregistrerSous implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			
		}
	}
	
	class ActionEnregistrerTout implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			
		}
	}
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Application();
			}
		});
	}
}
