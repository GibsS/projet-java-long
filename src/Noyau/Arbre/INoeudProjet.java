package Noyau.Arbre;

import Noyau.Arbre.Projet.Projet;

public interface INoeudProjet extends INoeudNommable {

	Projet getProjet();
}
