package Noyau.Arbre.Contrat.Conteneur;

import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Fonction;

/** l'interface IConteneurFonction d�finit un conteneur de fonction, les classes et les interfaces
 * en font partie.
 * @author gibson
 */
public interface IConteneurFonction extends INoeudNommable {

	Fonction addFonction(String nom, String type);
	void addFonction(Fonction c);
	
	Fonction getFonction(String nom);
	List<Fonction> getFonctions();
	
	void removeFonction(Fonction a);
	void detruireFonction(Fonction a);
}
