package UML.Observeur;

import java.util.ArrayList;
import java.util.List;

import Application.GestionProjet.FenetreDialogue;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.Contrat.Conteneur.IConteneurClasse;
import Noyau.Arbre.Contrat.Conteneur.IConteneurEnum;
import Noyau.Arbre.Contrat.Conteneur.IConteneurInterface;
import Noyau.Arbre.Contrat.Conteneur.IConteneurPaquet;
import Noyau.Arbre.ElementJava.Classe;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Interface;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Arbre.Observeur.Conception.IObserveurPaquet;
import UML.SchemaUML;

public class ObserveurPaquet implements IObserveurPaquet {

	private List<ObserveurClasse> listeObserveursClasse;
	private List<ObserveurInterface> listeObserveursInterface;
	private List<ObserveurEnum> listeObserveursEnum;
	private List<ObserveurPaquet> listeObserveursPaquet;
	
	private Paquet paquet;
	
	private SchemaUML schemaUML;
	
	public ObserveurPaquet(SchemaUML schuml, Paquet p) {
		this.listeObserveursClasse = new ArrayList<ObserveurClasse>();
		this.listeObserveursInterface = new ArrayList<ObserveurInterface>();
		this.listeObserveursEnum = new ArrayList<ObserveurEnum>();
		this.schemaUML=schuml;
		p.addObserveur(this);
		this.paquet=p;
	}
	
	public Paquet getPaquet() {
		return this.paquet;
	}
	
	public ObserveurPaquet getObserveur(Paquet p) {
		ObserveurPaquet op = null;
		for(ObserveurPaquet obsp : this.listeObserveursPaquet) {
			if(obsp.getPaquet()==p) {
				op=obsp;
			}
		}
		return op;
	}
	
	public ObserveurClasse getObserveur(Classe c) {
		ObserveurClasse oc = null;
		for(ObserveurClasse obsc : this.listeObserveursClasse) {
			if(obsc.getClasse()==c) {
				oc=obsc;
			}
		}
		return oc;
	}
	
	public ObserveurInterface getObserveur(Interface i) {
		ObserveurInterface oi = null;
		for(ObserveurInterface obsi : this.listeObserveursInterface) {
			if(obsi.getInterface()==i) {
				oi=obsi;
			}
		}
		return oi;
	}
	
	public ObserveurEnum getObserveur(Enum e) {
		ObserveurEnum oe = null;
		for(ObserveurEnum obse : this.listeObserveursEnum) {
			if(obse.getEnum()==e) {
				oe=obse;
			}
		}
		return oe;
	}
	
	public List<ObserveurClasse> getObserveursClasse() {
		return this.listeObserveursClasse;
	}
	
	public List<ObserveurInterface> getObserveursInterface() {
		return this.listeObserveursInterface;
	}
	
	public List<ObserveurEnum> getObserveursEnum() {
		return this.listeObserveursEnum;
	}
	
	public List<ObserveurPaquet> getObserveursPaquet() {
		return this.listeObserveursPaquet;
	}
	
	public void supprimer() {
		for(ObserveurClasse oc : this.getObserveursClasse()) {
			oc.supprimer();
		}
		for(ObserveurInterface oi : this.getObserveursInterface()) {
			oi.supprimer();
		}
		for(ObserveurEnum oe : this.getObserveursEnum()) {
			oe.supprimer();
		}
		for(ObserveurPaquet op : this.getObserveursPaquet()) {
			op.supprimer();
		}
		// supprimer l'observateur paquet		
	}

	@Override
	public void changeNom(INoeudNommable n) {
		FenetreDialogue.renommerNoeud(n);
	}

	@Override
	public void update(INoeud n) {
		
	}

	@Override
	public void updateFils(INoeud n) {
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		
	}

	@Override
	public void paquetAjoute(IConteneurPaquet i, Paquet a) {
		this.listeObserveursPaquet.add(new ObserveurPaquet(schemaUML,a));
	}

	@Override
	public void paquetRetire(IConteneurPaquet i, Paquet a) {
		ObserveurPaquet obsa = this.getObserveur(a);
		obsa.supprimer();
	}

	@Override
	public void classeAjoute(IConteneurClasse i, Classe a) {
		this.listeObserveursClasse.add(new ObserveurClasse(schemaUML,a));
	}

	@Override
	public void classeRetire(IConteneurClasse i, Classe a) {
		ObserveurClasse obsa = this.getObserveur(a);
		obsa.supprimer();
	}

	@Override
	public void interfaceAjoute(IConteneurInterface i, Interface a) {
		this.listeObserveursInterface.add(new ObserveurInterface(schemaUML,a));
	}

	@Override
	public void interfaceRetire(IConteneurInterface i, Interface a) {
		ObserveurInterface obsa = this.getObserveur(a);
		obsa.supprimer();
	}

	@Override
	public void enumAjoute(IConteneurEnum i, Enum a) {
		this.listeObserveursEnum.add(new ObserveurEnum(schemaUML,a));
	}

	@Override
	public void enumRetire(IConteneurEnum i, Enum a) {
		ObserveurEnum obsa = this.getObserveur(a);
		obsa.supprimer();
	}
	
}
