package Noyau.Arbre.ElementJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.INoeudProjet;
import Noyau.Arbre.INommableJava;
import Noyau.Arbre.ModeleNoeud;
import Noyau.Arbre.Contrat.Conteneur.*;
import Noyau.Arbre.Observeur.IObserveur;
import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conception.IObserveurPaquet;
import Noyau.Arbre.Projet.Projet;
import Noyau.Dependance.ConteneurDependance;
import Noyau.GestionFichier.Fichier;
import Noyau.Modifieur.ConteneurModifieur;

/** la classe Paquet représente un paquet Java : la liste de ces classes, les interfaces, les énumératins et les paquets
 * @author gibson
 */
public class Paquet extends ModeleNoeud implements INoeudProjet, IConteneurClasse, IConteneurInterface, IConteneurEnum, IConteneurPaquet, INommableJava  {
	
	List<Classe>    classes;
	List<Interface> interfaces;
	List<Enum>	  	enums;
	List<Paquet>	 paquets;
	
	ConteneurModifieur modifieurs;
	ConteneurDependance dependances;
	
	String nom;

	public Paquet(IConteneurPaquet j, String nom) {
		super(j);
		classes    = new ArrayList<Classe>();
		interfaces = new ArrayList<Interface>();
		enums      = new ArrayList<Enum>();
		paquets    = new ArrayList<Paquet>();
		
		setNom(nom);
	}
	
	public Projet getProjet() {
		if(getPere() != null)
			return ((INoeudProjet)getPere()).getProjet();
		else 
			return null;
	}
		
	public void compile() {
		List<Fichier> aCompiler = new ArrayList<Fichier>();
		for(Classe c: getClasses())
			if(!aCompiler.contains(c.getFichier()))
				aCompiler.add(c.getFichier());
		for(Interface i: getInterfaces())
			if(!aCompiler.contains(i.getFichier()))
				aCompiler.add(i.getFichier());
		for(Enum e: getEnums())
			if(!aCompiler.contains(e.getFichier()))
				aCompiler.add(e.getFichier());
		for(Paquet p: getPaquets())
			p.compile();
		
		for(Fichier f:aCompiler)
			f.creerCode();
	}
	
	public void supprimer() {	
		
		for(int i = 0; i < getClasses().size();i++)
			getClasses().get(i).supprimer();
		for(int i = 0; i < getInterfaces().size();i++)
			getInterfaces().get(i).supprimer();
		for(int i = 0; i < getEnums().size();i++)
			getEnums().get(i).supprimer();
		for(int i = 0; i < getPaquets().size();i++)
			getPaquets().get(i).supprimer();
		
		if(this.getPere() != null) 
			((IConteneurPaquet)this.getPere()).removePaquet(this);
	}

	public Classe addClasse(String nom) { 
		Classe c = new Classe(this, nom);
		classes.add(c); 
		return c;
	}
	public Interface addInterface(String nom) { 
		System.out.println("[Paquet] rajout d'interface");
		Interface i = new Interface(this, nom);
		interfaces.add(i); 
		return i;
	}
	public Enum addEnum(String nom) { 
		Enum e = new Enum(this, nom);
		enums.add(e); 
		return e;
	}
	public Paquet addPaquet(String nom) { 
		Paquet p = new Paquet(this, nom);
		paquets.add(p); 		
		return p;
	}
	
	public void addEnum(Enum e) { 
		enums.add(e); 
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).enumAjoute(this,e);
		notifierModification();
	}
	public void addInterface(Interface i) { 
		interfaces.add(i);
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).interfaceAjoute(this,i);
		notifierModification();
	}
	public void addClasse(Classe c) { 
		classes.add(c);
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).classeAjoute(this,c);
		notifierModification();
	}
	public void addPaquet(Paquet p) { 
		paquets.add(p); 
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).paquetAjoute(this,p);
		notifierModification();
	}
	
	public void removeClasse(Classe c) { 
		classes.remove(c); 
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).classeRetire(this,c);
		notifierModification();
	}
	public void removeInterface(Interface i) { 
		interfaces.remove(i); 
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).interfaceRetire(this,i);
		notifierModification();
	}
	public void removeEnum(Enum e) { 
		enums.remove(e); 
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).enumRetire(this,e);
		notifierModification();
	}
	public void removePaquet(Paquet p) {
		paquets.remove(p); 
		for(IObserveur o : getObserveurs())((IObserveurPaquet)o).paquetRetire(this,p);
		notifierModification();
	}
	
	public String getNom() { return nom;}
	public void setNom(String nouveauNom) throws ExceptionMauvaisNom { 
		nom = ExceptionMauvaisNom.testNom(nouveauNom); 
		for(IObserveur o : getObserveurs())((IObserveurNommable)o).changeNom(this);
		notifierModification();
	}
	
	public List<Classe> getClasses() { return classes; }
	public List<Interface> getInterfaces() { return interfaces; }
	public List<Enum> getEnums() { return enums; }
	public List<Paquet> getPaquets() { return paquets; }
	
	public Classe getClasse(String nom) { for(INommableJava n:classes)if(n.getNom().equals(nom))return (Classe) n; return null; }
	public Interface getInterface(String nom) { for(INommableJava n:interfaces)if(n.getNom().equals(nom))return (Interface) n; return null; }
	public Enum getEnum(String nom) { for(INommableJava n:enums)if(n.getNom().equals(nom))return (Enum) n; return null; }
	public Paquet getPaquet(String nom) { for(INommableJava n:paquets)if(n.getNom().equals(nom))return (Paquet) n; return null; }
	
	public void detruireEnum(Enum a) { a.supprimer(); }
	public void detruireClasse(Classe a) { a.supprimer(); }
	public void detruireInterface(Interface a) { a.supprimer(); }
	public void detruirePaquet(Paquet a) { a.supprimer(); }
	
	public List<String> getStringPath() {
		INoeudNommable courant = (INoeudNommable)getPere();
		List<String> chemin = new ArrayList<String>();
	
		while(!(courant instanceof Paquet)) {
			chemin.add(courant.getNom());
			courant = (INoeudNommable)courant.getPere();
		}
	
		Collections.reverse(chemin);
		return chemin;
	}
}
