package Schema;

import Schema.Observeur.IObserveurBoite;
import Schema.Schema.SettingSchema;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;
import java.util.*;
import java.awt.Color;

import javax.swing.SwingUtilities;

/** Represente une boite (UML ou logigramme)
 * @author G33
 */
public class Boite {
	
	// positionnement
	int x; // sa coordonnee en x (numero de colonne)
	int y; // sa coordonnee en y (numero de ligne)
	
	// dimension
	int largeur; // la largeur de la boite (en nombre de boites elementaires)
	int hauteur; // la hauteur de la boite (en nombre de boites elementaires)
	
	// contenu 
	List<List<String>> texteBoiteGauche; 
	List<List<String>> texteBoiteDroit;
	
	// fleches
	// 0 : cote supérieur gauche et suit le sens horaire
	Cote cote[];
	
	int largeurColonne;
	Schema schema;
	SettingSchema settings;
	
	private List<IObserveurBoite> observeur;
	
	class Cote { public List<Fleche> fleches = new ArrayList<Fleche>(); }
	
	/** Construit une boite en fonction de sa position et de sa taille
	 * @param x		sa position sur l'axe des abcsisses (numero de colonne)
	 * @param y		sa position sur l'axe des ordonnees (numero de ligne)
	 * @param larg	sa largeur (en nombre de boites elementaires)
	 * @param haut	sa hauteur (en nombre de boites elementaire)
	 */
	public Boite(int x, int y, int larg, int haut, Schema schema) {
		this.x=x;
		this.y=y;
		this.largeur=larg;
		this.hauteur=haut;
		this.texteBoiteDroit=new ArrayList<List<String>>();
		this.schema = schema;
		this.settings = schema.getSettingSchema();
		
		cote = new Cote[2*larg + 2*haut];
		for(int i = 0; i < 2*larg + 2*haut ; i++)
			cote[i] = new Cote();
	}
	
	public void addObserveur(IObserveurBoite o) { observeur.add(o); }
	public void removeObserveur(IObserveurBoite o) { observeur.remove(o); }
	
	// dimension et position 
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	public int getLargeur() { return this.largeur; }
	public int getHauteur() { return this.hauteur; }
	
	public void setX(int x) { this.x=x; }
	public void setY(int y) { this.y=y; }

	public void setLargeur(int l) { this.largeur=l; }
	public void setHauteur(int h) { this.hauteur=h; }
	
	// flèche
	public List<Fleche> getFlecheDroite(int i) { return this.cote[largeur + i].fleches; }
	public List<Fleche> getFlecheGauche(int i) { return this.cote[2*largeur+hauteur + (hauteur - i)].fleches; }
	public List<Fleche> getFlecheHaut(int i) { return this.cote[i].fleches; }
	public List<Fleche> getFlecheBas(int i) { return this.cote[largeur+hauteur + (largeur - i)].fleches; }
	
	public void rajouterFlecheHaut(int i, Fleche f) { this.cote[i].fleches.add(f);}
	public void rajouterFlecheBas(int i, Fleche f) { this.cote[2*largeur + hauteur - i].fleches.add(f); }
	public void rajouterFlecheGauche(int i, Fleche f) { this.cote[2*largeur + 2*hauteur - i].fleches.add(f); }
	public void rajouterFlecheDroit(int i, Fleche f) { this.cote[largeur + i].fleches.add(f); }
	
	public void supprimerFleche(Fleche f) {
		for(Cote n: cote) 
			n.fleches.remove(f);
	}
	
	public List<Point> getPointAdjacent() {
		List<Point> res = new ArrayList<Point>();
		for(int i = 2*getX(); i < 2*getX() + 2*getLargeur()- 1;i++) {
			res.add(new Point(i,2*getY()));
			res.add(new Point(i,2*getY() + 2*getHauteur() - 1));
		}
		for(int j = 2*getY() +1 ; j < 2*getY() + 2*getHauteur() -2;j++) {
			res.add(new Point(2*getX(),j));
			res.add(new Point(2*getX()+2*getLargeur() -1,j ));
		}
		return res;
	}
	
	// modification du texte interne
	public int getLargeurColonne() {
		if(hasSplit()) {
			return largeurColonne;
		} else {
			return 0;
		}
	}
	
	public void addSplit(int largeurColonne) {
		int i = 0;
		
		if(!hasSplit()) {
			texteBoiteGauche = new ArrayList<List<String>>();
			for(List<String> l : texteBoiteDroit) {
				texteBoiteGauche.add(new ArrayList<String>());
				for(@SuppressWarnings("unused") String s : l)
					texteBoiteGauche.get(i).add("");
				i++;
			}
					
		}
		this.largeurColonne = largeurColonne;
	}
	
	public void removeSplit() {
		if(hasSplit()) {
			texteBoiteGauche = null;
		}
	}
	
	public void addCategorie(int precedent) {
		if(hasSplit())
			texteBoiteGauche.add(precedent,new ArrayList<String>());
		texteBoiteDroit.add(precedent, new ArrayList<String>());
	}
	
	public void addCategorie() {
		if(hasSplit())
			texteBoiteGauche.add(new ArrayList<String>());
		texteBoiteDroit.add( new ArrayList<String>());
	}
	
	public void removeCategorie(int indice) {
		if(hasSplit())
			texteBoiteGauche.remove(indice);
		texteBoiteDroit.remove(indice);
	}
	
	public int countCategorie() {
		return texteBoiteDroit.size();
	}
	
	public void addTexte(String droit, int categorie) {
		this.texteBoiteDroit.get(categorie).add(droit);
	}
	
	public void addTexteGauche(String gauche, int categorie) {
		if(hasSplit())
			this.texteBoiteGauche.get(categorie).add(gauche);
	}
	
	public void addTexteDroit(String droit, int categorie) {
		this.texteBoiteDroit.get(categorie).add(droit);
	}
	
	public void addTexte(String gauche, String droite, int categorie) {
		if(hasSplit())
			this.texteBoiteGauche.get(categorie).add(gauche);
		this.texteBoiteDroit.get(categorie).add(droite);
	}
	
	public void setTexte(String gauche, String droite, int categorie, int rang) {
		if(hasSplit()) {
			this.texteBoiteGauche.get(categorie).remove(rang);
			this.texteBoiteGauche.get(categorie).set(rang, gauche);
		}
		this.texteBoiteDroit.get(categorie).remove(rang);
		this.texteBoiteDroit.get(categorie).set(rang, droite);
	}
	
	public void clearTexte(int categorie) {
		if(hasSplit())
			this.texteBoiteGauche.clear();
		this.texteBoiteDroit.clear();
	}
	
	public void setTexteArea(int categorie, int taille) {
		for(int i = 1; i <= taille; i++)
			addTexte("","",categorie);
	}
	
	public boolean hasSplit() {
		return texteBoiteGauche != null;
	}
	public int getRangCategorie(int c) {
		int rang = 0;
		for(int i = 0; i < c && i < countCategorie(); i++) 
			rang += texteBoiteDroit.get(i).size();
		return rang;
	}
	
	// affichage
	public void afficherContenu(Graphics g) {
		afficherBoite(g);
		afficherTexte(g);
	}
	
	public void afficherTexte(Graphics g)  {
		// Affichage du split vertical
		if(hasSplit())
			afficherSplit(g,getX()*settings.getLargeurPixel() + settings.getPadding()/2 + getLargeurColonne() - schema.getXOffset(),
						getY()*settings.getHauteurPixel() + settings.getPadding()/2 - schema.getYOffset(),
						getX()*settings.getLargeurPixel() + settings.getPadding()/2 + getLargeurColonne() - schema.getXOffset(),
						(getY()+getHauteur())*settings.getHauteurPixel() - settings.getPadding()/2 - schema.getYOffset());
				
		// Affichage du texte et des split horizontal
		for(int i=0;i<this.texteBoiteDroit.size();i++) {
			for(int j=0;j<this.texteBoiteDroit.get(i).size();j++) {
				if(hasSplit()) {
					String str1 = this.texteBoiteGauche.get(i).get(j);
					ecrireTexte(g,getBoundTexteGauche(i,j),str1);
				}
				String str2 = this.texteBoiteDroit.get(i).get(j);
				ecrireTexte(g,getBoundTexteDroit(i,j),str2);
			}	
			afficherSplit(g, getX()*settings.getLargeurPixel() + settings.getPadding()/2 - schema.getXOffset(), 
						 getY()*settings.getHauteurPixel() + settings.getPadding()/2 + (getRangCategorie(i)+1)*settings.getHeightText() - schema.getYOffset(), 
						 getX()*settings.getLargeurPixel() + getLargeur()*settings.getLargeurPixel() - settings.getPadding()/2 - schema.getXOffset(), 
						 getY()*settings.getHauteurPixel() + settings.getPadding()/2 + (getRangCategorie(i)+1)*settings.getHeightText() - schema.getYOffset());
		}
	}
	
	public void afficherBoite(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		float[] style2 = {5,0}; // pas de pointilles
		g2d.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,style2,0.0f));
		RoundRectangle2D r = new RoundRectangle2D.Double(this.x*settings.getLargeurPixel()+this.settings.getPadding()/2 - schema.getXOffset(),
				this.y*settings.getHauteurPixel()+this.settings.getPadding()/2 - schema.getYOffset(),
				this.largeur*settings.getLargeurPixel()-this.settings.getPadding(),
				this.hauteur*settings.getHauteurPixel()-this.settings.getPadding(),
				10,
				10);
		g2d.setColor(Color.lightGray);
		g2d.fill(r);
		g2d.setColor(Color.black);
		g2d.draw(r);
	}
	
	public Rectangle getBoundTexteGauche(int categorie, int rang) {
		return new Rectangle(this.getX()*settings.getLargeurPixel() + settings.getPadding()/2 + settings.getPaddingText() - schema.getXOffset(),
				this.getY()*settings.getHauteurPixel() + settings.getPadding()+(getRangCategorie(categorie)+rang-1)*settings.getHeightText() - schema.getYOffset(),
				getLargeurColonne()-2*settings.getPaddingText(),
				settings.getHeightText());
	}
	
	public Rectangle getBoundTexteDroit(int categorie, int rang) {
		return new Rectangle(this.getX()*settings.getLargeurPixel() + settings.getPadding()/2 + settings.getPaddingText() + getLargeurColonne() - schema.getXOffset(),
				this.getY()*settings.getHauteurPixel() + settings.getPadding()+(getRangCategorie(categorie)+rang-1)*settings.getHeightText() - schema.getYOffset(),
				getLargeur()*settings.getLargeurPixel()-getLargeurColonne()-2*settings.getPaddingText() - settings.getPadding(),
				settings.getHeightText());
	}
	
	public void ecrireTexte(Graphics g, Rectangle r, String s) {
		int textWidth = (int)(settings.getFont().getStringBounds(s, settings.getFontRender()).getWidth());
		
		
		if(textWidth>r.width) {
			int k=1;
			while((int)(settings.getFont().getStringBounds(s.substring(0,k), settings.getFontRender()).getWidth())<r.width) {
				k++;
			}
			if(k > 3)
				s = s.substring(0,k-3)+"...";
			else {
				System.out.println("k : " + k + " " + r + " " + s);
				s = "...";
			}
		}
		
		g.drawString(s, r.x, r.y + r.height);
	}
	
	// fonction auxiliaire de dessin
	public void afficherSplit(Graphics g,int x1,int y1,int x2,int y2) {
		g.drawLine(x1, y1, x2, y2);
	}
	
	public void afficherRectangle(Graphics g,int x1,int y1,int width,int height) {
		g.drawLine(x1, y1, x1+width, y1);
		g.drawLine(x1+width, y1, x1+width, y1+height);
		g.drawLine(x1+width, y1+height, x1, y1+height);
		g.drawLine(x1, y1+height, x1, y1);
	}
	
	public void mouseClicked(MouseEvent e) {
		Rectangle bound;
		boolean found = false;
		Point position = schema.screenToGrid(new Point(e.getX(), e.getY()));
		
		for(int i=0;i<this.texteBoiteDroit.size() && !found;i++) {
			for(int j=0;j<this.texteBoiteDroit.get(i).size() && !found;j++) {
				if(hasSplit()) {
					bound = this.getBoundTexteGauche(i, j);
					if(position.getX() > bound.x && position.getY() > bound.y && position.getX() < bound.x + bound.width && position.getY() < bound.y + bound.height) {
						if(SwingUtilities.isRightMouseButton(e))
							for(IObserveurBoite o : observeur)o.cliqueDroitTexte(false, i, j, e);
						if(SwingUtilities.isLeftMouseButton(e))
							for(IObserveurBoite o : observeur)o.cliqueGaucheTexte(false, i, j, e);
						found = true;
					}
				}
				bound = this.getBoundTexteDroit(i, j);
				if(position.getX() > bound.x && position.getY() > bound.y && position.getX() < bound.x + bound.width && position.getY() < bound.y + bound.height) {
					if(SwingUtilities.isRightMouseButton(e))
						for(IObserveurBoite o : observeur)o.cliqueDroitTexte(false, i, j, e);
					if(SwingUtilities.isLeftMouseButton(e))
						for(IObserveurBoite o : observeur)o.cliqueGaucheTexte(false, i, j, e);
					found = true;
				}
			}
		}
		if(!found) {
			if(SwingUtilities.isRightMouseButton(e))
				for(IObserveurBoite o : observeur)o.cliqueDroit(x, y, e);
			if(SwingUtilities.isLeftMouseButton(e))
				for(IObserveurBoite o : observeur)o.cliqueGauche(x,y,e);
		}
	}
}
	
