package Noyau.Arbre;

import java.util.List;

public interface INoeudNommable extends INoeud, INommable {
	
	List<String> getStringPath();
}
