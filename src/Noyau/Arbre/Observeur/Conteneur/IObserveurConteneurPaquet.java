package Noyau.Arbre.Observeur.Conteneur;

import Noyau.Arbre.Contrat.Conteneur.IConteneurPaquet;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Arbre.Observeur.IObserveur;

public interface IObserveurConteneurPaquet extends IObserveur {

	void paquetAjoute(IConteneurPaquet i, Paquet a);
	void paquetRetire(IConteneurPaquet i, Paquet a);
}
