package UML.Observeur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import Schema.Boite;
import Schema.Observeur.IObserveurBoite;
import UML.SchemaUML;
import Application.GestionProjet.FenetreDialogue;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Enum;
import Noyau.Arbre.ElementJava.Paquet;
import Noyau.Arbre.Observeur.Conception.IObserveurEnum;

public class ObserveurEnum implements IObserveurEnum, IObserveurBoite {
	
	private Boite boite;
	
	private Enum enu;
	private List<String> membres = enu.getMembres();
	
	private SchemaUML schemaUML;
	
	public ObserveurEnum(SchemaUML schuml, Enum e) {
		this.schemaUML=schuml;
		int nbElements = e.getMembres().size();
		int hauteurBoite = nbElements/10;
		//this.boite = schemaUML.getSchema().addBoite(1,hauteurBoite);
		e.addObserveur(this);
		this.enu=e;
	}
	
	public Enum getEnum() {
		return this.enu;
	}
	
	public void supprimer() {
		// supprimer l'observateur enum		
	}

	@Override
	public void changeNom(INoeudNommable n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nouveauMembre(Enum e, String membre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void suppressionMembre(Enum e, String membre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changementMembre(Enum e, String membre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroitTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		
		// Enumeration
		if(categorie==0) {
			JPopupMenu menu = new JPopupMenu ();
	    	
	    	JMenuItem rename = new JMenuItem("renommer enumeration");
	    	JMenuItem suppr = new JMenuItem("supprimer enumeration");
	    	
	    	rename.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					FenetreDialogue.modifierEnumeration(enu);
				}
	    	});
	    	
		    suppr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					enu.supprimer();
				}
	    	});
	    	
	    	menu.add (rename);
	    	menu.add (suppr);

	    	menu.show(e.getComponent(), e.getX(), e.getY());
		}
		
		// Membre
		else if(categorie==1) {
			final int rangfin = rang;
			JPopupMenu menu = new JPopupMenu ();
	    	
	    	JMenuItem modif = new JMenuItem("modifier membre");
	    	JMenuItem suppr = new JMenuItem("supprimer membre");
	    	
	    	modif.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					//FenetreDialogue.modifierMembre(membres.get(rangfin));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				}
	    	});
	    	
		    suppr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					enu.removeMembre(membres.get(rangfin));
				}
	    	});
	    	
	    	menu.add (modif);
	    	menu.add (suppr);

	    	menu.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	@Override
	public void cliqueGaucheTexte(boolean droite, int categorie, int rang,
			MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueDroit(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cliqueGauche(int x, int y, MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
