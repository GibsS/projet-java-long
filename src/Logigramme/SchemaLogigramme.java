package Logigramme;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;

import Application.Application;
import Noyau.Arbre.INoeud;
import Noyau.Arbre.INoeudNommable;
import Noyau.Arbre.ElementJava.Fonction;
import Noyau.Arbre.Observeur.Conception.IObserveurFonction;
import Noyau.Arbre.Projet.Noyau;
import Schema.*;
import Schema.Observeur.IObserveurSchema;

public class SchemaLogigramme extends JPanel implements IObserveurFonction, IObserveurSchema {
	
	private BarreChoix barreChoix; // la barre de choix

	private Schema schema; // le schema
	
	private Noyau noyau; // le noyau
	
	private TypeInstruction typeBoiteSelectionnee; // le type de la boite selectionnee
	
	private Application application;
	
	final public static int largeurBoiteElementaire = 100; // la largeur des boite elementaires
	final public static int hauteurBoiteElementaire = 100; // la hauteur des boite elementaires
	final public static int nombreBoiteLargeur = 20; // le nombre de boites sur la largeur
	final public static int nombreBoiteHauteur = 15; // le nombre de boites sur la hauteur
	
	final public static int seuilFlecheProche = 10; // le nombre de pixels en dessous duquel une fleche est proche
	
	public SchemaLogigramme(Application a) {
		this.application = a;
		this.schema = new Schema(largeurBoiteElementaire,hauteurBoiteElementaire,nombreBoiteLargeur,nombreBoiteHauteur);
		this.barreChoix = new BarreChoix(1,100,1200);
		this.typeBoiteSelectionnee = TypeInstruction.Aucune; //Correspond � aucune boite selectionnee
		
		this.setLayout(new BorderLayout());
		
		JButton boutonDoWhile = new JButton("Do While");
		boutonDoWhile.setSize(100,80);
		boutonDoWhile.addActionListener(new SelectionDoWhile());
		JButton boutonFor = new JButton("For");
		boutonFor.setSize(100,80);
		boutonFor.addActionListener(new SelectionFor());
		JButton boutonIf = new JButton("If");
		boutonIf.setSize(100,80);
		boutonIf.addActionListener(new SelectionIf());
		JButton boutonSequence = new JButton("Sequence");
		boutonSequence.setSize(100,80);	
		boutonSequence.addActionListener(new SelectionSequence());
		JButton boutonSwitch = new JButton("Switch");
		boutonSwitch.setSize(100,80);
		boutonSwitch.addActionListener(new SelectionSwitch());
		JButton boutonWhile = new JButton("While");
		boutonWhile.setSize(100,80);
		boutonWhile.addActionListener(new SelectionWhile());

		barreChoix.ajouterBouton(boutonSequence,0);
		barreChoix.ajouterBouton(boutonIf,0);
		barreChoix.ajouterBouton(boutonSwitch,0);
		barreChoix.ajouterBouton(boutonFor,0);
		barreChoix.ajouterBouton(boutonWhile,0);
		barreChoix.ajouterBouton(boutonDoWhile,0);
		
		this.add(schema,BorderLayout.CENTER);
		this.add(barreChoix,BorderLayout.NORTH);

		Boite debut = new Boite(0,0,1,1,this.schema);
		debut.addCategorie();
		debut.addTexte("DEBUT",0);
		this.schema.addBoite(debut);
		
		Boite fin = new Boite(2,0,1,1,this.schema);
		fin.addCategorie();
		fin.addTexte("FIN",0);
		this.schema.addBoite(fin);
		
		Fleche f = new Fleche(this.schema);
		this.schema.addFleche(f,debut,fin);
		
		Contexte c = new Contexte(0,0,5,5, schema);
		c.enableDraw();
		schema.getContexte().addContexte(c);
		
		this.schema.enableDragging();
	}
	
	public Application getApplication() {
		return application;
	}
	

	@Override
	public void cliqueDroitCaseVide(int x, int y) {
		schema.addBoite(x,y,1,1);
		
	}

	@Override
	public void cliqueGaucheCaseVide(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeNom(INoeudNommable n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePetitFils(INoeud n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeType(Fonction f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addArgument(Fonction f, String argument) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeArgument(Fonction f, String argument) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCorp(Fonction f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCorp(Fonction f) {
		// TODO Auto-generated method stub
		
	}
	
	class SelectionDoWhile implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			typeBoiteSelectionnee = TypeInstruction.DoWhile;
		}
	}
	
	class SelectionFor implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			typeBoiteSelectionnee = TypeInstruction.For;
		}
	}
	
	class SelectionIf implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			typeBoiteSelectionnee = TypeInstruction.If;
		}
	}
	
	class SelectionSequence implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			typeBoiteSelectionnee = TypeInstruction.Sequence;
		}
	}
	
	class SelectionSwitch implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			typeBoiteSelectionnee = TypeInstruction.Switch;
		}
	}
	
	class SelectionWhile implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			typeBoiteSelectionnee = TypeInstruction.While;
		}
	}

	@Override
	public void flecheFini(Fleche f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dragFini(Boite b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Fleche f = this.schema.getFlecheProche(e.getX(),e.getY(),seuilFlecheProche);
		if (f!=null) {
			switch(this.typeBoiteSelectionnee) {
				case DoWhile : 
					
					break;
				case For : 
					
					break;
				case If : 
					
					break;
				case Sequence : 
					
					break;
				case Switch : 
					
					break;
				case While : 
					
					break;
				default :
					
			}
		}
	}

}
