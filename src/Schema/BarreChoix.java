package Schema;

import javax.swing.*;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.geom.Line2D;

/** Represente une barre de choix (exemple : choix du type de boite a ajouter)
 * @author G33
 */
@SuppressWarnings("serial")
public class BarreChoix extends JPanel {

	private int nb_lignes; // le nombre de lignes de boutons
	
	private JButton boutonsDeChoix[][]; // le tableau contenant l'ensemble des boutons
	
	private int nb_BoutonsParLigne[]; // tableau contenant le nombre de boutons par ligne
	
	private int hauteurLigne; // la hauteur d'une ligne
	
	private int largeurFenetre; // la largeur de la fenetre
	
	private JPanel lignes[];
	
	/** Construit une barre de choix en fonction du nombre de ligne de boutons, de la hauteur d'une ligne et de la largeur de la fenetre
	 * @param nbl	le nombre de lignes
	 * @param hl	la hauteur d'une ligne
	 * @param lf	la largeur de la fenetre
	 */
	public BarreChoix(int nbl, int hl, int lf) {
		this.setLayout(new GridLayout(nbl,1));
		this.nb_lignes=nbl;
		this.hauteurLigne=hl;
		this.largeurFenetre=lf;
		this.boutonsDeChoix = new JButton[this.nb_lignes][10];
		//this.nb_BoutonsParLigne = new int[this.nb_lignes];
		this.lignes = new JPanel[nbl];
		for(int i=0;i<this.nb_lignes;i++) {
			//this.nb_BoutonsParLigne[i]=0;
			this.lignes[i]=new JPanel();
			this.add(this.lignes[i]);
			this.lignes[i].setLayout(new FlowLayout());
		}
		
	}
	
	/** Ajoute un bouton a la ligne voulue
	 * @param bouton	le bouton
	 * @param numeroLigne	le numero de la ligne
	 */
	public void ajouterBouton(JButton bouton, int numeroLigne) {
		this.lignes[numeroLigne].add(bouton);
	}
	/*public void ajouterBouton(JButton bouton, int numeroLigne) {
		this.add(bouton);
		this.boutonsDeChoix[numeroLigne][this.nb_BoutonsParLigne[numeroLigne]]=bouton;
		this.nb_BoutonsParLigne[numeroLigne]++;
		bouton.setSize(100, this.hauteurLigne-10);
		for(int i=0;i<this.nb_BoutonsParLigne[numeroLigne];i++) {
			this.boutonsDeChoix[numeroLigne][i].setLocation(10,10);
			//this.boutonsDeChoix[numeroLigne][i].setLocation((this.largeurFenetre/2)+(i-(this.nb_BoutonsParLigne[numeroLigne])/2)*110, numeroLigne*this.hauteurLigne+5);
		}
	}*/
	
	/** Retourne le nombre de lignes de boutons
	 */
	public int getNbLignes() {
		return this.nb_lignes;
	}
	
	/** Retourne la hauteur d'une ligne
	 */
	public int getHauteurLigne() {
		return this.hauteurLigne;
	}
	
	public void dessiner(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		float[] style = {5,0};
		g2d.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f,style,0.0f));
		for(int i=1;i<=this.nb_lignes;i++) {
			//g.drawLine(0, i*this.hauteurLigne, this.largeurFenetre, i*this.hauteurLigne);
			Line2D l = new Line2D.Double(0, i*this.hauteurLigne, this.largeurFenetre, i*this.hauteurLigne);
			g2d.draw(l);
		}
	}
	
}
