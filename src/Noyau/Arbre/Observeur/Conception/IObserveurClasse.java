package Noyau.Arbre.Observeur.Conception;

import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conteneur.*;

public interface IObserveurClasse extends IObserveurNommable, IObserveurConteneurModifieur, 
										  IObserveurConteneurDependance,  IObserveurConteneurClasse, 
										  IObserveurConteneurInterface, IObserveurConteneurEnum, 
				  						  IObserveurConteneurAttribut, IObserveurConteneurFonction {
	
}
