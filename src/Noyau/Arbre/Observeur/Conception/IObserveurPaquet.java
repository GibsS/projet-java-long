package Noyau.Arbre.Observeur.Conception;

import Noyau.Arbre.Observeur.IObserveurNommable;
import Noyau.Arbre.Observeur.Conteneur.*;

public interface IObserveurPaquet extends IObserveurNommable, IObserveurConteneurPaquet, IObserveurConteneurClasse,
										  IObserveurConteneurInterface, IObserveurConteneurEnum {

}
