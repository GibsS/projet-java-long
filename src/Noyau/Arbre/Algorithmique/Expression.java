package Noyau.Arbre.Algorithmique;

import Noyau.Code.Code;

/** la classe Expression représente une expression au sens Java du terme.
 * Dans cette implémentation, l'expression n'est représenté que par un string.
 * @author gibson
 */
public class Expression {
	public String valeur;
	
	public Expression(String v) {
		valeur = v;
	}
	
	public Expression() {
		
	}
	
	public Code interpreterCode(Code code) {
		Code c = new Code("");
		valeur = code.toString();
		return c;
	}
	
	public String creerCode() {
		return valeur;
	}
}
